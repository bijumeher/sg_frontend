import Api from '../../utils/Api';

export const createCourse = (courseData) => {
    return async (dispatch) => {
        try {
            const response = await Api.post('create_course', courseData);
            const courseDataResponse = response?.data;
            console.log('API Response:', courseDataResponse);

            if (courseDataResponse.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: 'success',
                        message: courseDataResponse.message,
                    },
                });
            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: 'error',
                        message: courseDataResponse.message ,
                    },
                });
            }
        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: error.response?.data?.message  },
            });
        }
    };
};

export const getCoursesAllData = () => {
    return async (dispatch) => {
        try {
            const response = await Api.get('get_all_course');
            const courseDataResponse = response?.data;

            if (courseDataResponse?.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: 'success',
                        message: courseDataResponse.message,
                    },
                });
                dispatch({
                    type: 'ADD_COURSE_ALERT',
                    payload: courseDataResponse.data,
                });
            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: 'error',
                        message: courseDataResponse.message ,
                    },
                });
            }
        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: error.response?.data?.message  },
            });
        }
    };
};

export const updateCourseData = (updatedCourseData) => {
    return async (dispatch) => {
        try {
            const response = await Api.put(`update_course/${updatedCourseData.courseId}`, updatedCourseData);
            const courseDataResponse = response?.data;

            if (courseDataResponse?.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: 'success',
                        message: courseDataResponse.message,
                    },
                });
            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: 'error',
                        message: courseDataResponse.message ,
                    },
                });
            }
        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: error.response?.data?.message },
            });
        }
    };
};

export const deleteCourseData = (courseId) => {
    return async (dispatch) => {
        try {
            const response = await Api.delete(`delete_course/${courseId}`);
            const courseDataResponse = response?.data;

            if (courseDataResponse?.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: 'success',
                        message: courseDataResponse.message,
                    },
                });
            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: 'error',
                        message: courseDataResponse.message ,
                    },
                });
            }
        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: error.response?.data?.message  },
            });
        }
    };
};
