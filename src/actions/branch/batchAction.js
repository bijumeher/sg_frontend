import Api from '../../utils/Api';

export const createBatch = (branchData) => {
    return async (dispatch) => {
        try {
            const response = await Api.post('create_batch', branchData);
            const batchData = response?.data;
            console.log('API Response:', batchData);

            if (batchData.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: batchData.message,
                    }
                });

            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: batchData.message,
                    },
                });
            }

        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
        }
    };
};

export const getBatchAlldata = () => {
    return async (dispatch) => {
        try {
            const response = await Api.get(`get_batch`);
            const batchData = response?.data;
            // console.log('API Response:', batchData);

            if (batchData.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: batchData.message,
                    }
                })
                dispatch({
                    type: 'ADD_BATCH_ALERT',
                    payload: batchData.data

                })
            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: batchData.message,
                    },
                });
            }

        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
        }
    };
};

export const updateBatchData = (updatedBatch) => {
    return async (dispatch) => {
        try {
            const response = await Api.put(`update_batch/${updatedBatch.batchId}`, updatedBatch);
            const batchData = response?.data;
            console.log('API Response:', batchData);

            if (batchData.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: batchData.message,
                    }
                });

            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: batchData.message,
                    },
                });
            }

        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
        }
    };
};

export const DeleteBatchData = (batchId) => {
    return async (dispatch) => {
        try {
            const response = await Api.delete(`delete_batch/${batchId}`);
            const batchData = response?.data;
            console.log('API Response:', batchData);

            if (batchData.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: batchData.message,
                    }
                });
                

            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: batchData.message,
                    },
                });
            }

        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
        }
    };
};
