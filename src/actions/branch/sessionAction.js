import Api from '../../utils/Api';

export const createSession = (sessionData) => {
    return async (dispatch) => {
        try {
            const response = await Api.post('create_session', sessionData);
            const sessionallData = response?.data;
            console.log('API Response:', sessionallData);

            if (sessionallData.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: sessionallData.message,
                    }
                });

            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: sessionallData.message,
                    },
                });
            }

        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
        }
    };
};

export const getAllsessionData = () => {
    return async (dispatch) => {
        try {
            const response = await Api.get('get_session');
            const sessionData = response?.data;
            // console.log('API Response:', batchData);

            if (sessionData.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: sessionData.message,
                    }
                })
                dispatch({
                    type: 'ADD_SESSION_ALERT',
                    payload: sessionData.data

                })
            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: sessionData.message,
                    },
                });
            }

        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
        }
    };
};

export const updateSessionData = (sessionData) => {
    return async (dispatch) => {
        try {
            const response = await Api.put(`update_session`, sessionData);
            const sessionupdateData = response?.data;
            console.log('API Response:', sessionupdateData);

            if (sessionupdateData.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: sessionupdateData.message,
                    }
                });
                dispatch(getAllsessionData());

            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: sessionupdateData.message,
                    },
                });
            }

        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
        }
    };
};

export const deleteSessionData = (sessionId) => {
    return async (dispatch) => {
        try {
            const response = await Api.delete(`delete_session/${sessionId}`);
            const deleteData = response?.data;
            console.log('API Response:', deleteData);

            if (deleteData.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: deleteData.message,
                    }
                });

            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: deleteData.message,
                    },
                });
            }

        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
        }
    };
};
