import Api from '../../utils/Api';
import { toast } from 'react-hot-toast';
export const login = (email, password, authType, navigate) => {
  return async (dispatch) => {
    try {
      const response = await Api.post('branch/login', { email, password, authType });
      const userData = response?.data;
      console.log('API Response:', userData);

      if (userData.status === 201) {
        sessionStorage.setItem('branchData', JSON.stringify(userData.branchData));
        navigate('/branch')
        dispatch({
          type: 'ADD_API_ALERT',
          payload: {
            severity: "success",
            message: userData.message,
          }
        });


      } else {
        dispatch({
          type: 'ADD_API_ALERT',
          payload: {
            severity: "error",
            message: userData.message,
          },
        });
        if (authType === 'branch') {
          toast.error(' Contact to the admin.');
        }

      }

    } catch (error) {
      dispatch({
        type: 'ADD_ERROR_ALERT',
        payload: { message: 'Server error. Please try again.' },

      });
      if (authType === 'branch') {
        toast.error(' Contact to the admin.');
      }
    }
  };
};

export const studentRegister = (studentData, navigate) => {
  return async (dispatch) => {
    try {
      const response = await Api.post('create_register_data', studentData);
      const registerstudentData = response?.data;
      console.log('API Response:', registerstudentData);

      if (registerstudentData.status === 201) {
        // console.log('Dispatching ADD_API_ALERT', registerstudentData.message);
        navigate('/next-page');

      } else if (registerstudentData.status === 400) {
        toast.error(registerstudentData.message);

      } else {
        toast.error(registerstudentData.error);

      }

    } catch (error) {
      dispatch({
        type: 'ADD_ERROR_ALERT',
        payload: { message: 'server error' },

      });
      toast.error('server error');

    }
  };
};


export const logout = () => {
  return (dispatch) => {
    sessionStorage.clear();
    dispatch({
      type: 'ADD_API_ALERT',
      payload: {
        severity: "error",
        message: 'Logout successful',
      },
    });
  };
};

export const getallBranchData = () => {
  return async (dispatch) => {
    try {
      const response = await Api.get('get_all_branch_data');
      const branchesData = response?.data;

      console.log(branchesData);

      if (branchesData.status === 201) {
        dispatch({
          type: 'FETCH_BRANCHES_SUCCESS',
          payload: branchesData.data,
        });
        dispatch({
          type: 'ADD_API_ALERT',
          payload: {
            severity: "success",
            message: branchesData.message,
          },
        });
      } else {
        dispatch({
          type: 'ADD_API_ALERT',
          payload: {
            severity: "error",
            message: branchesData.message,
          },
        });
      }
    } catch (error) {
      dispatch({
        type: 'ADD_API_ALERT',
        payload: {
          severity: "error",
          message: "Server error",
        },
      });
    }
  };
};
export const getBranchDataByid = ({ branchId }) => {
  return async (dispatch) => {
    try {
      const response = await Api.get(`get_branch_data_id/${branchId}`);
      const getbranchData = response?.data;

      console.log(getbranchData);

      if (getbranchData.status === 201) {
        dispatch({
          type: 'FETCH_BRANCHES_ID_SUCCESS',
          payload: getbranchData.data,
        });
        dispatch({
          type: 'ADD_API_ALERT',
          payload: {
            severity: "success",
            message: getbranchData.message,
          },
        });
      } else {
        dispatch({
          type: 'ADD_API_ALERT',
          payload: {
            severity: "error",
            message: getbranchData.message,
          },
        });
      }
    } catch (error) {
      dispatch({
        type: 'ADD_API_ALERT',
        payload: {
          severity: "error",
          message: "Server error",
        },
      });
    }
  };
};
