import Api from '../../utils/Api';


export const studentRegister = (studentData, navigate) => {
  return async (dispatch) => {
    try {
      const response = await Api.post('create_register_data', studentData);
      const registerstudentData = response?.data;
      console.log('API Response:', registerstudentData);

      if (registerstudentData.status === 201) {
        // console.log('Dispatching ADD_API_ALERT', registerstudentData.message);
        dispatch({
          type: 'ADD_API_ALERT',
          payload: {
            severity: "success",
            message: registerstudentData.message,
          }
        });
        navigate('/')


      } else {
        dispatch({
          type: 'ADD_API_ALERT',
          payload: {
            severity: "error",
            message: registerstudentData.message,
          },
        });


      }

    } catch (error) {
      dispatch({
        type: 'ADD_ERROR_ALERT',
        payload: { message: 'server error'},

      });

    }
  };
};
export const getAllStudentdata = () => {
  return async (dispatch) => {
      try {
          const response = await Api.get(`get_all_register_data`);
          const studentData = response?.data;
          // console.log('API Response:', studentData);

          if (studentData.status === 201) {
              dispatch({
                  type: 'ADD_API_ALERT',
                  payload: {
                      severity: "success",
                      message: studentData.message,
                  }
              })
              dispatch({
                  type: 'ADD_STUDENT_ALERT',
                  payload: studentData.data

              })
          } else {
              dispatch({
                  type: 'ADD_API_ALERT',
                  payload: {
                      severity: "error",
                      message: studentData.message,
                  },
              });
          }

      } catch (error) {
          dispatch({
              type: 'ADD_ERROR_ALERT',
              payload: { message: 'Server error. Please try again.' },
          });
      }
  };
};
export const getStudentdataById = (studentId) => {
  return async (dispatch) => {
      try {
          const response = await Api.post(`get_register_data_id`, { studentId });
          const studentData = response?.data;
          console.log('API Response:', studentData);

          if (studentData.status === 201) {
              dispatch({
                  type: 'ADD_STUDENTID_ALERT',
                  payload: studentData.data,  // Ensure the correct data structure
              });
              dispatch({
                  type: 'ADD_STUDENT_ALERT',
                  payload: {
                      severity: "success",
                      message: studentData.message,
                  },
              });
          } else {
              dispatch({
                  type: 'ADD_API_ALERT',
                  payload: {
                      severity: "error",
                      message: studentData.message,
                  },
              });
          }
      } catch (error) {
          dispatch({
              type: 'ADD_ERROR_ALERT',
              payload: { message: 'Server error. Please try again.' },
          });
      }
  };
};

export const getStudentsByBatch = (batchId) => async (dispatch) => {
  try {
    const response = await Api.get(`batches/${batchId}/students`);
    dispatch({
      type: 'GET_STUDENTS_BY_BATCH',
      payload: response.data.students,
    });
  } catch (error) {
    console.error('Error fetching students:', error);
  }
};


