import Api from '../../utils/Api'
export const contactRegister = (contactData) => {
    return async (dispatch) => {
        try {
  
            const response = await Api.post('create_Contact', contactData);//admindata dispatch from login page
            const contactSendData = response?.data
            // console.log(response,'<<<response');
            // console.log(response.data,'<<<response.data');
            if (contactSendData.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: contactSendData.message
                    }
                })

            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: contactSendData.message
                    }
                })
            }
  
        }
  
        catch {
            dispatch({
                type: "ADD_API_ALERT",
                payload: {
                    severity: "error",
                    message: "server error"
                }
            })
        }
    }
  }

