import Api from '../../utils/Api'


export const applyLoan = (loanData) => {
  return async (dispatch) => {
    try {

      const response = await Api.post('create_Loan', loanData);//admindata dispatch from login page
      const loanRegisterData = response?.data
      // console.log(response,'<<<response');
      // console.log(response.data,'<<<response.data');
      if (loanRegisterData.status === 201) {
        dispatch({
          type: 'ADD_API_ALERT',
          payload: {
            severity: "success",
            message: loanRegisterData.message
          }
        })

      } else {
        dispatch({
          type: 'ADD_API_ALERT',
          payload: {
            severity: "error",
            message: loanRegisterData.message
          }
        })
      }

    }

    catch {
      dispatch({
        type: "ADD_API_ALERT",
        payload: {
          severity: "error",
          message: "server error"
        }
      })
    }
  }
}

