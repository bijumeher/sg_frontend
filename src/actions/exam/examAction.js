import Api from '../../utils/Api';

export const getAllExampaper = () => {
    return async (dispatch) => {
        try {
            const response = await Api.post('admin/getexam');
            const allexamdata = response?.data;
            console.log(allexamdata);

            if (allexamdata.status === 201) {
                dispatch({
                    type: 'ADD_EXAM_DATA',
                    payload: allexamdata?.examdata,
                });

            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: allexamdata.message,
                    },
                });
            }
        } catch (error) {
            dispatch({
                type: 'ADD_API_ALERT',
                payload: {
                    severity: "error",
                    message: "Failed to create exam",
                },
            });
        }
    };
};


export const deleteExamPaper = (examId) => {
    return async (dispatch) => {
        try {
            const response = await Api.delete(`admin/delete/exam/${examId}`);
            const deleteExamData = response?.data;

            console.log(deleteExamData);

            if (deleteExamData.status === 201) {
                dispatch({
                    type: 'REMOVE_EXAM_DATA',
                    payload: examId, // Remove the specific exam from the state by its ID
                });

                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: "Exam deleted successfully",
                    },
                });
            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: deleteExamData.message,
                    },
                });
            }
        } catch (error) {
            dispatch({
                type: 'ADD_API_ALERT',
                payload: {
                    severity: "error",
                    message: "Failed to delete exam",
                },
            });
        }
    };
};