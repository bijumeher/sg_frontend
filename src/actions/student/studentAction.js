import Api from '../../utils/Api';

export const Studentlogin = (email, password, authType, navigate) => {
    return async (dispatch) => {
        try {

            const response = await Api.post('login', { email, password, authType });
            const studentData = response?.data;
            console.log('API Response:', studentData);
            if (studentData.status === 201) {
                sessionStorage.setItem('registerData', JSON.stringify(studentData.registerData));
                navigate('/student');
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: studentData.registerData,
                    severity: "success",
                    message: studentData.message
                });
            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: studentData.message
                    },
                });
                return false;
            }

        } catch (error) {
            dispatch({
                type: 'ADD_API_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
            return false;
        }
    };
};
export const registerUserName = (email) => {
    return async (dispatch) => {
        try {
            const response = await Api.post('verify_emial', { email });
            const result = response?.data;
            if (result.status === 200) {
                return { success: true, name: result.name };
            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: result.message
                    },
                });
                return { success: false };
            }
        } catch (error) {
            dispatch({
                type: 'ADD_API_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
            return { success: false };
        }
    };
};

export const forgotPassword = (email, newPassword, navigate) => {
    return async (dispatch) => {
        try {
            const response = await Api.post('reset_password', { email, newPassword });
            const result = response?.data;
            if (result.status === 201) {
                navigate('/login');
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: result.message
                    },
                });
            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: result.message
                    },
                });
            }
        } catch (error) {
            dispatch({
                type: 'ADD_API_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
        }
    };
};
export const getDataBrancWise = (obj) => {
    return async (dispatch) => {
        try {
            const response = await Api.post('branch_wise_data', obj);
            const branchWiseData = response?.data;

            console.log(branchWiseData);

            if (branchWiseData.status === 201) {
                dispatch({
                    type: 'ADD_BRANCHE_DATA',
                    payload: branchWiseData.data,
                });
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: branchWiseData.message,
                    },
                });
            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: branchWiseData.message,
                    },
                });
            }

        }
        catch (error) {
            dispatch({
                type: 'ADD_API_ALERT',
                payload: {
                    severity: "error",
                    message: "Server error",
                },
            });
        }
    };
};
export const createpayment = (paymentdata) => {
    return async (dispatch) => {
        try {
            const response = await Api.post('create_payment', paymentdata);
            const paymentData = response?.data;
            console.log('API Response:', paymentData);
            if (paymentData.status === 201) {
                localStorage.setItem('paymentData', JSON.stringify(paymentData.data));
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: paymentData.message,
                    }
                });
                return response;
            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: paymentData.message,
                    },
                });
                return response;
            }

        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
        }
    };
};
export const getPaymentAlldata = () => {
    return async (dispatch) => {
        try {
            const response = await Api.get(`get_payment`);
            const paymentData = response?.data;
            // console.log('API Response:', paymentData);

            if (paymentData.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: paymentData.message,
                    }
                })
                dispatch({
                    type: 'ADD_PAYMENT_ALERT',
                    payload: paymentData.data

                })
            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: paymentData.message,
                    },
                });
            }

        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
        }
    };
};


export const DeletePaymentData = (paymentId) => {
    return async (dispatch) => {
        try {
            const response = await Api.delete(`delete_payment/${paymentId}`);
            const paymentData = response?.data;
            console.log('API Response:', paymentData);

            if (paymentData.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: paymentData.message,
                    }
                });


            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: paymentData.message,
                    },
                });
            }

        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
        }
    };
};
export const updateStudentData = (updatedStudent) => {
    return async (dispatch) => {
        try {
            const response = await Api.put(`register_update`, updatedStudent);
            const updatedData = response?.data;
            console.log('API Response:', updatedData);

            if (updatedData.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: updatedData.message,
                    }
                });


            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: updatedData.message,
                    },
                });
            }
            return updatedData;

        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
        }
    };
};




export const deleteStudentData = (id) => {
    return async (dispatch) => {
        try {
            const response = await Api.delete(`delete_student_data/${id}`);
            const branchbystudentData = response?.data;
            console.log('API Response:', branchbystudentData);

            if (branchbystudentData.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: branchbystudentData.message,
                    }
                });


            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: branchbystudentData.message,
                    },
                });
            }

        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
        }
    };
};
export const logout = () => {
    return (dispatch) => {
        sessionStorage.clear();
        dispatch({ type: 'LOGOUT' });
    };
};

