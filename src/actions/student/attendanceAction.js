import Api from '../../utils/Api';

export const createAttendance = (studentateendata) => {
    return async (dispatch) => {
        try {
            const response = await Api.post('create_attendance', studentateendata);
            const attendanceData = response?.data;
            console.log('API Response:', attendanceData);

            if (attendanceData.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: attendanceData.message,
                    }
                });

            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: attendanceData.message,
                    },
                });
            }

        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
        }
    };
};

export const getattendanceAlldata = () => {
    return async (dispatch) => {
        try {
            const response = await Api.get(`get_attendance`);
            const attendanceData = response?.data;
            console.log('API Response:', attendanceData);

            if (attendanceData.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: attendanceData.message,
                    }
                })
                dispatch({
                    type: 'ADD_ATTENDANCE_ALERT',
                    payload: attendanceData.data

                })
            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: attendanceData.message,
                    },
                });
            }

        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
        }
    };
};
export const getattendancedataById = () => {
    return async (dispatch) => {
        try {
            const response = await Api.get(`get_attendances`);
            const attendanceData = response?.data;
            console.log('API Response:', attendanceData);

            if (attendanceData.status === 201) {
                dispatch({
                    type: 'FETCH_ATTENDANCE_ID_SUCCESS',
                    payload: {
                        severity: "success",
                        message: attendanceData.message,
                    }
                })
                dispatch({
                    type: 'ADD_ATTENDANCE_ALERT',
                    payload: attendanceData.data

                })
            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: attendanceData.message,
                    },
                });
            }

        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
        }
    };
};
export const updateAttendanceData = (updatedAttendance) => {
    return async (dispatch) => {
        try {
            const response = await Api.put(`update_attendance`, updatedAttendance);
            const attendanceData = response?.data;
            console.log('API Response:', attendanceData);

            if (attendanceData.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: attendanceData.message,
                    }
                });

            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: attendanceData.message,
                    },
                });
            }

        } catch (error) {
            dispatch({
                type: 'ADD_ERROR_ALERT',
                payload: { message: 'Server error. Please try again.' },
            });
        }
    };
};
export const AttendanceDataDelete = (attendanceId) => {
    return async (dispatch) => {
        try {
            const response = await Api.delete(`delete_attendance/${attendanceId}`);
            const attendancedata = response?.data;
            console.log(attendancedata);

            if (attendancedata.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: attendancedata?.data,
                });

            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: attendancedata.message,
                    },
                });
            }
        } catch (error) {
            dispatch({
                type: 'ADD_API_ALERT',
                payload: {
                    severity: "error",
                    message: "Failed to create exam",
                },
            });
        }
    };
};