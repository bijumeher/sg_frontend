import Api from '../../utils/Api'


export const createFranchise = (franchiseData) => {
    return async (dispatch) => {
        try {

            const response = await Api.post('create_franchise', franchiseData);//admindata dispatch from login page
            const FranchiseData = response?.data
            console.log(FranchiseData,'<<<response');
            // console.log(response.data,'<<<response.data');
            if (FranchiseData.status === 201) {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "success",
                        message: FranchiseData.message
                    }
                })

            } else {
                dispatch({
                    type: 'ADD_API_ALERT',
                    payload: {
                        severity: "error",
                        message: FranchiseData.message
                    }
                })
            }

        }

        catch {
            dispatch({
                type: "ADD_API_ALERT",
                payload: {
                    severity: "error",
                    message: "server error"
                }
            })
        }
    }
}

