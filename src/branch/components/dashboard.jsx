
import React from 'react';
import { RiComputerFill } from "react-icons/ri";
import { PiStudentBold } from "react-icons/pi";
import { FaPeopleRoof } from "react-icons/fa6";
import { BiCoinStack } from "react-icons/bi";
import { GiReceiveMoney } from "react-icons/gi";
import { GiTakeMyMoney } from "react-icons/gi";
import { Box, Drawer } from '@mui/material';
const drawerWidth = 240;

// Shared Tailwind CSS classes
const buttonClasses = 'bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 mb-2 rounded flex items-center gap-6 w-full ';
const viewDetailsButtonClasses = 'mt-2 text-white font-bold py-1 px-2 rounded text-sm text-black';

const Dashboard = () => {
    return (
        <Box sx={{ display: 'flex' }}>
            <Drawer
                variant="permanent"
                
                sx={{ width: drawerWidth ,mr: 2, display: { sm: 'none' } }}
            >
            </Drawer>
            <Box sx={{ flexGrow: 1, p: 3 }}>
                <div className=" bg-zinc-100 p-4"
                    style={{ width: "81.3vw" }}
                >
                    <div className="flex mt-4">
                        <div className="flex-1 ml-4">
                            <div className="grid grid-cols-3 gap-4">
                                <div>
                                    {/* student information start */}
                                    <div className="bg-white p-4 rounded-lg shadow">
                                        <div className="text-lg font-semibold mb-2">Student Information</div>
                                        <div >
                                            <div className="relative w-40 h-40">
                                                <div
                                                    className="absolute inset-0 rounded-full border-[10px] border-blue-500"
                                                    style={{ clipPath: "polygon(50% 0%, 100% 0%, 100% 50%, 50% 50%)" }}
                                                ></div>
                                                <div
                                                    className="absolute inset-0 rounded-full border-[10px] border-yellow-500"
                                                    style={{ clipPath: "polygon(50% 0%, 50% 50%, 100% 50%, 100% 100%)" }}
                                                ></div>
                                                <div
                                                    className="absolute inset-0 rounded-full border-[10px] border-green-500"
                                                    style={{ clipPath: "polygon(50% 50%, 0% 50%, 0% 100%, 50% 100%)" }}
                                                ></div>
                                                <div
                                                    className="absolute inset-0 rounded-full border-[10px] border-red-500"
                                                    style={{ clipPath: "polygon(0% 0%, 50% 0%, 50% 50%, 0% 50%)" }}
                                                ></div>
                                            </div>
                                        </div>
                                        <div className="mt-4">
                                            <ul className="space-y-2">
                                                <li className="flex items-center">
                                                    <span className="w-4 h-4 bg-red-500 rounded-full inline-block mr-2"></span>
                                                    <span className="text-black dark:text-white">PGDCA/TALLY (30)</span>
                                                </li>
                                                <li className="flex items-center">
                                                    <span className="w-4 h-4 bg-blue-500 rounded-full inline-block mr-2"></span>
                                                    <span className="text-black dark:text-white">PGDCA (37)</span>
                                                </li>
                                                <li className="flex items-center">
                                                    <span className="w-4 h-4 bg-green-500 rounded-full inline-block mr-2"></span>
                                                    <span className="text-black dark:text-white">TALLY (3)</span>
                                                </li>
                                                <li className="flex items-center">
                                                    <span className="w-4 h-4 bg-yellow-500 rounded-full inline-block mr-2"></span>
                                                    <span className="text-black dark:text-white">BCC (3)</span>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                    {/* student information end */}
                                </div>
                                {/* course details start */}
                                <div className="col-span-2">
                                    <div className="grid grid-cols-3 gap-4">
                                        <div
                                            class="max-w-xs mx-auto bg-gradient-to-r from-[#aad2e9] via-purple-100 to-[#aaabf3] rounded-3xl border-2 border-[#191CF3]
                                         shadow-lg text-center h-[123px]  w-[180px] py-2"
                                        >
                                            <RiComputerFill size={40} className=' mx-auto mb-2' />
                                            <h2 class="text-lg font-semibold">Total Course</h2>
                                            <p class="text-xl font-bold">8</p>
                                        </div>

                                        <div
                                            class="max-w-xs mx-auto bg-gradient-to-r from-[#aaabf3] via-purple-100 to-[#a9f69d]
                                        rounded-3xl border-2 border-[#93F319] shadow-lg text-center h-[123px]  w-[180px] py-2"
                                        >
                                            <PiStudentBold size={40} className=' mx-auto mb-2' />
                                            <h2 class="text-lg font-semibold">Total Student</h2>
                                            <p class="text-xl font-bold">73</p>
                                        </div>

                                        <div
                                            class="max-w-xs mx-auto bg-gradient-to-r from-[#a9f69d] via-purple-100 to-[#ecc892] rounded-3xl border-2 border-[#F39C19] shadow-lg text-center h-[123px]  w-[180px] py-2"
                                        >
                                            <FaPeopleRoof size={40} className=' mx-auto mb-2' />
                                            <h2 class="text-lg font-semibold">Total Batch</h2>
                                            <p class="text-xl font-bold">9</p>
                                        </div>

                                        <div
                                            class="max-w-xs mx-auto bg-gradient-to-r from-[#ecc892] via-purple-100 to-[#81f594]
                                         rounded-3xl border-2 border-[#19F33C] shadow-lg text-center h-[123px]  w-[180px] py-2"
                                        >
                                            <BiCoinStack size={40} className=' mx-auto mb-2' />
                                            <h2 class=" text-xs font-semibold">Today's Payment Received</h2>
                                            <p class="text-xl font-bold ">100</p>
                                        </div>

                                        <div
                                            class="max-w-xs mx-auto bg-gradient-to-r from-[#81f594] via-purple-100 to-[#fc84b6] 
                                    rounded-3xl border-2 border-[#F31974] shadow-lg text-center h-[123px]  w-[180px] py-2"
                                        >
                                            <GiReceiveMoney size={40} className=' mx-auto mb-2' />
                                            <h2 class="text-xs font-semibold">Today's part payment</h2>
                                            <p class="text-xl font-bold">3800</p>
                                        </div>

                                        <div
                                            class="max-w-xs mx-auto bg-gradient-to-r from-[#fc84b6]  via-purple-100 to-[#7dd2f6] rounded-3xl border-2 border-[#19B1F3]
                                     shadow-lg text-center h-[123px]  w-[180px] py-2"
                                        >
                                            <GiTakeMyMoney size={40} className=' mx-auto mb-2' />
                                            <h2 class="text-xs font-semibold">Today's Final Payment</h2>
                                            <p class="text-xl font-bold">0</p>
                                        </div>

                                    </div>
                                </div>
                                {/* course details end*/}
                            </div>
                        </div>
                    </div>
                </div>
            </Box>
        </Box>

    );
};

export default Dashboard;

