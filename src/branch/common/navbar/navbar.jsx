import React, { useState, useEffect } from 'react';
import { AppBar, Box, CssBaseline, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Divider, Drawer, IconButton, List, ListItem, ListItemButton, ListItemIcon, ListItemText, Toolbar, Typography } from '@mui/material';
import { Button, Menu, MenuItem } from '@mui/material';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import group from '../../assets/images/logo.png'
import { useNavigate, useLocation } from 'react-router-dom';
import styles from './ButtonStyles.module.css';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import Avatar from '@mui/material/Avatar';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { logout } from '../../../actions/branch/branchAction';
import avtarlogo from '../../../branch/assets/images/logo.avif';
import CloseIcon from '@mui/icons-material/Close';
import { getDataBrancWise } from '../../../actions/student/studentAction';
import { useDispatch } from 'react-redux';
import MenuIcon from '@mui/icons-material/Menu';
import sglogo from '../../../branch/assets/images/logo.png';




const drawerWidth = 230;
const Navbar = () => {
  // const branchName = sessionStorage.getItem('branchName');
  const branch = JSON.parse(sessionStorage.getItem('branchData'));
  const branchName = branch.branchName;
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const location = useLocation();
  const obj = {
    branchName: branchName
  };
  const [selectMenu, setSelectMenu] = useState('')
  // Update selectMenu based on pathname start
  useEffect(() => {
    const pathname = location.pathname;
    switch (pathname) {
      case '/branch':
        setSelectMenu('dashboard');
        break;
      case '/sessiondetails':
      case '/coursedetails':
      case '/productrequest':
      case '/batchdetails':
        setSelectMenu('managemaster');
        break;
      case '/registrationdata':
      case '/studentdetails':
      case '/studentAttaindance':
      case '/studentpayment':
        setSelectMenu('managestudent');
        break;
      case '/collection':
      case '/collectionreport':
        setSelectMenu('managepayment');
        break;
      case '/studentresult':
      case '/admitcard':
        setSelectMenu('questionexams');
        break;
      case '/studymateriall':
        setSelectMenu('studymaterial');
        break;
      case '/Studentreports':
        setSelectMenu('allreports');
        break;
    }
  }, [location.pathname]);
  // Update selectMenu based on pathname end
  const [anchorEl, setAnchorEl] = React.useState(null);
  // Event handlers for the First menu start
  const [openMenu1, setOpenMenu1] = useState(false);
  const [anchorElMenu1, setAnchorElMenu1] = useState(null);
  const handleClickMenu1 = (event) => {
    setOpenMenu1(true);
    setAnchorElMenu1(event.currentTarget);
  };

  const handleCloseMenu1 = () => {
    setOpenMenu1(false);
  };
  // Event handlers for the First menu end
  // Event handlers for the second menu
  const [openMenu2, setOpenMenu2] = useState(false);
  const [anchorElMenu2, setAnchorElMenu2] = useState(null);
  const handleClickMenu2 = (event) => {
    setAnchorElMenu2(event.currentTarget);
    setOpenMenu2(true);
    dispatch(getDataBrancWise(obj))
  };

  const handleCloseMenu2 = () => {
    setOpenMenu2(false);
  };
  const RegistrationData = () => {
    navigate('/registrationdata')
    setOpenMenu2(false);
  }
  const StudentDetails = () => {
    navigate('/studentdetails')
    setOpenMenu2(false);
  }
  // const MasterData = () => {
  //   navigate('/masterdata')
  //   setOpenMenu2(false);
  // }
  const StudentAttaindance = () => {
    navigate('/studentAttaindance')
    setOpenMenu2(false);
  }

  // Event handlers for the three menu
  const [anchorElMenu3, setAnchorElMenu3] = useState(null);
  const [openMenu3, setOpenMenu3] = useState(false);

  const handleClickMenu3 = (event) => {
    setAnchorElMenu3(event.currentTarget);
    setOpenMenu3(true);
  };

  const handleCloseMenu3 = () => {
    setAnchorElMenu3(null);
    setOpenMenu3(false);
  };

  // Event handlers for the four menu
  const [anchorElMenu4, setAnchorElMenu4] = useState(null);
  const [openMenu4, setOpenMenu4] = useState(false);

  const handleClickMenu4 = (event) => {
    setAnchorElMenu4(event.currentTarget);
    setOpenMenu4(true);
  };

  const handleCloseMenu4 = () => {
    setAnchorElMenu4(null);
    setOpenMenu4(false);
  };

  // Event handlers for the five menu
  const [anchorElMenu5, setAnchorElMenu5] = useState(null);
  const [openMenu5, setOpenMenu5] = useState(false);

  const handleClickMenu5 = (event) => {
    setAnchorElMenu5(event.currentTarget);
    setOpenMenu5(true);
  };

  const handleCloseMenu5 = () => {
    setAnchorElMenu5(null);
    setOpenMenu5(false);


  };
//  {/* Mobile vortion menu start */}
  const [anchorElMenu7, setAnchorElMenu7] = React.useState(null);
  const [openMenu7, setOpenMenu7] = React.useState(false);

  const handleClick = (event) => {
    setAnchorElMenu7(event.currentTarget);
    setOpenMenu7(true);
  };

  const handleClose = () => {
    setAnchorElMenu7(null);
    setOpenMenu7(false);
  };
// {/* Mobile vortion menu End */}


  const [anchorElMenu6, setAnchorElMenu6] = useState(null);
  const [openMenu6, setOpenMenu6] = useState(false);
  const [profileDialogOpen, setProfileDialogOpen] = useState(false);

  const handleProfileDialogOpen = () => {
    setProfileDialogOpen(true);
  };

  const handleProfileDialogClose = () => {
    setProfileDialogOpen(false);
  };

  const [openLogoutModal, setOpenLogoutModal] = useState(false)




  const handleClickMenu6 = (event) => {
    setAnchorElMenu6(event.currentTarget);
    setOpenMenu6(true);

  };

  const handleCloseMenu6 = () => {
    setAnchorElMenu6(null);
    setOpenMenu6(false);
  };
  const open = Boolean(anchorEl);


  const handleNavigate = () => {
    navigate('/branch')
    setOpenMenu1(false);
  }

  const SessionDetails = () => {
    navigate('/sessiondetails')
    setOpenMenu1(false);
  }
  const CourseDetails = () => {
    navigate('/coursedetails')
    setOpenMenu1(false);
  }
  const SubjectDetails = () => {
    navigate('/batchdetails')
    setOpenMenu1(false);
  }


  const FeeCollection = () => {
    navigate('/collection')
    setOpenMenu3(false);

  }
  const FeeCollectionReport = () => {
    navigate('/collectionreport')
    setOpenMenu3(false);

  }
  const AdmitCardDetails = () => {
    navigate('/admitcard')
    setOpenMenu4(false);
  }
  const Viewstudntresult = () => {
    navigate('/studentresult')
    setOpenMenu4(false);
  }
  const StudyMateriall = () => {
    navigate('/studymateriall')
    setOpenMenu5(false);
  }

  window.addEventListener('popstate', function (event) {
    if (document.location.pathname === '/login') {
      sessionStorage.clear();
    }
  });


  const handleLogout = () => {
    dispatch(logout());
    navigate('/');
  };
  const handleOpenLogoutModal = () => setOpenLogoutModal(true);
  const handleCloseLogoutModal = () => setOpenLogoutModal(false);
  const handleConfirmLogout = () => {
    handleLogout();
    handleCloseLogoutModal();
  };



  return (
    <Box sx={{ display: 'flex' }}>

      <AppBar position="fixed"
        sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }} style={{ backgroundImage: 'linear-gradient(to bottom, #dde3e9, #00000040)', color: 'white' }} >
        <Box style={{ width: '100%', overflowX: 'hidden' }} className="w-full   flex justify-between items-center">
          <div className="flex items-center">
            <img className={styles.img_responsive}
              src={sglogo}
              alt="Description"

            />
            <Button
              id="basic-button"
              aria-controls={openMenu7 ? 'basic-menu' : undefined}
              aria-haspopup="true"
              aria-expanded={openMenu7 ? 'true' : undefined}
              onClick={handleClick}
            >
              <IconButton sx={{ mr: 2, display: { lg:'none',xs: 'block' } }}>
                <MenuIcon />
              </IconButton>
            </Button>


          </div>

          {/* <div>Admin Address</div> */}

          <div className="text-white font-bold text-lg flex items-center">Branch Name:{branch.branchName}</div>
          {/* <CgProfile size={35} /> */}
          <div className="flex items-end">
            <Avatar
              alt="Remy Sharp"
              src={avtarlogo}
              sx={{ width: 40, height: 40 }}
            />
            {/* <IconButton onClick={handleOpenLogoutModal}>
            <LogoutIcon className="text-gray-200 cursor-pointer ml-2" />
          </IconButton> */}
            <IconButton onClick={handleProfileDialogOpen}>
              <MoreVertIcon className="text-gray-200 cursor-pointer " />
            </IconButton>
          </div>
        </Box>

      </AppBar>
      {/* Mobile vortion menu start */}
      <Drawer
        id="basic-menu"
        anchorEl={anchorElMenu7}
        open={openMenu7}
        onClose={handleClose}      
      >
        <Drawer
          variant="permanent"
          sx={{
            width: drawerWidth,
            flexShrink: 0,
            [`& .MuiDrawer-paper`]: {
              width: drawerWidth, boxSizing: 'border-box',
              backgroundImage: 'linear-gradient(to bottom, #1d5fa1, #dde3e9)'
            },
          }}
        >
          <Toolbar />
          <Box >
            <List >
              <Button
                className={styles.BtnOne}
                style={{ width: "100%", borderBlockEndWidth: 'white' }}
                aria-haspopup="true"
                aria-expanded={open ? 'true' : undefined}
                onClick={handleNavigate}
                sx={{
                  width: "100%",
                  backgroundColor: selectMenu === 'dashboard' ? '#fbfcfd' : '',
                  color: selectMenu === 'dashboard' ? 'black' : 'white',
                  '&:hover': {
                    backgroundColor: selectMenu === 'dashboard' ? 'white' : '', // Override hover background color
                    color: selectMenu === 'dashboard' ? 'black' : '',
                  },
                }}
              >
                My Dashboard
              </Button>
            </List>
            <List>

              <Button
                className={styles.BtnOne}
                style={{ width: '100%' }}
                id="basic-button"
                aria-controls={openMenu1 ? 'basic-menu-1' : undefined}
                aria-haspopup="true"
                // variant="contained"
                onClick={handleClickMenu1}
                endIcon={<ArrowDropDownIcon />}
                sx={{
                  width: "100%",
                  backgroundColor: selectMenu === 'managemaster' ? 'white' : '',
                  color: selectMenu === 'managemaster' ? 'black' : 'white',
                  '&:hover': {
                    backgroundColor: selectMenu === 'managemaster' ? 'white' : '', // Override hover background color
                    color: selectMenu === 'managemaster' ? 'black' : '',
                  },
                }}
              >
                Manage Master
              </Button>

              <Menu
                style={{ width: "100%" }}
                id="basic-menu-1"
                anchorEl={anchorElMenu1}
                open={openMenu1}
                onClose={handleCloseMenu1}
                MenuListProps={{
                  'aria-labelledby': 'basic-button',
                  style: {
                    width: 223,
                  },
                }}
              >
                <MenuItem onClick={SessionDetails}>Add Session Details</MenuItem>
                <MenuItem onClick={SubjectDetails}>Add Batch Details</MenuItem>
                <MenuItem onClick={CourseDetails}>Add  Course Details</MenuItem>
              </Menu>
            </List>
            <List>

              <Button
                className={styles.BtnOne}
                style={{ width: '100%' }}
                id="basic-button-one"
                aria-controls={openMenu2 ? 'basic-menu-2' : undefined}
                aria-haspopup="true"
                // variant="contained"
                onClick={handleClickMenu2}
                endIcon={<ArrowDropDownIcon />}
                sx={{
                  width: "100%",
                  backgroundColor: selectMenu === 'managestudent' ? 'white' : '',
                  color: selectMenu === 'managestudent' ? 'black' : 'white',
                  '&:hover': {
                    backgroundColor: selectMenu === 'managestudent' ? 'white' : '', // Override hover background color
                    color: selectMenu === 'managestudent' ? 'black' : '',
                  },
                }}
              >
                Manage Student
              </Button>


              <Menu
                id="basic-menu-2"
                anchorEl={anchorElMenu2}
                open={openMenu2}
                onClose={handleCloseMenu2}
                MenuListProps={{
                  'aria-labelledby': 'basic-button-one',
                  style: {
                    width: 223,
                  },
                }}
              >
                <MenuItem onClick={RegistrationData}>Student Registration</MenuItem>
                <MenuItem onClick={StudentDetails}>View All Student Details</MenuItem>
                {/* <MenuItem onClick={MasterData}> View Student Master Data</MenuItem> */}
                <MenuItem onClick={StudentAttaindance}>Student Attaindance</MenuItem>

              </Menu>
            </List>
            <List>

              <Button
                className={styles.BtnOne}
                style={{ width: '100%' }}
                id="basic-button-three"
                aria-controls={openMenu3 ? 'basic-menu-3' : undefined}
                aria-haspopup="true"
                // variant="contained"
                onClick={handleClickMenu3}
                endIcon={<ArrowDropDownIcon />}
                sx={{
                  width: "100%",
                  backgroundColor: selectMenu === 'managepayment' ? 'white' : '',
                  color: selectMenu === 'managepayment' ? 'black' : 'white',
                  '&:hover': {
                    backgroundColor: selectMenu === 'managepayment' ? 'white' : '', // Override hover background color
                    color: selectMenu === 'managepayment' ? 'black' : '',
                  },
                }}
              >
                Manage Payment
              </Button>

              <Menu
                id="basic-menu-3"
                anchorEl={anchorElMenu3}
                open={openMenu3}
                onClose={handleCloseMenu3}
                MenuListProps={{
                  'aria-labelledby': 'basic-button-three',
                  style: {
                    width: 223,
                  },
                }}
              >
                <MenuItem onClick={FeeCollection}>Fee Collection</MenuItem>
                <MenuItem onClick={FeeCollectionReport}>Fee Collection Reports</MenuItem>
              </Menu>
            </List>
            <List>

              <Button
                className={styles.BtnOne}
                style={{ width: '100%' }}
                id="basic-button-three"
                aria-controls={openMenu4 ? 'basic-menu-4' : undefined}
                aria-haspopup="true"
                // variant="contained"
                onClick={handleClickMenu4}
                endIcon={<ArrowDropDownIcon />}
                sx={{
                  width: "100%",
                  backgroundColor: selectMenu === 'questionexams' ? 'white' : '',
                  color: selectMenu === 'questionexams' ? 'black' : 'white',
                  '&:hover': {
                    backgroundColor: selectMenu === 'questionexams' ? 'white' : '', // Override hover background color
                    color: selectMenu === 'questionexams' ? 'black' : '',
                  },
                }}
              >
                Question & Exams
              </Button>

              <Menu
                id="basic-menu-4"
                anchorEl={anchorElMenu4}
                open={openMenu4}
                onClose={handleCloseMenu4}
                MenuListProps={{
                  'aria-labelledby': 'basic-button-three',
                  style: {
                    width: 223,
                  },
                }}
              >
                <MenuItem onClick={Viewstudntresult}>View Student Result</MenuItem>
                <MenuItem onClick={AdmitCardDetails}>Admit Card Details</MenuItem>
              </Menu>
            </List>
            <List>

              <Button
                className={styles.BtnOne}
                style={{ width: '100%' }}
                id="basic-button-three"
                aria-controls={openMenu5 ? 'basic-menu-5' : undefined}
                aria-haspopup="true"
                // variant="contained"
                onClick={handleClickMenu5}
                endIcon={<ArrowDropDownIcon />}
                sx={{
                  width: "100%",
                  backgroundColor: selectMenu === 'studymaterial' ? 'white' : '',
                  color: selectMenu === 'studymaterial' ? 'black' : 'white',
                  '&:hover': {
                    backgroundColor: selectMenu === 'studymaterial' ? 'white' : '', // Override hover background color
                    color: selectMenu === 'studymaterial' ? 'black' : '',
                  },
                }}
              >
                Study Material
              </Button>

              <Menu
                id="basic-menu-5"
                anchorEl={anchorElMenu5}
                open={openMenu5}
                onClose={handleCloseMenu5}
                MenuListProps={{
                  'aria-labelledby': 'basic-button-three',
                  style: {
                    width: 223,

                  },
                }}
              >
                <MenuItem onClick={StudyMateriall} >Study Material</MenuItem>
              </Menu>
            </List>

          </Box>
        </Drawer>
      </Drawer>
      {/* Mobile vortion menu End */}
      <Drawer
        className={styles.drower}
        variant="permanent"
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          [`& .MuiDrawer-paper`]: {
            width: drawerWidth, boxSizing: 'border-box',
            backgroundImage: 'linear-gradient(to bottom, #1d5fa1, #dde3e9)'
          },
        }}
      >
        <Toolbar />
        <Box sx={{ overflow: 'auto' }} className={styles.first}>
          <List >
            <Button
              className={styles.BtnOne}
              style={{ width: "100%", borderBlockEndWidth: 'white' }}
              aria-haspopup="true"
              aria-expanded={open ? 'true' : undefined}
              onClick={handleNavigate}
              sx={{
                width: "100%",
                backgroundColor: selectMenu === 'dashboard' ? '#fbfcfd' : '',
                color: selectMenu === 'dashboard' ? 'black' : 'white',
                '&:hover': {
                  backgroundColor: selectMenu === 'dashboard' ? 'white' : '', // Override hover background color
                  color: selectMenu === 'dashboard' ? 'black' : '',
                },
              }}
            >
              My Dashboard
            </Button>
          </List>
          <List>

            <Button
              className={styles.BtnOne}
              style={{ width: '100%' }}
              id="basic-button"
              aria-controls={openMenu1 ? 'basic-menu-1' : undefined}
              aria-haspopup="true"
              // variant="contained"
              onClick={handleClickMenu1}
              endIcon={<ArrowDropDownIcon />}
              sx={{
                width: "100%",
                backgroundColor: selectMenu === 'managemaster' ? 'white' : '',
                color: selectMenu === 'managemaster' ? 'black' : 'white',
                '&:hover': {
                  backgroundColor: selectMenu === 'managemaster' ? 'white' : '', // Override hover background color
                  color: selectMenu === 'managemaster' ? 'black' : '',
                },
              }}
            >
              Manage Master
            </Button>

            <Menu
              style={{ width: "100%" }}
              id="basic-menu-1"
              anchorEl={anchorElMenu1}
              open={openMenu1}
              onClose={handleCloseMenu1}
              MenuListProps={{
                'aria-labelledby': 'basic-button',
                style: {
                  width: 223,
                },
              }}
            >
              <MenuItem onClick={SessionDetails}>Add Session Details</MenuItem>
              <MenuItem onClick={SubjectDetails}>Add Batch Details</MenuItem>
              <MenuItem onClick={CourseDetails}>Add  Course Details</MenuItem>
            </Menu>
          </List>
          <List>

            <Button
              className={styles.BtnOne}
              style={{ width: '100%' }}
              id="basic-button-one"
              aria-controls={openMenu2 ? 'basic-menu-2' : undefined}
              aria-haspopup="true"
              // variant="contained"
              onClick={handleClickMenu2}
              endIcon={<ArrowDropDownIcon />}
              sx={{
                width: "100%",
                backgroundColor: selectMenu === 'managestudent' ? 'white' : '',
                color: selectMenu === 'managestudent' ? 'black' : 'white',
                '&:hover': {
                  backgroundColor: selectMenu === 'managestudent' ? 'white' : '', // Override hover background color
                  color: selectMenu === 'managestudent' ? 'black' : '',
                },
              }}
            >
              Manage Student
            </Button>


            <Menu
              id="basic-menu-2"
              anchorEl={anchorElMenu2}
              open={openMenu2}
              onClose={handleCloseMenu2}
              MenuListProps={{
                'aria-labelledby': 'basic-button-one',
                style: {
                  width: 223,
                },
              }}
            >
              <MenuItem onClick={RegistrationData}>Student Registration</MenuItem>
              <MenuItem onClick={StudentDetails}>View All Student Details</MenuItem>
              {/* <MenuItem onClick={MasterData}> View Student Master Data</MenuItem> */}
              <MenuItem onClick={StudentAttaindance}>Student Attaindance</MenuItem>

            </Menu>
          </List>
          <List>

            <Button
              className={styles.BtnOne}
              style={{ width: '100%' }}
              id="basic-button-three"
              aria-controls={openMenu3 ? 'basic-menu-3' : undefined}
              aria-haspopup="true"
              // variant="contained"
              onClick={handleClickMenu3}
              endIcon={<ArrowDropDownIcon />}
              sx={{
                width: "100%",
                backgroundColor: selectMenu === 'managepayment' ? 'white' : '',
                color: selectMenu === 'managepayment' ? 'black' : 'white',
                '&:hover': {
                  backgroundColor: selectMenu === 'managepayment' ? 'white' : '', // Override hover background color
                  color: selectMenu === 'managepayment' ? 'black' : '',
                },
              }}
            >
              Manage Payment
            </Button>

            <Menu
              id="basic-menu-3"
              anchorEl={anchorElMenu3}
              open={openMenu3}
              onClose={handleCloseMenu3}
              MenuListProps={{
                'aria-labelledby': 'basic-button-three',
                style: {
                  width: 223,
                },
              }}
            >
              <MenuItem onClick={FeeCollection}>Fee Collection</MenuItem>
              <MenuItem onClick={FeeCollectionReport}>Fee Collection Reports</MenuItem>
            </Menu>
          </List>
          <List>

            <Button
              className={styles.BtnOne}
              style={{ width: '100%' }}
              id="basic-button-three"
              aria-controls={openMenu4 ? 'basic-menu-4' : undefined}
              aria-haspopup="true"
              // variant="contained"
              onClick={handleClickMenu4}
              endIcon={<ArrowDropDownIcon />}
              sx={{
                width: "100%",
                backgroundColor: selectMenu === 'questionexams' ? 'white' : '',
                color: selectMenu === 'questionexams' ? 'black' : 'white',
                '&:hover': {
                  backgroundColor: selectMenu === 'questionexams' ? 'white' : '', // Override hover background color
                  color: selectMenu === 'questionexams' ? 'black' : '',
                },
              }}
            >
              Question & Exams
            </Button>

            <Menu
              id="basic-menu-4"
              anchorEl={anchorElMenu4}
              open={openMenu4}
              onClose={handleCloseMenu4}
              MenuListProps={{
                'aria-labelledby': 'basic-button-three',
                style: {
                  width: 223,
                },
              }}
            >
              <MenuItem onClick={Viewstudntresult}>View Student Result</MenuItem>
              <MenuItem onClick={AdmitCardDetails}>Admit Card Details</MenuItem>
            </Menu>
          </List>
          <List>

            <Button
              className={styles.BtnOne}
              style={{ width: '100%' }}
              id="basic-button-three"
              aria-controls={openMenu5 ? 'basic-menu-5' : undefined}
              aria-haspopup="true"
              // variant="contained"
              onClick={handleClickMenu5}
              endIcon={<ArrowDropDownIcon />}
              sx={{
                width: "100%",
                backgroundColor: selectMenu === 'studymaterial' ? 'white' : '',
                color: selectMenu === 'studymaterial' ? 'black' : 'white',
                '&:hover': {
                  backgroundColor: selectMenu === 'studymaterial' ? 'white' : '', // Override hover background color
                  color: selectMenu === 'studymaterial' ? 'black' : '',
                },
              }}
            >
              Study Material
            </Button>

            <Menu
              id="basic-menu-5"
              anchorEl={anchorElMenu5}
              open={openMenu5}
              onClose={handleCloseMenu5}
              MenuListProps={{
                'aria-labelledby': 'basic-button-three',
                style: {
                  width: 223,

                },
              }}
            >
              <MenuItem onClick={StudyMateriall} >Study Material</MenuItem>
            </Menu>
          </List>

        </Box>
      </Drawer>
      {/*  Logout Dialog Start*/}
      <Box sx={{display:'flex',justifyContent:'flex-end'}}>
      <Dialog
        open={profileDialogOpen}
        onClose={handleProfileDialogClose}
        
      >
        <DialogTitle>
          <IconButton
            aria-label="close"
            onClick={handleProfileDialogClose}
            sx={{
              position: 'absolute',
              right: 0,
              top: 0,
              color: (theme) => theme.palette.grey[500],
            }}
          >
            <CloseIcon style={{ color: 'red' }} />
          </IconButton>
        </DialogTitle>
        <DialogContent sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
          <AccountCircleIcon sx={{ fontSize: 64, color: '#2196f3' }} />
          <Button onClick={handleOpenLogoutModal} sx={{
            mt: 2, backgroundColor: 'rgba(39 92 144)', '&:hover': {
              backgroundColor: 'black',
            }
          }} className=" text-white px-4 py-2 ">
            LOGOUT
          </Button>
        </DialogContent>
      </Dialog>
      <Dialog
        
        open={openLogoutModal}
        onClose={handleCloseLogoutModal}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title" sx={{ textAlign: 'center' }}>{"Logout"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description" sx={{ textAlign: 'center', color: 'black' }}>
            Are you sure you want to logout?
          </DialogContentText>
        </DialogContent>
        <DialogActions sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', marginRight: '8%' }}>
          <Button onClick={handleCloseLogoutModal} sx={{
            mt: 2, backgroundColor: 'blue', '&:hover': {
              backgroundColor: 'black',
            }
          }} className=" text-white px-4 py-2 ">
            Cancel
          </Button>
          <Button onClick={handleConfirmLogout} autoFocus sx={{
            mt: 2, backgroundColor: 'blue', '&:hover': {
              backgroundColor: 'black',
            }
          }} className=" text-white px-4 py-2 ">
            Logout
          </Button>
        </DialogActions>
      </Dialog>
      </Box>
      {/*  Logout Dialog End*/}
      <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
      </Box>
    </Box >
  )
}

export default Navbar