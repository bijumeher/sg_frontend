import React from 'react';
import { Modal, Box, Typography, Divider, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 800,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
  overflow: 'hidden',
};

const ViewStudentDetailsModal = ({ open, handleClose, studentData }) => {
  const students = useSelector((state) => state.student.studentData);
  const navigate = useNavigate();

  if (!studentData) {
    return null;
  }
  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are zero-based
    const day = String(date.getDate()).padStart(2, '0');
    return `${day}-${month}-${year}`;
};

  const student = students.find(student => student._id === studentData._id);

  const handleActionClick = (payment) => {
    navigate('/paymentreceiptt', { state: { payment, student } });
  };


  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="student-details-modal-title"
      aria-describedby="student-details-modal-description"
    >
      <Box sx={style}>
        <Typography id="student-details-modal-title" variant="h6" component="h2">
          Student Payment Details
        </Typography>
        <Divider />
        <Box mt={2}>
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>#</TableCell>
                  <TableCell>Paid Amount</TableCell>
                  <TableCell>Rest Amount</TableCell>
                  <TableCell>Payment Mode</TableCell>
                  <TableCell>Payment Date</TableCell>
                  <TableCell>Download</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {studentData.payments.map((payment, index) => (
                  <TableRow key={index}>
                    <TableCell>{index + 1}</TableCell>
                    <TableCell>{payment.Paidamount}</TableCell>
                    <TableCell>{payment.RestAmount}</TableCell>
                    <TableCell>{payment.paymentMode}</TableCell>
                    <TableCell>{formatDate(payment.paymentDate)}</TableCell>
                    <TableCell>
                      <Button variant="contained" color="primary" onClick={() => handleActionClick(payment)}>
                        Click
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
      </Box>
    </Modal>
  );
};

export default ViewStudentDetailsModal;
