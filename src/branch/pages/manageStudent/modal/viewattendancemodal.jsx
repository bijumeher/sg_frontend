import React from 'react';
import { Modal, Box, Typography, Divider } from '@mui/material';
import AttendancePieChart from '../attenpie'; // Adjust the import path as needed

const modalStyle = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 600, // Adjusted width to fit the pie chart
  maxHeight: '80vh',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
  overflowY: 'hidden', // Vertical scrolling
  overflowX: 'hidden', // Prevent horizontal scrolling
};

const ViewAttendanceDetailsModal = ({ open, handleClose, attendanceData }) => {
  if (!attendanceData) {
    return null;
  }

  // Function to aggregate attendance data by subject
  const aggregateAttendanceBySubject = (attendances) => {
    const subjectMap = new Map();

    attendances.forEach((attendance) => {
      const { subjectname, attendancestatus } = attendance;
      if (!subjectMap.has(subjectname)) {
        subjectMap.set(subjectname, { presentCount: 0, absentCount: 0 });
      }
      const subjectData = subjectMap.get(subjectname);
      if (attendancestatus === 'present') {
        subjectData.presentCount += 1;
      } else if (attendancestatus === 'absent') {
        subjectData.absentCount += 1;
      }
    });

    return Array.from(subjectMap, ([subjectname, data]) => ({
      subjectname,
      presentCount: data.presentCount,
      absentCount: data.absentCount,
    }));
  };

  const aggregatedData = aggregateAttendanceBySubject(attendanceData.attendances);

  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="attendance-details-modal-title"
      aria-describedby="attendance-details-modal-description"
    >
      <Box sx={modalStyle}>
        <Typography id="attendance-details-modal-title" variant="h6" component="h2">
          Attendance Details
        </Typography>
        <Divider />
        <Box mt={2}>
          <Typography variant="body2">Student Name: {attendanceData.studentName}</Typography>
          <Typography variant="body2">Course Name: {attendanceData.courseName}</Typography>  
          <Box sx={{ overflow: 'hidden', display: 'flex', justifyContent: 'center', mt: 2 }}> {/* Added margin-top */}
            <AttendancePieChart aggregatedData={aggregatedData} />
          </Box>
        </Box>
      </Box>
    </Modal>
  );
};

export default ViewAttendanceDetailsModal;
