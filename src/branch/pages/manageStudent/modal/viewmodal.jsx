import React from 'react';
import { Modal, Box, Typography, Divider } from '@mui/material';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const ViewStudentDetailsModal = ({ open, handleClose, studentData }) => {
    if (!studentData) {
        return null; 
      }
      const formatDate = (dateString) => {
        const date = new Date(dateString);
        const year = date.getFullYear();
        const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are zero-based
        const day = String(date.getDate()).padStart(2, '0');
        return `${day}-${month}-${year}`;
    };
    const courseNames = Array.isArray(studentData.courseName)
    ? studentData.courseName.join(', ')
    : studentData.courseName;
  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="student-details-modal-title"
      aria-describedby="student-details-modal-description"
    >
      <Box sx={style}>
        <Typography id="student-details-modal-title" variant="h6" component="h2">
          Student Details
        </Typography>
        <Divider />
        <Box mt={2}>
          <Typography variant="body2">Name: {studentData.studentName}</Typography>
          <Typography variant="body2">Father Name: {studentData.fatherName}</Typography>
          <Typography variant="body2">Gender: {studentData.gender}</Typography>
          <Typography variant="body2">Caste: {studentData.caste}</Typography>
          <Typography variant="body2">Email: {studentData.email}</Typography>
          <Typography variant="body2">Adhar No: {studentData.aadharNumber}</Typography>
          <Typography variant="body2">Course Name: {courseNames}</Typography>
          <Typography variant="body2">Mobile No: {studentData.mobNumber}</Typography>
          <Typography variant="body2">Reg ID: {studentData.registrationId}</Typography>
          <Typography variant="body2">Date of Regd: {studentData.dateOfRegister}</Typography>
          <Typography variant="body2">Branch Name: {studentData.branchName}</Typography>
          <Typography variant="body2">Batch Name: {studentData.batchName}</Typography>
          <Typography variant="body2" mt={2}>
            <strong>Payment Details:</strong>
          </Typography>
          {studentData.payments.map((payment, index) => (
            <Typography key={index} variant="body2">
            Paidamount:  {payment.Paidamount},
            RestAmount: {payment.RestAmount},
            paymentMode: {payment.paymentMode},
            paymentDate:{formatDate(payment.paymentDate)}
            </Typography>
          ))}
        </Box>
      </Box>
    </Modal>
  );
};

export default ViewStudentDetailsModal;
