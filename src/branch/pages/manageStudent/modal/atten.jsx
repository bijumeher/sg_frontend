import React from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import styles from './style.module.css'; // Import the CSS module

const AttendanceCalendar = ({ attendanceData }) => {
  // Deduplicate attendanceData
  const deduplicatedData = Array.from(new Map(
    attendanceData.map(item => [new Date(item.attendancedate).toDateString(), item])
  ).values());

  // Debugging: Check the deduplicated data
  console.log('Deduplicated Data:', deduplicatedData);

  // Calculate absent days and percentage
  const totalDays = deduplicatedData.length;
  const absentDays = deduplicatedData.filter(attendance => attendance.attendancestatus === 'absent').length;
  const absentPercentage = (totalDays > 0 && absentDays > 0) ? (absentDays / totalDays) * 100 : 0; // Calculate percentage, default to 0

  // Debugging: Check the calculated values
  console.log('Total Days:', totalDays);
  console.log('Absent Days:', absentDays);
  console.log('Absent Percentage:', absentPercentage);

  const getTileContent = ({ date, view }) => {
    if (view === 'month') {
      // Find all attendance records for the given date
      const attendancesForDate = deduplicatedData.filter(
        (attendance) => new Date(attendance.attendancedate).toDateString() === date.toDateString()
      );

      if (attendancesForDate.length > 0) {
        return (
          <div className={styles['attendance-status-wrapper']}>
            {attendancesForDate.map((attendance, index) => {
              const statusClass =
                attendance.attendancestatus === 'present' ? styles.present : styles.absent;

              return (
                <div key={index} className={`${styles['attendance-status']} ${statusClass}`}>
                  <span className={styles['status-icon']}>
                    {attendance.attendancestatus === 'present' ? (
                      <>&#10003;</> // Green check mark
                    ) : (
                      <>&#x2717;</> // Red cross mark
                    )}
                  </span>
                </div>
              );
            })}
          </div>
        );
      }
    }
    return null;
  };

  const tileClassName = ({ date, view }) => {
    if (view === 'month') {
      // Find attendance record for the given date
      const attendanceForDate = deduplicatedData.find(
        (attendance) => new Date(attendance.attendancedate).toDateString() === date.toDateString()
      );

      if (attendanceForDate) {
        // Return class based on attendance status
        return attendanceForDate.attendancestatus === 'present'
          ? styles['present-day']
          : styles['absent-day'];
      }
    }
    return null;
  };

  return (
    <div className={styles['attendance-calendar']}>
      <Calendar
        tileContent={getTileContent}
        tileClassName={tileClassName}
      />
      {/* Conditionally render the absent percentage only if it's greater than 0 */}
      {absentDays > 0 && (
        <div className={styles['absent-percentage']}>
          Absent: {absentPercentage.toFixed(1)}%
        </div>
      )}
    </div>
  );
};

export default AttendanceCalendar;
