import React, { useState } from 'react';
import { Modal, Box, Typography, IconButton } from '@mui/material';
import EditAttendanceModal from './editattendancemodal';
import { MdDelete, MdEdit, MdClose } from 'react-icons/md';
import { useDispatch } from 'react-redux';
import { AttendanceDataDelete, updateAttendanceData } from '../../../../actions/student/attendanceAction';
import { getDataBrancWise } from '../../../../actions/student/studentAction';
import DeleteDialog from '../../deleteModal/coursDeleteModal';

const ViewAttendanceDetails = ({ open, onClose, attendanceDetails }) => {
    const dispatch = useDispatch();
    const [editModalOpen, setEditModalOpen] = useState(false);
    const [selectedAttendance, setSelectedAttendance] = useState(null);
    const [deleteOpen, setDeleteOpen] = useState(false);

    const branch = JSON.parse(sessionStorage.getItem('branchData'));
    const branchName = branch.branchName;
    const [deleteAttendanceId, setDeleteAttendanceId] = useState(null);

    const handelOrderDialog = (attendanceId) => {
        setDeleteAttendanceId(attendanceId);
        setDeleteOpen(true);
    };
    const obj = { branchName: branchName };

    const handleEditModalopen = (attendance) => {
        setSelectedAttendance(attendance);
        setEditModalOpen(true);
    };

    const handleEditModalClose = () => {
        setEditModalOpen(false);
        setSelectedAttendance(null);
    };

    const handleUpdate = (updatedAttendance) => {
        dispatch(updateAttendanceData({ ...updatedAttendance, attendanceId: selectedAttendance._id })).then(() => {
            setEditModalOpen(false);
            setSelectedAttendance(null);
            dispatch(getDataBrancWise(obj));
            onClose();
        });
    };

    const handleDelete = (attendanceId) => {
        dispatch(AttendanceDataDelete(attendanceId)).then(() => {
            setDeleteOpen(false);
            dispatch(getDataBrancWise(obj));
            onClose();
        });
    };

    if (!attendanceDetails) {
        return null;
    }

    return (
        <Modal open={open} onClose={onClose} sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <Box sx={{
                padding: 4,
                backgroundColor: 'white',
                borderRadius: 2,
                width: '40%',
                maxHeight: '70vh',
                overflow: 'hidden',
                position: 'relative'
            }}>
                {/* Close Icon */}
                <IconButton
                    onClick={onClose}
                    sx={{ position: 'absolute', top: 8, right: 8 }}
                >
                    <MdClose />
                </IconButton>

                <Typography variant="h6" gutterBottom>Attendance Details</Typography>
                <div className="flex flex-col space-y-4" style={{ maxHeight: '55vh', overflowY: 'auto' }}>
                    <div>
                    {attendanceDetails.length > 0 ? (
                        <table className="min-w-full bg-white border border-zinc-300">
                            <thead className="bg-zinc-100">
                                <tr>
                                    <th className="px-4 py-2 border">Date</th>
                                    <th className="px-4 py-2 border">Status</th>
                                    <th className="px-4 py-2 border">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {attendanceDetails.map((student, index) => (
                                    <tr key={index}>
                                        <td className="px-4 py-2 border">{student.attendancedate}</td>
                                        <td className="px-4 py-2 border">{student.attendancestatus}</td>
                                        <td className="px-4 py-2 border">
                                            <button
                                                onClick={() => handleEditModalopen(student)}
                                            >
                                                <MdEdit />
                                            </button>
                                            <button
                                                onClick={() => handelOrderDialog(student._id)}
                                            >
                                                <MdDelete color='red' />
                                            </button>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    ) : (
                        <Typography variant="body2" color="textSecondary" align="center" sx={{ mt: 2 }}>
                            Select Subject
                            No attendance records found for the selected subject.
                        </Typography>
                    )}
                    </div>
                </div>
                <EditAttendanceModal
                    open={editModalOpen}
                    handleClose={handleEditModalClose}
                    initialData={selectedAttendance}
                    handleUpdate={handleUpdate}
                />
                <DeleteDialog
                    open={deleteOpen}
                    handleClose={() => setDeleteOpen(false)}
                    handleDelete={() => handleDelete(deleteAttendanceId)}
                    title="Are you sure you want to delete this attendance?"
                />
            </Box>
        </Modal>
    );
};

export default ViewAttendanceDetails;
