import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField, Typography } from '@mui/material';
import React, { useState, useEffect } from 'react';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';

const EditAttendanceModal = ({ open, handleClose, initialData, handleUpdate }) => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

  const [attendanceData, setAttendanceData] = useState({
    attendancedate: '',
    attendancestatus: '',
    subjectname: '',
  });

  useEffect(() => {
    if (initialData) {
      setAttendanceData(initialData);
    }
  }, [initialData]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setAttendanceData((prevData) => ({ ...prevData, [name]: value }));
  };

  const handleSubmit = () => {
    handleUpdate(attendanceData);
  };

  return (
    <Dialog
      fullScreen={fullScreen}
      open={open}
      onClose={handleClose}
      aria-labelledby="edit-attendance-title"
    >
      <DialogTitle id="edit-attendance-title">
        <Typography variant="h6">Edit Attendance</Typography>
      </DialogTitle>
      <DialogContent>
        <TextField
          required
          fullWidth
          margin="normal"
          label="Date"
          name="attendancedate"
          type="date"
          InputLabelProps={{ shrink: true }}
          value={attendanceData.attendancedate}
          onChange={handleChange}
        />
        <TextField
          required
          fullWidth
          margin="normal"
          label="Status"
          select
          SelectProps={{ native: true }}
          name="attendancestatus"
          value={attendanceData.attendancestatus}
          onChange={handleChange}
        >
          <option value="">Choose Status</option>
          <option value="present">Present</option>
          <option value="absent">Absent</option>
        </TextField>
        <TextField
          required
          fullWidth
          margin="normal"
          label="Subject"
          name="subjectname"
          value={attendanceData.subjectname}
          onChange={handleChange}
        />
      </DialogContent>
      <DialogActions>
        <Button variant="outlined" color="primary" onClick={handleClose}>
          Cancel
        </Button>
        <Button variant="contained" color="primary" onClick={handleSubmit}>
          Update
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default EditAttendanceModal;
