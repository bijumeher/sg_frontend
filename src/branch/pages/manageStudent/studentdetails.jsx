import { Box, Drawer, Toolbar, Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TablePagination } from '@mui/material';
import { MdEdit, MdDelete, MdVisibility } from 'react-icons/md';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import DeleteDialog from '../deleteModal/coursDeleteModal';
import StudentDetailsEditModal from '../editModal/StudentModal';
import { getDataBrancWise, deleteStudentData, updateStudentData, DeletePaymentData, getPaymentAlldata } from '../../../actions/student/studentAction';
import ViewStudentDetailsModal from './modal/viewmodal';
import { Card } from 'react-bootstrap';
const drawerWidth = 240;

const Studentdetails = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const studentfetch = useSelector(state => state.student.studentData);
  const branch = JSON.parse(sessionStorage.getItem('branchData'));
  const branchName = branch.branchName;
  const obj = { branchName: branchName };
  console.log(studentfetch);

  const [OrderDialog, setOrderDialog] = useState(false);
  const [editModal, setEditModal] = useState(false);
  const [currentStudent, setCurrentStudent] = useState(null);
  const [deleteStudentId, setDeleteStudentId] = useState(null);
  const [viewModal, setViewModal] = useState(false);
  const [searchQuery, setSearchQuery] = useState('');
  const [OrderDialogs, setOrderDialogs] = useState(false);
  const [deletePaymentId, setDeletePaymentId] = useState(null);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const filteredStudents = studentfetch?.filter(student => {
    const searchString = searchQuery.toLowerCase();
    return (
      student.studentName.toLowerCase().includes(searchString)

    );
  });

  const paginatedStudents = filteredStudents?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);


  const handelOrderDialog = (id) => {
    setDeleteStudentId(id);
    setOrderDialog(true);
  };
  const handelOrderDialogs = (id) => {
    setDeletePaymentId(id);
    setOrderDialogs(true);

  };


  const handelEditModal = (studentData) => {
    setCurrentStudent(studentData);
    setEditModal(true);
  };

  const handleCloseEditModal = () => {
    setEditModal(false);
    setCurrentStudent(null);
  };

  const handleUpdate = (updatedStudent) => {
    dispatch(updateStudentData({ ...updatedStudent, studentId: currentStudent._id })).then(() => {
      setEditModal(false);
      setCurrentStudent(null);
      dispatch(getDataBrancWise(obj));
    });
  };

  const handleDelete = (id) => {
    dispatch(deleteStudentData(id))
      .then(() => {
        setOrderDialog(false);
        dispatch(getDataBrancWise(obj));

      });
  };

  const handleSearch = (e) => {
    setSearchQuery(e.target.value);
  };
  const formatDateToInput = (dateString) => {
    const date = new Date(dateString);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are zero-based
    const day = String(date.getDate()).padStart(2, '0');
    return `${day}-${month}-${year}`;
  };

  const handleNavigateToMasterdata = (studentId) => {
    navigate(`/Payment/${studentId}`);
  };
  const handleViewModal = (studentData) => {
    setCurrentStudent(studentData);
    setViewModal(true);
  };

  const handleCloseViewModal = () => {
    setViewModal(false);
    setCurrentStudent(null);
  };
  useEffect(() => {
    dispatch(getDataBrancWise(obj));
  }, [dispatch]);

  const handleDeletes = (paymentId) => {
    dispatch(DeletePaymentData(paymentId)).then(() => {
      setOrderDialogs(false);
      // dispatch(getPaymentAlldata());
      dispatch(getDataBrancWise(obj));

    });
  };

  return (
    <Box sx={{ display: 'flex' }}>
      <Drawer
        variant="permanent"
        sx={{
          width: drawerWidth,
          display: { lg:'block',xs: 'none' }
        }}
      >
        {/* Add Drawer content here */}
      </Drawer>

      <Box component="main" sx={{ flexGrow: 1 }} className='mt-[-22px]'>
        <Toolbar />
        <div className="p-4">
          <div className="text-2xl text-zinc-600 mb-4">
            <a href="#" className="text-blue-600">🏠Home</a> /
            <a href="#" className="text-blue-600">Manage Student</a> / <span>View All Student Details</span>
          </div>

          <div className="bg-gradient-to-r from-red-500 to-yellow-500 text-white text-xl font-bold p-4 rounded">
            View All Student Details
          </div>
          <div className="mt-4 flex items-center">
            <input
              type="text"
              placeholder="Type Candidate Name or Mobile Number"
              className="border p-2 flex-grow rounded-l"
              value={searchQuery}

              onChange={handleSearch}
            />
            <button className="bg-blue-500 text-white p-2 rounded-r">Search</button>
          </div>
          <div className=" mt-4" style={{ marginTop: '16px', overflowX: 'auto' }}>
            <TableContainer sx={{ maxWidth: '100%', overflowX: 'hidden' }}>
              <Table className="min-w-full border-collapse border border-zinc-300">
                <TableHead>
                  <TableRow>
                    <TableCell sx={{ border: '1px solid #ccc' }}>Sl No</TableCell>
                    <TableCell sx={{ border: '1px solid #ccc' }}>Student Name</TableCell>
                    <TableCell sx={{ border: '1px solid #ccc' }}>Father Name</TableCell>
                    <TableCell sx={{ border: '1px solid #ccc' }}>Mother Name</TableCell>
                    <TableCell sx={{ border: '1px solid #ccc' }}>Gender</TableCell>
                    <TableCell sx={{ border: '1px solid #ccc' }}>Caste</TableCell>
                    <TableCell sx={{ border: '1px solid #ccc' }}>Email ID</TableCell>
                    <TableCell sx={{ border: '1px solid #ccc' }}>Adhar No</TableCell>
                    <TableCell sx={{ border: '1px solid #ccc' }}>Course Name</TableCell>
                    <TableCell sx={{ border: '1px solid #ccc' }}>Mobile No</TableCell>
                    <TableCell sx={{ border: '1px solid #ccc' }}>Reg ID</TableCell>
                    <TableCell sx={{ border: '1px solid #ccc' }}>Date of Regd</TableCell>
                    <TableCell sx={{ border: '1px solid #ccc' }}>Branch Name</TableCell>
                    <TableCell sx={{ border: '1px solid #ccc' }}>Batch Name</TableCell>
                    <TableCell sx={{ border: '1px solid #ccc' }}>Payment</TableCell>
                    <TableCell sx={{ border: '1px solid #ccc' }}>Payment status</TableCell>
                    <TableCell sx={{ border: '1px solid #ccc' }}>Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {paginatedStudents?.map((student, index) => (
                    <TableRow key={student._id}>
                      <TableCell sx={{ border: '1px solid #ccc' }}>{index + 1}</TableCell>
                      <TableCell sx={{ border: '1px solid #ccc' }}>{student.studentName}</TableCell>
                      <TableCell sx={{ border: '1px solid #ccc' }}>{student.fatherName}</TableCell>
                      <TableCell sx={{ border: '1px solid #ccc' }}>{student.motherName}</TableCell>
                      <TableCell sx={{ border: '1px solid #ccc' }}>{student.gender}</TableCell>
                      <TableCell sx={{ border: '1px solid #ccc' }}>{student.caste}</TableCell>
                      <TableCell sx={{ border: '1px solid #ccc' }} style={{ maxWidth: '100px', overflow: 'auto' }}>{student.email}</TableCell>
                      <TableCell sx={{ border: '1px solid #ccc' }}>{student.aadharNumber}</TableCell>
                      <TableCell sx={{ border: '1px solid #ccc' }}>
                        {Array.isArray(student.courseName) ? student.courseName.join(', ') : student.courseName}
                      </TableCell>

                      <TableCell sx={{ border: '1px solid #ccc' }}>{student.mobNumber}</TableCell>
                      <TableCell sx={{ border: '1px solid #ccc' }}>{student.registrationId}</TableCell>
                      <TableCell sx={{ border: '1px solid #ccc' }}>{student.dateOfRegister}</TableCell>
                      <TableCell sx={{ border: '1px solid #ccc' }}>{student.branchName}</TableCell>
                      <TableCell sx={{ border: '1px solid #ccc' }}>{student.batchName}</TableCell>
                      <TableCell sx={{ border: '1px solid #ccc' }}>

                        <Button
                          className='w-[100px] flex gap-2 hover:scale-105 duration-500' variant="contained"
                          onClick={() => handleNavigateToMasterdata(student._id)}
                        >
                          ADD

                        </Button>
                      </TableCell>
                      <TableCell sx={{ border: '1px solid #ccc' }}>
                        {student.payments.map((payment) => (
                          <TableCell className="border border-zinc-300 p-2" style={{ flexDirection: 'column', display: 'flex', height: '50px', width: '130px', }}> {formatDateToInput(payment.paymentDate)}
                            {/* <button
                            className="bg-red-600 text-white px-2 py-1 rounded shadow"
                            
                           
                          > */}
                            <MdDelete size={50} style={{ width: '28px' }} className="bg-red-600 text-white px-2 py-1 rounded shadow" onClick={() => handelOrderDialogs(payment._id)} />
                            {/* </button> */}
                          </TableCell>
                        )
                        )}


                      </TableCell>
                      <TableCell className="border border-zinc-300 p-2">
                        <div className="flex space-x-2 justify-center">
                          <button
                            className="bg-green-500 text-white p-1 rounded"
                            onClick={() => handelEditModal(student)}
                          >
                            <MdEdit />
                          </button>
                          <button
                            className="bg-red-500 text-white p-1 rounded"
                            onClick={() => handelOrderDialog(student._id)}
                          >
                            <MdDelete />
                          </button>
                          <button onClick={() => handleViewModal(student)}>
                            <MdVisibility className="text-green-600" />
                          </button>
                        </div>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 50]}
              component="div"
              count={filteredStudents?.length || 0}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </div>
        </div>
      </Box>

      <DeleteDialog
        open={OrderDialog}
        handleClose={() => setOrderDialog(false)}
        handleDelete={() => handleDelete(deleteStudentId)}
        title="Are you sure you want to delete this student data?"
      />
      <DeleteDialog
        open={OrderDialogs}
        handleClose={() => setOrderDialogs(false)}
        handleDelete={() => handleDeletes(deletePaymentId)}
        title="Are you sure you want to delete this payment?"
      />

      {currentStudent && (
        <StudentDetailsEditModal
          open={editModal}
          handleClose={handleCloseEditModal}
          initialData={currentStudent}
          handleUpdate={handleUpdate}
        />

      )}
      <ViewStudentDetailsModal
        open={viewModal}
        handleClose={handleCloseViewModal}
        studentData={currentStudent}
      />
    </Box>
  );
};

export default Studentdetails;
