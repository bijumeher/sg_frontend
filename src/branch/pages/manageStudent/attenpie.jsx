import React from 'react';
import { PieChart, Pie, Cell, Tooltip, Legend } from 'recharts';

const COLORS = ['#0088FE', '#FF8042', '#00C49F', '#FFBB28']; // Add more colors if needed

const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, value }) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * Math.PI / 180);
  const y = cy + radius * Math.sin(-midAngle * Math.PI / 180);

  return (
    <text x={x} y={y} fill="white" textAnchor="middle" dominantBaseline="central">
      {`${value.toFixed(1)}%`}
    </text>
  );
};

const AttendancePieChart = ({ aggregatedData }) => {
  // Prepare data for the pie chart
  const data = aggregatedData.flatMap((subjectData) => {
    const totalRecords = subjectData.presentCount + subjectData.absentCount;

    if (totalRecords === 0) {
      return []; // Avoid adding subjects with no attendance data
    }

    const percentagePresent = (subjectData.presentCount / totalRecords) * 100;
    const percentageAbsent = (subjectData.absentCount / totalRecords) * 100;

    return [
      {
        name: `${subjectData.subjectname} - Present`,
        value: percentagePresent
      },
      {
        name: `${subjectData.subjectname} - Absent`,
        value: percentageAbsent
      }
    ];
  });

  const chartStyles = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%'
  };

  return (
    <div style={chartStyles}>
      <PieChart width={600} height={400}>
        <Pie
          data={data}
          cx={300}
          cy={170}
          labelLine={false}
          outerRadius={150}
          fill="#8884d8"
          dataKey="value"
          label={renderCustomizedLabel} // Only show percentage labels
        >
          {data.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
          ))}
        </Pie>
        <Tooltip formatter={(value, name) => {
          const [subject, status] = name.split(' - ');
          return [`${value.toFixed(1)}%`, `${subject} - ${status}`]; // Show subject name with percentage on hover
        }} />
        <Legend />
      </PieChart>
    </div>
  );
};

export default AttendancePieChart;
