import { Box, Drawer, Toolbar } from '@mui/material';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { getDataBrancWise } from '../../../actions/student/studentAction';

const drawerWidth = 240;

const Masterdata = () => {
  const dispatch = useDispatch();
  const { studentId } = useParams(); 
  const students = useSelector((state) => state.student.studentData);
  const branch = JSON.parse(sessionStorage.getItem('branchData'));
  const branchName = branch?.branchName;

  const student = students.find(student => student._id === studentId);

  useEffect(() => {
    const obj = { branchName: branchName };
    dispatch(getDataBrancWise(obj));
  }, [dispatch, branchName]);

  if (!student) {
    return (
      <div>
        Loading...
      </div>
    );
  }

  return (
    <Box sx={{ display: 'flex' }}>
      <Drawer
        variant="permanent"
        sx={{
          width: drawerWidth,
          display: { lg:'block',xs: 'none' }
        }}
      >
        {/* Add Drawer content here if needed */}
      </Drawer>

      <Box component="main" sx={{ flexGrow: 1 }} className='mt-[-22px]'>
        <Toolbar />
        <div className="p-4">
          <div className="text-2xl text-zinc-600 mb-4">
            <a href="#" className="text-blue-600">🏠Home</a> /
            <a href="#" className="text-blue-600">Manage Student</a> / <span>Master Data</span>
          </div>

          <div className="bg-gradient-to-r from-red-500 to-yellow-500 text-white text-xl font-bold p-4 rounded">
            Master Data for {student.studentName}
          </div>

          <div className="mt-4">
            <h2 className="text-lg font-bold mb-2">Student Details</h2>
            <div className="grid grid-cols-2 gap-x-4">
              <div className="mb-4">
                <label className="block mb-1">Student Name</label>
                <div>{student.studentName}</div>
              </div>
             
             
             
              <div className="mb-4">
                <label className="block mb-1">Course Name</label>
                <div>{student.courseName}</div>
              </div>
              <div className="mb-4">
                <label className="block mb-1">Course Fee</label>
                <div>{student.courseFees}</div>
              </div>
              <div className="mb-4">
                <label className="block mb-1">Course Duration</label>
                <div>{student.courseDuration}</div>
              </div>
             
              <div className="mb-4">
                <label className="block mb-1">Registration ID</label>
                <div>{student.registrationId}</div>
              </div>
              
              
             
            </div>
          </div>
        </div>
      </Box>
    </Box>
  );
};

export default Masterdata;
