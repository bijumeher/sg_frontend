import React, { useEffect, useState } from 'react';
import { Box, Toolbar, Drawer, Button, Modal } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { getDataBrancWise } from '../../../actions/student/studentAction';
import { createAttendance, getattendanceAlldata } from '../../../actions/student/attendanceAction';
import ViewAttendanceDetailsModal from './modal/viewattendancemodal';
import { MdVisibility } from 'react-icons/md';
import { toast, Toaster } from "react-hot-toast";
import { getBranchDataByid } from '../../../actions/branch/branchAction';
import ViewAttendanceDetails from './modal/viewatten';
import { getAllsessionData } from '../../../actions/branch/sessionAction';
const drawerWidth = 240;

const StudentAttendancePage = () => {
  const [selectedStudentId, setSelectedStudentId] = useState('');
  const [subjects, setSubjects] = useState([]);
  const [viewAttendanceData, setViewAttendanceData] = useState(null);
  const [viewModalOpen, setViewModalOpen] = useState(false);
  const [viewAttendanceDatas, setViewAttendanceDatas] = useState(null);
  const [viewModalOpens, setViewModalOpens] = useState(false);
  const [openAddModal, setOpenAddModal] = useState(false);
  const [currentStudent, setCurrentStudent] = useState(null);

  const dispatch = useDispatch();
  const students = useSelector((state) => state.student.studentData);
  const branch = JSON.parse(sessionStorage.getItem('branchData'));
  const branchName = branch.branchName;
  const obj = { branchName: branchName };
  console.log(students);
  const allSessions = useSelector((state) => state.session.sessionData);
  console.log(allSessions);

  const [studentateendata, setStudentAteenData] = useState({
    attendancedate: '',
    attendancestatus: '',
    subjectname: '',
    studentId: '',
  });

  useEffect(() => {
    dispatch(getDataBrancWise(obj));
  }, [dispatch]);

  useEffect(() => {
    if (allSessions && Array.isArray(allSessions)) {
      const sessionNames = allSessions.map(session => session.sessionName);
      setSubjects(sessionNames);
    }
  }, [allSessions]);

  useEffect(() => {
    if (selectedStudentId) {
      setStudentAteenData((prevData) => ({
        ...prevData,
        studentId: selectedStudentId,
      }));
    }
  }, [selectedStudentId]);
  const branchData = JSON.parse(sessionStorage.getItem('branchData'));

  useEffect(() => {
    dispatch(getBranchDataByid({ branchId: branchData._id }))
  }, [dispatch])



  const branchesdata = useSelector(state => state.masterbranc.fetchbranch);
  console.log(branchesdata);

  const handleChange = (event) => {
    const { name, value } = event.target;

    if (name === 'studentId') {
      setSelectedStudentId(value);
    }
    else {
      setStudentAteenData((prevData) => ({
        ...prevData,
        [name]: value,
      }));
    }
  };



  const [selectedSubject, setSelectedSubject] = useState(''); // State to store the selected subject
  useEffect(() => {
    if (students && students.attendances) {
      const subjects = [...new Set(students.attendances.map(record => record.subjectname))];
      if (subjects.length > 0) {
        setSelectedSubject(subjects[0]); // Default to the first subject if available
      }
    }
  }, [students]);
  const handleSubjectChange = (e) => {
    console.log('Subject selected:', e.target.value);
    setSelectedSubject(e.target.value);
  };

  const handleViewAttendance = (student) => {
    setViewAttendanceData(student);
    setViewModalOpen(true);
    dispatch(getDataBrancWise(obj));
  };

  const handleCloseViewModal = () => {
    setViewModalOpen(false);
    setViewAttendanceData(null);
  };

  const handleViewAttendanceDetails = (student) => {

    console.log('Selected Subject:', selectedSubject);
    if (!selectedSubject) {
      toast.error("Please select a subject before viewing details.");
      return;
    }
    console.log('All Attendance Records:', student.attendances);

    const filteredAttendance = student.attendances.filter(
      (record) => record.subjectname === selectedSubject
    );

    console.log('Filtered Attendance:', filteredAttendance);

    setViewAttendanceDatas(filteredAttendance);
    setViewModalOpens(true);
  };
  const handleAddAttendance = () => {
    if (studentateendata.studentId && studentateendata.attendancedate && studentateendata.subjectname) {
      dispatch(createAttendance(studentateendata)).then(() => {
        setStudentAteenData({
          attendancedate: '',
          attendancestatus: '',
          subjectname: '',
          studentId: '',
        });
        setOpenAddModal(false);
        dispatch(getattendanceAlldata());
        dispatch(getDataBrancWise(obj));

      });
    }
  };
  useEffect(() => {
    dispatch(getAllsessionData());
  }, [dispatch]);

  const handleCloseViewModals = () => {
    setViewModalOpens(false);
    setViewAttendanceDatas(null);
  };
  const handleAddButtonClick = (student) => {
    setCurrentStudent(student);
    setStudentAteenData((prevData) => ({
      ...prevData,
      studentId: student._id,
    }));

    setOpenAddModal(true);
  };

  const handleCloseModal = () => {
    setOpenAddModal(false);
    setStudentAteenData({
      attendancedate: '',
      attendancestatus: '',
      subjectname: '',
    });
    setCurrentStudent(null);

  };


  const branche = branchesdata && branchesdata.length > 0 ? branchesdata[0] : null;  // Access the first branch if applicable


  const aggregateAttendance = (student) => {
    const records = student.attendances || [];

    // Filter records by the selected subject
    const filteredRecords = selectedSubject
      ? records.filter(record => record.subjectname === selectedSubject)
      : records;

    // Calculate the subject count and status count
    const subjectCount = filteredRecords.reduce((acc, record) => {
      acc[record.subjectname] = (acc[record.subjectname] || 0) + 1;
      return acc;
    }, {});

    const statusCount = filteredRecords.reduce((acc, record) => {
      acc[record.attendancestatus] = (acc[record.attendancestatus] || 0) + 1;
      return acc;
    }, {});

    return { subjectCount, statusCount };
  };

  return (
    <Box sx={{ display: 'flex' }}>
      <Drawer
        variant="permanent"
        sx={{
          width: drawerWidth,
          display: { lg: 'block', xs: 'none' }
        }}
      >

        {/* Add Drawer content here */}
      </Drawer>
      <Box sx={{ flexGrow: 1 }} className="mt-[-20px]">
        <Toaster position="top-center" reverseOrder={false} />
        <Toolbar />
        <div className="p-4">
          <div className="text-2xl text-zinc-600 mb-4">
            <a href="#" className="text-blue-600">🏠Home</a> /
            <a href="#" className="text-blue-600">Manage Student</a> / <span>Student Attendance</span>
          </div>
          <div className="bg-gradient-to-r from-red-500 to-yellow-500 text-white text-xl font-bold p-4 rounded">
            <h2 className="text-lg font-semibold">Student Attendance</h2>
          </div>
          <div className="bg-white p-4 rounded-b-lg shadow-lg">

            <div className="overflow-x-auto">
              <table className="min-w-full bg-white border border-zinc-300">
                <thead className="bg-zinc-100">
                  <tr>
                    <th className="px-4 py-2 border">Sl. No.</th>
                    <th className="px-4 py-2 border">Student Name</th>
                    <th className="px-4 py-2 border">Course Name</th>
                    <th className="px-4 py-2 border">Present Count</th>
                    <th className="px-4 py-2 border">Absent Count</th>
                    <th className="px-4 py-2 border">Subject Name</th>
                    <th className="px-4 py-2 border">Attendance</th>
                    <th className="border border-zinc-300">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {students.map((student, index) => {
                    const { subjectCount, statusCount } = aggregateAttendance(student);
                    return (
                      <tr key={student._id}>
                        <td className="px-4 py-2 border">{index + 1}</td>
                        <td className="px-4 py-2 border">{student.studentName}</td>
                        <td className="px-4 py-2 border">{student.courseName}</td>
                        <td className="px-4 py-2 border">{statusCount.present || 0}</td>
                        <td className="px-4 py-2 border">{statusCount.absent || 0}</td>
                        <td className="px-4 py-2 border">
                          <select
                            className="w-full p-1 border border-zinc-300 rounded"
                            value={selectedSubject}
                            onChange={handleSubjectChange}
                          >

                            <option value="">Choose Subject Name</option>
                            {[...new Set(student.attendances.map((record) => record.subjectname))].map(

                              (subject, index) => (
                                <option key={index} value={subject}>
                                  {subject}

                                </option>
                              )
                            )}

                          </select>

                        </td>
                        <td className="px-4 py-2 border">
                          <Button
                            variant="contained"
                            color="primary"
                            onClick={() => handleAddButtonClick(student)}
                          >
                            Add
                          </Button>
                        </td>
                        <div className="flex space-x-2 justify-center">
                          <button className="text-green-600" onClick={() => handleViewAttendance(student)} color="primary"> <MdVisibility /></button>
                          <button
                            className="text-blue-600"
                            onClick={() => handleViewAttendanceDetails(student)}
                            color="primary"
                          >
                            Details
                          </button>
                        </div>
                      </tr>
                    );
                  })}
                </tbody>

              </table>
            </div>
          </div>
        </div>
        <ViewAttendanceDetailsModal
          open={viewModalOpen}
          handleClose={handleCloseViewModal}
          attendanceData={viewAttendanceData}
        />
        <ViewAttendanceDetails
          open={viewModalOpens}
          onClose={() => setViewModalOpens(false)}
          attendanceDetails={viewAttendanceDatas}
        />


      </Box>
      <Modal
        open={openAddModal}
        onClose={handleCloseModal}
        aria-labelledby="add-attendance-modal"
      >
        <Box sx={{ p: 4, backgroundColor: 'white', margin: 'auto', marginTop: '6%', width: '400px', borderRadius: '8px' }}>
          <h2 id="add-attendance-modal" className="text-xl mb-4">
            Add Attendance for {currentStudent?.studentName}
          </h2>
          <div className="mb-4">
            <label className="block text-zinc-700">Date of Attendance</label>
            <input
              type="date"
              className="w-full mt-1 p-2 border border-zinc-300 rounded"
              name="attendancedate"
              value={studentateendata.attendancedate}
              onChange={handleChange}
            />
          </div>
          <div className="mb-4">
            <label className="block text-zinc-700">Subject Name</label>
            <select
              className="w-full mt-1 p-2 border border-zinc-300 rounded"
              name="subjectname"
              value={studentateendata.subjectname}
              onChange={handleChange}
            >
              <option value="">Choose Subject</option>
              {subjects.map((subject) => (
                <option key={subject} value={subject}>
                  {subject}
                </option>
              ))}
            </select>
          </div>
          <div className="mb-4">
            <label className="block text-zinc-700">Attendance Status</label>
            <select
              className="w-full mt-1 p-2 border border-zinc-300 rounded"
              name="attendancestatus"
              value={studentateendata.attendancestatus}
              onChange={handleChange}
            >
              <option value="">Choose Status</option>
              <option value="present">Present</option>
              <option value="absent">Absent</option>
            </select>
          </div>
          <Button
            variant="contained"
            color="primary"
            onClick={handleAddAttendance}
          >
            Add Attendance
          </Button>
        </Box>
      </Modal>
    </Box>
  );
};

export default StudentAttendancePage;
