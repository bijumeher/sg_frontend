import { Box, Drawer, Toolbar } from '@mui/material'
import React from 'react'
const drawerWidth = 240;

const Admitcarddetails = () => {
    return (
        <>
            <Box sx={{ display: 'flex' }}>
                <Drawer
                    variant="permanent"
                    sx={{
                        width: drawerWidth,
                    }}
                >
                </Drawer>
                <Box component="main" sx={{ flexGrow: 1 }} className=' mt-[-22px]'>
                    <Toolbar />
                    <div className="p-4">
                        <div className=" text-2xl text-zinc-600 mb-4">
                            <a href="#" className="text-blue-600">🏠Home</a> /
                            <a href="#" className="text-blue-600">Question & Exam</a> / <span>View AdmitCard Details</span>
                        </div>
                        <div className="bg-gradient-to-r from-red-500 to-orange-400 text-white p-4 rounded-t-lg">
                            <h2 className="text-lg">Admit Card Details</h2>
                        </div>
                        <div className="border border-zinc-300 rounded-b-lg p-4">
                            <div className="flex items-center mb-4">
                                <select className="border border-zinc-300 rounded p-2 mr-4">
                                    <option>Choose Student ID</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </Box >
            </Box >
        </>

    )
}

export default Admitcarddetails