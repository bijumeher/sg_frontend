import React from 'react'
import { Box, Drawer, Toolbar } from '@mui/material'
const drawerWidth = 240;
const Viewstudntresult = () => {
    return (
        <>
            <Box sx={{ display: 'flex' }}>
                <Drawer
                    variant="permanent"
                    sx={{
                        width: drawerWidth,
                    }}
                >
                </Drawer>
                <Box component="main" sx={{ flexGrow: 1 }} className=' mt-[-22px]'>
                    <Toolbar />
                    <div className="p-4">
                        <div className=" text-2xl text-zinc-600 mb-4">
                            <a href="#" className="text-blue-600">🏠Home</a> /
                            <a href="#" className="text-blue-600">Question & Exams</a> / <span>View Exam Result Details</span>
                        </div>
                        <div className="bg-gradient-to-r from-red-500 to-orange-400 text-white p-4 rounded-t-lg">
                            <h2 className="text-lg">View Exam Result Details</h2>
                        </div>
                        <div className="border border-zinc-300 rounded-b-lg p-4">
                            <div className="flex items-center mb-4">
                                <select className="border border-zinc-300 rounded p-2 mr-4">
                                    <option>Choose Exam Name</option>
                                </select>
                                <button className="bg-green-500 text-white px-4 py-2 rounded">Export</button>
                            </div>
                            <table className="w-full border-collapse border border-zinc-300">
                                <thead>
                                    <tr className="bg-zinc-100">
                                        <th className="border border-zinc-300 p-2">Sl_No.</th>
                                        <th className="border border-zinc-300 p-2">View</th>
                                        <th className="border border-zinc-300 p-2">Delete</th>
                                        <th className="border border-zinc-300 p-2">Roll Number</th>
                                        <th className="border border-zinc-300 p-2">Student Name</th>
                                        <th className="border border-zinc-300 p-2">Exam Name</th>
                                        <th className="border border-zinc-300 p-2">Total Question</th>
                                        <th className="border border-zinc-300 p-2">Answer Given</th>
                                        <th className="border border-zinc-300 p-2">Correct Answer</th>
                                        <th className="border border-zinc-300 p-2">Wrong Answer</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className="border border-zinc-300 p-2 text-center" colspan="10">No Data Found...</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </Box >
            </Box >
        </>
    )
}

export default Viewstudntresult