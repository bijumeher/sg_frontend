import { Box, Drawer, Toolbar } from '@mui/material'
import React from 'react'
const drawerWidth = 240;

const Studymateriall = () => {
    return (
        <>
            <Box sx={{ display: 'flex' }}>
                <Drawer
                    variant="permanent"
                    sx={{
                        width: drawerWidth,
                        display: { lg:'block',xs: 'none' }
                    }}
                >
                </Drawer>
                <Box component="main" sx={{ flexGrow: 1 }} className=' mt-[-22px]'>
                    <Toolbar />

                    <div class="p-4">
                        <div className=" text-2xl text-zinc-600 mb-4">
                            <a href="#" className="text-blue-600">🏠Home</a> /
                            <a href="#" className="text-blue-600">Study Material</a> / <span>Add Study Material</span>
                        </div>
                        <div class="bg-gradient-to-r from-red-500 to-orange-400 text-white p-4 rounded-t-lg">
                            <h2 class="text-lg font-semibold">Add Study Material</h2>
                        </div>
                        <div class="bg-white p-4 rounded-b-md shadow-md">
                            <form class="space-y-4">
                                <div class="grid grid-cols-1 md:grid-cols-2 gap-4">
                                    <div>
                                        <label for="subject" class="block text-sm font-medium text-zinc-700">Subject Name</label>
                                        <select
                                            id="subject"
                                            name="subject"
                                            class="mt-1 block w-full border border-zinc-300 rounded-md shadow-sm focus:ring-teal-500 focus:border-teal-500 sm:text-sm"
                                        >
                                            <option>Choose Subject Name</option>
                                        </select>
                                    </div>
                                    <div>
                                        <label for="material-name" class="block text-sm font-medium text-zinc-700"
                                        >Study Material Name</label
                                        >
                                        <input
                                            type="text"
                                            id="material-name"
                                            name="material-name"
                                            class="mt-1 block w-full border border-zinc-300 rounded-md shadow-sm focus:ring-teal-500 focus:border-teal-500 sm:text-sm"
                                        />
                                    </div>
                                </div>
                                <div>
                                    <label for="file-upload" class="block text-sm font-medium text-zinc-700">Choose File</label>
                                    <input
                                        type="file"
                                        id="file-upload"
                                        name="file-upload"
                                        class="mt-1 block w-full text-sm text-zinc-500 border border-zinc-300 rounded-md shadow-sm focus:ring-teal-500 focus:border-teal-500"
                                    />
                                </div>
                                <button
                                    type="submit"
                                    class="w-full bg-teal-500 text-white py-2 rounded-md shadow-md hover:bg-teal-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-teal-500"
                                >
                                    Upload Study Material
                                </button>
                            </form>
                        </div>
                        <table className="min-w-full bg-white border border-zinc-200 mt-10">
                            <thead>
                                <tr className="bg-zinc-100">
                                    <th className="py-2 px-4 border-b">Sl. No.</th>
                                    <th className="py-2 px-4 border-b">Subject Name</th>
                                    <th className="py-2 px-4 border-b">Material</th>
                                    <th className="py-2 px-4 border-b">Material</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className="py-2 px-4 border-b text-center">1</td>
                                    <td className="py-2 px-4 border-b">PGDCA</td>
                                    <td className="py-2 px-4 border-b">HTML</td>
                                    <td className="py-2 px-4 border-b">
                                        <a href="#" className="text-blue-500">HTML NOTES.pdf</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="py-2 px-4 border-b text-center">2</td>
                                    <td className="py-2 px-4 border-b">PGDCA</td>
                                    <td className="py-2 px-4 border-b">FUNDAMENTAL OF COMPUTER</td>
                                    <td className="py-2 px-4 border-b">
                                        <a href="#" className="text-blue-500">computer_fundamental.pdf</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="py-2 px-4 border-b text-center">3</td>
                                    <td className="py-2 px-4 border-b">PGDCA</td>
                                    <td className="py-2 px-4 border-b">POWER POINT BOOKS</td>
                                    <td className="py-2 px-4 border-b">
                                        <a href="#" className="text-blue-500">POWER POINT BOOKS.pdf</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>


                </Box>
            </Box>
        </>
    )
}

export default Studymateriall