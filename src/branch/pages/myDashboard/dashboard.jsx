import { Box, Button, Drawer, Toolbar, Tooltip } from '@mui/material';
import { RiComputerFill } from "react-icons/ri";
import { PiStudentFill } from "react-icons/pi";
import { GiReceiveMoney, GiTakeMyMoney } from "react-icons/gi";
import { FaCoins } from "react-icons/fa";
import { BiSolidGroup } from "react-icons/bi";
import { useState } from 'react';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import MainCard from './MainCard';
// import IncomeAreaChart from './IncomeAreaChart';
// import MonthlyBarChart from './MonthlyBarChart';
import RevenuChartCard from '../myDashboard/RevenuChartCard';
import RevenuChartCardData from '../myDashboard/revenu-chart';
import '../myDashboard/dash.style.css'; // Assuming this imports your custom styles
import IncomeAreaChart from './IncomeAreaChart';
// import { Area, AreaChart, CartesianGrid, XAxis, YAxis } from 'recharts';
import CustomMonthLayout from './Calendar';
import { Card } from 'react-bootstrap';


const drawerWidth = 240;


const data = [
  {
    name: "PGDCA/TALLY", value: 10
  }, {
    name: "PGDCA", value: 4
  }, {
    name: "TALLY", value: 4.5
  }, {
    name: "BCC", value: 2
  }
]

// notificatin fuction start 
// const useStyles = makeStyles({
//   scrollableCard: {
//     maxHeight: '400px',
//     overflowY: 'auto',
//     padding: '20px',
//   },
// });
// notificatin fuction end 



const MyDashboardData = () => {
  const [slot, setSlot] = useState('week');
  // const classes = useStyles();
  return (
    <>
      <Box
        // className='bg-gradient-to-r from-white via-white to-black'
        sx={{
          display: 'flex',
          // Replace with your image path
          backgroundSize: 'cover',
          backgroundImage: 'linear-gradient(to bottom, #f3f9fd,#f3f9fd)'

          // Ensure the background covers the entire viewport height
        }}>
        <Drawer
          variant="permanent"
          // className='drower'
          sx={{
            width: drawerWidth,
            display: { lg:'block',xs: 'none' }
          }}
        >
          {/* Drawer content goes here */}
        </Drawer>

        <Box component="main" sx={{ flexGrow: 1, p: 4 ,overflow:'hidden'}}>

          <div>
            <div className="flex justify-between items-center mb-4">
              <h2 className="text-blue-600 text-lg font-bold"></h2>
              <a href="#" className="text-sm font-semibold text-black">Download Authorization Letter</a>
            </div>
            {/* card section start */}
            <div className=" container ">
              <div className="flex justify-between items-center mb-4">
                <h2 className=" text-lg font-bold text-black">Student Information</h2>
                <a href="#" className=" text-sm font-semibold text-black">Download Authorization Letter</a>
              </div>
              <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-10 mb-6 ml-16">
                <div class="flex items-center bg-card text-card-foreground rounded-lg shadow-md p-4 w-64 bg-gradient-to-r from-pink-500 to-red-500 hover:scale-105 duration-300 cursor-pointer">
                  <div class="flex items-center justify-center w-16 h-16 text-white">
                    <RiComputerFill size={30} />
                  </div>
                  <div class="ml-4">
                    <div class="text-muted-foreground text-white">Total Course</div>
                    <div class="text-2xl font-bold text-white ">6</div>
                  </div>
                </div>
                <div class="flex items-center bg-card text-card-foreground rounded-lg shadow-md p-4 w-64 bg-gradient-to-r from-sky-300 to-blue-500 hover:scale-105 duration-300 cursor-pointer">
                  <div class="flex items-center justify-center w-16 h-16 text-white">
                    <PiStudentFill size={30} />
                  </div>
                  <div class="ml-4">
                    <div class="text-muted-foreground text-white">Total Student</div>
                    <div class="text-2xl font-bold text-white">79</div>
                  </div>
                </div>
                <div class="flex items-center bg-card text-card-foreground rounded-lg shadow-md p-4 w-64 bg-gradient-to-r from-teal-500 to-green-500 hover:scale-105 duration-300 cursor-pointer">
                  <div class="flex items-center justify-center w-16 h-16 text-white">
                    <BiSolidGroup size={35} />
                  </div>
                  <div class="ml-4">
                    <div class="text-muted-foreground text-white">Total Batch</div>
                    <div class="text-2xl font-bold text-white">7</div>
                  </div>
                </div>
                <div class="flex items-center bg-card text-card-foreground rounded-lg shadow-md p-4 w-64 bg-gradient-to-r from-yellow-300 to-yellow-500 hover:scale-105 duration-300 cursor-pointer">
                  <div class="flex items-center justify-center w-16 h-16 text-white">
                    <GiReceiveMoney size={30} />
                  </div>
                  <div class="ml-4">
                    <div class="text-muted-foreground text-white">Today's Payment Recevied</div>
                    <div class="text-2xl font-bold text-white">30000</div>
                  </div>
                </div>
                <div class="flex items-center bg-card text-card-foreground rounded-lg shadow-md p-4 w-64 bg-gradient-to-r from-orange-300 to-orange-500  hover:scale-105 duration-300 cursor-pointer">
                  <div class="flex items-center justify-center w-16 h-16 text-white">
                    <GiTakeMyMoney size={35} />
                  </div>
                  <div class="ml-4">
                    <div class="text-muted-foreground text-white">Today's part payment</div>
                    <div class="text-2xl font-bold text-white">11020</div>
                  </div>
                </div>
                <div class="flex items-center bg-card text-card-foreground rounded-lg shadow-md p-4 w-64 bg-gradient-to-r from-red-300 to-red-500 hover:scale-105 duration-300 cursor-pointer">
                  <div class="flex items-center justify-center w-16 h-16 text-white">
                    <FaCoins size={35} />
                  </div>
                  <div class="ml-4">
                    <div class="text-muted-foreground text-white">Today's Final payment</div>
                    <div class="text-2xl font-bold text-white">11020</div>
                  </div>
                </div>
              </div>
            </div>
            {/* card section end */}
            {/* course sources start */}
            <Grid container spacing={2}>
              <Grid item xs={12} md={12}>
                <Grid container alignItems="center" justifyContent="space-between">
                  <Grid item>
                    <Typography variant="h4">Attendance</Typography>
                  </Grid>
                  <Grid item>
                    <Box sx={{ display: 'flex' }}>
                      <Button
                        size="small"
                        onClick={() => setSlot('month')}
                        color={slot === 'month' ? 'primary' : 'secondary'}
                        variant={slot === 'month' ? 'outlined' : 'text'}
                      >
                        Month
                      </Button>
                      <Button
                        size="small"
                        onClick={() => setSlot('week')}
                        color={slot === 'week' ? 'primary' : 'secondary'}
                        variant={slot === 'week' ? 'outlined' : 'text'}
                      >
                        Week
                      </Button>
                    </Box>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} md={12}>
                <Grid spacing={2} className='hedermap'>
                  <Grid item xs={12} >
                    <MainCard content={false} >
                      <Box >
                        <IncomeAreaChart slot={slot} />
                      </Box>
                    </MainCard>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            {/* course information end*/}
            {/* course information start */}
            <Grid container spacing={2} className='mt-5 '>

              <Grid item xs={12} sm={6} md={4}>
                <RevenuChartCard chartData={RevenuChartCardData} />
              </Grid>
              <Grid item xs={12} sm={6} md={4} >
                <CustomMonthLayout />
              </Grid>
              <Grid item xs={12} sm={4} md={4} >
                <Card style={{ height: 395, overflowY: 'auto' }}className='p-4' >
                  <Typography sx={{fontWeight:800,fontSize:"30px",marginTop:"-20px"}} className=' text-center text-2xl text-blue-500 '>Notification</Typography>
                  <Typography>notificationnotification</Typography>
                  <Typography>Notification</Typography>
                  <Typography>notificationnotification</Typography>
                  <Typography>Notification</Typography>
                  <Typography>notificationnotification</Typography>
                  <Typography>Notification</Typography>
                  <Typography>notificationnotification</Typography>
                  <Typography>Notification</Typography>

                </Card>
              </Grid>

            </Grid>

          </div>

          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4 mt-14 mx-4">
            <div className="bg-card text-card-foreground p-4 rounded shadow hover:scale-105 duration-300 cursor-pointer hover:bg-[#ccd7fb] hover:text-white">
              <p className="text-muted-foreground">Income</p>
              <p className="text-2xl font-bold">$5,456</p>
              <p className="text-green-500">+14%</p>
            </div>
            <div className="bg-card text-card-foreground p-4 rounded shadow hover:scale-105 duration-300 cursor-pointer hover:bg-[#ccd7fb] hover:text-white">
              <p className="text-muted-foreground">Expenses</p>
              <p className="text-2xl font-bold">$4,764</p>
              <p className="text-red-500">-8%</p>
            </div>
            <div className="bg-card text-card-foreground p-4 rounded shadow hover:scale-105 duration-300 cursor-pointer hover:bg-[#ccd7fb] hover:text-white">
              <p className="text-muted-foreground">Spendings</p>
              <p className="text-2xl font-bold">$1.5M</p>
              <p className="text-green-500">+15%</p>
            </div>
            <div className="bg-card text-card-foreground p-4 rounded shadow hover:scale-105 duration-300 cursor-pointer hover:bg-[#ccd7fb] hover:text-white">
              <p className="text-muted-foreground">Totals</p>
              <p className="text-2xl font-bold">$31,564</p>
              <p className="text-yellow-500">+76%</p>
            </div>
          </div>
        </Box>
      </Box>
    </>
  );
}

export default MyDashboardData;
