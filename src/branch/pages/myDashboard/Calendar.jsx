import * as React from 'react';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DateCalendar } from '@mui/x-date-pickers/DateCalendar';
import { Card } from 'react-bootstrap';


export default function AddWeekNumber() {
  return (

    <LocalizationProvider
      dateAdapter={AdapterDayjs}
      localeText={{
        calendarWeekNumberHeaderText: '#',
        calendarWeekNumberText: (weekNumber) => `${weekNumber}.`,
      }}
    >
      <Card style={{ height: '395px', borderRadius: '25px', backgroundImage: 'linear-gradient(to bottom, #dde3e9,#ccd7fb)', display: 'flex', justifyContent: 'center', color: 'white' }}>
        <DateCalendar displayWeekNumber sx={{ color: 'white' }} />
      </Card>
    </LocalizationProvider>

  );
}