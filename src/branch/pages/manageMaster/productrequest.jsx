// import { Box, Drawer, Toolbar } from '@mui/material';
// import React, { useState } from 'react'
// import { MdDelete, MdEdit } from 'react-icons/md';
// import DeleteDialog from '../deleteModal/coursDeleteModal';
// import EditModal from '../editModal/ProductModal';
// const drawerWidth = 240;


// const Data2 = [
//   {
//     id: 0,
//     slno: 1,
//     ssid: "02",
//     productname: "Datascience",
//     dateofrequest: "Science",
//     quantity:555,
//     amount:500,
//     totalamount:1222

//   },
//   {
//     id: 1,
//     slno: 2,
//     ssid: "02",
//     productname: "Datascience",
//     dateofrequest: "Science",
//     quantity:555,
//     amount:500,
//     totalamount:1222
//   },
//   {
//     id: 2,
//     slno: 3,
//     ssid: "02",
//     productname: "Datascience",
//     dateofrequest: "Science",
//     quantity:555,
//     amount:500,
//     totalamount:1222
//   },
//   {
//     id: 3,
//     slno: 4,
//     ssid: "02",
//     productname: "Datascience",
//     dateofrequest: "Science",
//     quantity:555,
//     amount:500,
//     totalamount:1222
//   },
// ]
// const Productrequest = () => {
//   const [OrderDialog, setOrderDialog] = useState(false)

//   const handelOrderDialog = () => {
//     setOrderDialog(!OrderDialog)
//   }
//   const [editModal, setEditModal] = useState(false)

//     const handelEditModal = () => {
//         setEditModal(!editModal)
//     }
//   return (
//     <Box sx={{ display: 'flex' }}>
//       <Drawer
//         variant="permanent"
//         sx={{
//           width: drawerWidth,
//         }}
//       >
//       </Drawer>

//       <Box component="main" sx={{ flexGrow: 1 }} className=' mt-[-22px]' >
//         <Toolbar />
//         <div className="p-4">
//           <div className=" text-2xl text-zinc-600 mb-4">
//             <a href="#" className="text-blue-600">🏠Home</a> /
//             <a href="#" className="text-blue-600">Manage Master</a> / <span>Add Product Rrequest</span>
//           </div>

//           <div className="bg-gradient-to-r from-red-500 to-yellow-500 text-white text-lg font-bold p-4 rounded">
//             Add Product Request
//           </div>

//           <div className="bg-white p-4 shadow-md rounded mt-4">
//             <div className="grid grid-cols-2 gap-4">
//               <div>
//                 <label className="block text-zinc-700">Product Name</label>
//                 <select className="w-full border border-zinc-300 p-2 rounded">
//                   <option>Choose Product Name</option>
//                 </select>
//               </div>
//               <div>
//                 <label className="block text-zinc-700">Date of Request</label>
//                 <input type="date" className="w-full border border-zinc-300 p-2 rounded" />
//               </div>
//             </div>
//             <div className="grid grid-cols-3 gap-4 mt-4">
//               <button className="bg-zinc-200 text-zinc-700 p-2 rounded">Price</button>
//               <button className="bg-zinc-200 text-zinc-700 p-2 rounded">Quantity</button>
//               <button className="bg-zinc-200 text-zinc-700 p-2 rounded">TotalAmount</button>
//             </div>
//             <div className="flex justify-end mt-4">
//               <button className="bg-blue-600 text-white px-4 py-2 rounded shadow">Submit</button>
//             </div>
//           </div>

//           <div className="mt-4 overflow-x-auto">
//             <table className="min-w-full border border-zinc-300">
//               <thead className="bg-zinc-100">
//                 <tr className="bg-blue-200 text-center">
//                   <th className="border border-zinc-300 p-2">Sl_No.</th>
//                   <th className="border border-zinc-300 p-2">SS ID</th>
//                   <th className="border border-zinc-300 p-2">Product Name</th>
//                   <th className="border border-zinc-300 p-2">Date Of Request</th>
//                   <th className="border border-zinc-300 p-2">Quantity</th>
//                   <th className="border border-zinc-300 p-2">Amount</th>
//                   <th className="border border-zinc-300 p-2">Total Amount</th>
//                   <th className="border border-zinc-300 p-2">View</th>
//                 </tr>
//               </thead>
//               <tbody>
//                 {Data2.map((e) => (
//                   <tr key={e.id}>
//                     <td className="border border-zinc-300 p-2 text-center">{e.slno}</td>
//                     <td className="border border-zinc-300 p-2 text-center">{e.ssid}</td>
//                     <td className="border border-zinc-300 p-2 text-center">{e.productname}</td>
//                     <td className="border border-zinc-300 p-2 text-center">{e.dateofrequest}</td>
//                     <td className="border border-zinc-300 p-2 text-center">{e.quantity}</td>
//                     <td className="border border-zinc-300 p-2 text-center">{e.amount}</td>
//                     <td className="border border-zinc-300 p-2 text-center">{e.totalamount}</td>

//                     <td className=" border-blue-500 p-2 text-center flex justify-center gap-2">
//                       <MdEdit onClick={handelEditModal} size={20}style={{ cursor: 'pointer'}} />
//                       <MdDelete onClick={handelOrderDialog} size={20} style={{ cursor: 'pointer'}}/>
//                     </td>
//                   </tr>
//                 ))}

//               </tbody>
//             </table>
//           </div>
          
//           <DeleteDialog open={OrderDialog} handleClose={handelOrderDialog} />
//           <EditModal open1={editModal} handleClose1={handelEditModal} />
//         </div>

//       </Box>
//     </Box>
//   )
// }

// export default Productrequest