import { Box, Drawer, Toolbar } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { MdEdit, MdDelete } from 'react-icons/md';
import SessionModal from '../editModal/SessionModal';
import DeleteDialog from '../deleteModal/coursDeleteModal';
import { useDispatch, useSelector } from 'react-redux';
import { createSession, getAllsessionData, updateSessionData, deleteSessionData } from '../../../actions/branch/sessionAction';
import { getDataBrancWise } from '../../../actions/student/studentAction';
import { getBranchDataByid } from '../../../actions/branch/branchAction';
import { getCoursesAllData } from '../../../actions/branch/courseAction';

const drawerWidth = 240;

const Sessiondetails = () => {
  const [sessionData, setSessionData] = useState({
    sessionName: '',
    sessionDate: '',
    startTime: '',
    endTime: '',
  });

  const allSessions = useSelector((state) => state.session.sessionData);
  const students = useSelector((state) => state.course.courseData) || [];

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllsessionData());
  }, [dispatch]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setSessionData({ ...sessionData, [name]: value });
  };

  const handleSubmit = () => {
    dispatch(createSession(sessionData)).then(() => {
      setSessionData({
        sessionName: '',
        sessionDate: '',
        startTime: '',
        endTime: '',
      });
      dispatch(getAllsessionData());
    });
  };

  const [OrderDialog, setOrderDialog] = useState(false);
  const [editModal, setEditModal] = useState(false);
  const [currentSession, setCurrentSession] = useState(null);
  const [deleteSessionId, setDeleteSessionId] = useState(null);

  const handleOrderDialog = (sessionId) => {
    setDeleteSessionId(sessionId);
    setOrderDialog(true);
  };

  const handleEditModal = (sessionData) => {
    setCurrentSession(sessionData);
    setEditModal(true);
  };

  const handleCloseEditModal = () => {
    setEditModal(false);
    setCurrentSession(null);
  };

  const handleUpdate = (updatedSession) => {
    dispatch(updateSessionData({ ...updatedSession, sessionId: currentSession._id })).then(() => {
      setEditModal(false);
      setCurrentSession(null);
      dispatch(getAllsessionData());
    });
  };

  const handleDelete = (sessionId) => {
    dispatch(deleteSessionData(sessionId)).then(() => {
      setOrderDialog(false);
      dispatch(getAllsessionData());
    });
  };

  const branch = JSON.parse(sessionStorage.getItem('branchData'));
  const branchName = branch.branchName;
  const obj = { branchName: branchName };

  useEffect(() => {
    dispatch(getDataBrancWise(obj));
  }, [dispatch]);

  return (
    <Box sx={{ display: 'flex' }}>
      <Drawer
        variant="permanent"
        sx={{
          width: drawerWidth,
          display: { lg:'block',xs: 'none' }
        }}
      >
        {/* Add Drawer content here */}
      </Drawer>
      <Box component="main" sx={{ flexGrow: 1 }} className="mt-[-20px]">
        <Toolbar />
        <div className="p-4">
          <div className="text-2xl text-zinc-600 mb-4">
            <a href="#" className="text-blue-600">
              🏠Home
            </a>{' '}
            /{' '}
            <a href="#" className="text-blue-600">
              Manage Master
            </a>{' '}
            / <span>Add Session Details</span>
          </div>
          <div className="bg-gradient-to-r from-red-500 to-yellow-500 text-white p-4 rounded-t-lg">
            <h2 className="text-xl font-bold">Add Session Details</h2>
          </div>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4 mb-4">
          
          </div>
          <div className="bg-white p-4 shadow-md rounded-b-lg">
            <div className="flex items-center mb-4">
              <div className="mr-4">
                <input
                  type="text"
                  name="sessionName"
                  value={sessionData.sessionName}
                  onChange={handleChange}
                  className="border rounded p-2 w-full"
                  placeholder="Session Name"
                />
              </div>
              <div className="mr-4">
                <input
                  type="date"
                  name="sessionDate"
                  value={sessionData.sessionDate}
                  onChange={handleChange}
                  className="border rounded p-2 w-full"
                  placeholder="Session Date"
                />
              </div>
              <div className="mr-4">
                <input
                  type="time"
                  name="startTime"
                  value={sessionData.startTime}
                  onChange={handleChange}
                  className="border rounded p-2 w-full"
                  placeholder="Start Time"
                />
              </div>
              <div className="mr-4">
                <input
                  type="time"
                  name="endTime"
                  value={sessionData.endTime}
                  onChange={handleChange}
                  className="border rounded p-2 w-full"
                  placeholder="End Time"
                />
              </div>
              <button
                className="bg-blue-600 text-white px-4 py-2 rounded shadow"
                onClick={handleSubmit}
              >
                Add Session
              </button>
            </div>
            <table className="min-w-full border-collapse border border-zinc-300">
              <thead>
                <tr className="bg-blue-200 text-center">
                  <th className="border border-zinc-300 p-2">Sl. No.</th>
                  <th className="border border-zinc-300 p-2">Session Name</th>
                  <th className="border border-zinc-300 p-2">Session Date</th>
                  <th className="border border-zinc-300 p-2">Start Time</th>
                  <th className="border border-zinc-300 p-2">End Time</th>
                  <th className="border border-zinc-300 p-2">Action</th>
                </tr>
              </thead>
              <tbody>
                {allSessions?.map((session, index) => (
                  <tr key={session._id} className="text-center">
                    <td className="border border-zinc-300 p-2">{index + 1}</td>
                    <td className="border border-zinc-300 p-2">{session.sessionName}</td>
                    <td className="border border-zinc-300 p-2">{session.sessionDate}</td>
                    <td className="border border-zinc-300 p-2">{session.startTime}</td>
                    <td className="border border-zinc-300 p-2">{session.endTime}</td>
                    <td className="border border-zinc-300 p-2">
                      <button onClick={() => handleEditModal(session)}>
                        <MdEdit />
                      </button>
                      <button onClick={() => handleOrderDialog(session._id)}>
                        <MdDelete />
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </Box>
      {editModal && (
        <SessionModal
          isOpen={editModal}
          onClose={handleCloseEditModal}
          sessionData={currentSession}
          onUpdate={handleUpdate}
        />
      )}
      <DeleteDialog
        open={OrderDialog}
        handleClose={() => setOrderDialog(false)}
        handleDelete={() => handleDelete(deleteSessionId)}
        title="Are you sure you want to delete this session?"
      />
    </Box>
  );
};

export default Sessiondetails;
