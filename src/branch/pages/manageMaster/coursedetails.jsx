import React, { useState, useEffect } from 'react';
import { Box, Drawer, Toolbar } from '@mui/material';
import { MdEdit, MdDelete } from 'react-icons/md';
import DeleteDialog from '../deleteModal/coursDeleteModal';
import CourseEditDialog from '../editModal/CourseModal';
import { useDispatch, useSelector } from 'react-redux';
import { createCourse, getCoursesAllData, updateCourseData, deleteCourseData } from '../../../actions/branch/courseAction';
import { getBranchDataByid } from '../../../actions/branch/branchAction';

const drawerWidth = 240;

const CourseDetails = () => {
  const branch = JSON.parse(sessionStorage.getItem('branchData'));
  const branchId = branch?._id;
  const branchesdata = useSelector(state => state.masterbranc.fetchbranch);
  const branchData = JSON.parse(sessionStorage.getItem('branchData'));

  const [courseData, setCourseData] = useState({
    courseName: '',
    courseDuration: '',
    courseFees: '',
    courseType: '',
    branchId: branchId,
  });

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getBranchDataByid({ branchId: branchData._id })).then(() => {
      console.log('Courses fetched successfully');
    });
  }, [dispatch]);


  const handleChange = (e) => {
    const { name, value } = e.target;
    setCourseData({ ...courseData, [name]: value });
  };

  const handleSubmit = async () => {
    if (!courseData.courseName || !courseData.courseDuration || !courseData.courseFees || !courseData.courseType) {
      alert('Please fill all the fields');
      return;
    }
    dispatch(createCourse(courseData)).then(() => {
      setCourseData({
        courseName: '',
        courseDuration: '',
        courseFees: '',
        courseType: '',
        branchId: branchId,
      });
      dispatch(getBranchDataByid({ branchId: branchData._id }));
      dispatch(getCoursesAllData())
    });
  };

  const [OrderDialog, setOrderDialog] = useState(false);
  const [editCourseModal, setEditCourseModal] = useState(false);
  const [currentCourse, setCurrentCourse] = useState(null);
  const [deleteCourseId, setDeleteCourseId] = useState(null);

  const handelOrderDialog = (courseId) => {
    setDeleteCourseId(courseId);
    setOrderDialog(true);
  };

  const handleEditCourseModal = (courseData) => {
    setCurrentCourse(courseData);
    setEditCourseModal(true);
  };

  const handleCloseEditCourseModal = () => {
    setEditCourseModal(false);
    setCurrentCourse(null);
  };

  const handleUpdateCourse = (updatedCourse) => {
    dispatch(updateCourseData({ ...updatedCourse, courseId: currentCourse._id })).then(() => {
      setEditCourseModal(false);
      setCurrentCourse(null);
      dispatch(getBranchDataByid({ branchId: branchData._id }));

    });
  };

  const handleDelete = (courseId) => {
    dispatch(deleteCourseData(courseId)).then(() => {
      setOrderDialog(false);
      dispatch(getBranchDataByid({ branchId: branchData._id }));

    });
  };

  return (
    <Box sx={{ display: 'flex' }}>
      <Drawer
        variant="permanent"
        sx={{
          width: drawerWidth,
          display: { lg: 'block', xs: 'none' }
        }}
      >
      </Drawer>
      <Box component="main" sx={{ flexGrow: 1 }} className="mt-[-20px]">
        <Toolbar />
        <div className="p-4">
          <div className="text-2xl text-zinc-600 mb-4">
            <a href="#" className="text-blue-600">
              🏠Home
            </a>{' '}
            /{' '}
            <a href="#" className="text-blue-600">
              Manage Master
            </a>{' '}
            / <span>Add Course Details</span>
          </div>
          <div className="bg-gradient-to-r from-red-500 to-yellow-500 text-white p-4 rounded-t-lg">
            <h2 className="text-xl font-bold">Add Course Details</h2>
          </div>
          <div className="bg-white p-4 shadow-md rounded-b-lg">
            <div className="flex items-center mb-4">
              <div className="mr-4">
                <input
                  id="course-name"
                  type="text"
                  name="courseName"
                  value={courseData.courseName}
                  onChange={handleChange}
                  className="border rounded p-2 w-full"
                  placeholder="Course Name"
                />
              </div>
              <div className="mr-4">
                <input
                  id="course-fees"
                  type="text"
                  name="courseFees"
                  value={courseData.courseFees}
                  onChange={handleChange}
                  className="border rounded p-2 w-full"
                  placeholder="Course Fees"
                />
              </div>
              <div className="mr-4">
                <select
                  id="course-type"
                  name="courseType"
                  value={courseData.courseType}
                  onChange={handleChange}
                  className="border rounded p-2 w-full"
                >
                  <option value="">Choose Course Type</option>
                  <option value="Short Term">Short Term</option>
                  <option value="Long Term">Long Term</option>
                </select>
              </div>
              <div className="mr-4">
                <input
                  id="course-duration"
                  type="text"
                  name="courseDuration"
                  value={courseData.courseDuration}
                  onChange={handleChange}
                  className="border rounded p-2 w-full"
                  placeholder="Duration"
                />
              </div>
              <button
                className="bg-blue-600 text-white px-4 py-2 rounded shadow"
                onClick={handleSubmit}
              >
                Add Course
              </button>
            </div>
            <table className="min-w-full border-collapse border border-zinc-300">
              <thead>
                <tr className="bg-blue-200 text-center">
                  <th className="border border-zinc-300 p-2">Sl. No.</th>
                  <th className="border border-zinc-300 p-2">Course Name</th>
                  <th className="border border-zinc-300 p-2">Duration</th>
                  <th className="border border-zinc-300 p-2">Fees</th>
                  <th className="border border-zinc-300 p-2">Course Type</th>
                  <th className="border border-zinc-300 p-2">Actions</th>
                </tr>
              </thead>
              <tbody>
                {branchesdata && branchesdata.length > 0 && branchesdata?.map(branch => (
                  branch.courses?.map((course, index) => (
                    <tr key={index}>
                      <td className="border border-zinc-300 p-2 text-center">{index + 1}</td>
                      <td className="border border-zinc-300 p-2 text-center">{course.courseName}</td>
                      <td className="border border-zinc-300 p-2 text-center">{course.courseDuration}</td>
                      <td className="border border-zinc-300 p-2 text-center">{course.courseFees}</td>
                      <td className="border border-zinc-300 p-2 text-center">{course.courseType}</td>
                      <td className="border border-zinc-300 p-2 text-center">
                        <div className="flex justify-center items-center space-x-4">
                          <button
                            className="bg-yellow-500 text-white px-2 py-1 rounded shadow"
                            onClick={() => handleEditCourseModal(course)}
                          >
                            <MdEdit />
                          </button>
                          <button
                            className="bg-red-600 text-white px-2 py-1 rounded shadow"
                            onClick={() => handelOrderDialog(course._id)}
                          >
                            <MdDelete />
                          </button>
                        </div>
                      </td>
                    </tr>
                  ))
                ))}
              </tbody>
            </table>
            <DeleteDialog
              open={OrderDialog}
              handleClose={() => setOrderDialog(false)}
              handleDelete={() => handleDelete(deleteCourseId)}
              title="Are you sure you want to delete this course?"
            />
            <CourseEditDialog
              open={editCourseModal}
              handleClose={handleCloseEditCourseModal}
              initialData={currentCourse}
              handleUpdate={handleUpdateCourse}
            />
          </div>
        </div>
      </Box>
    </Box>
  );
};

export default CourseDetails;
