import { Box, Drawer, Toolbar } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { MdEdit, MdDelete } from 'react-icons/md';
import BatchDetails from '../editModal/BatchModal';
import DeleteDialog from '../deleteModal/coursDeleteModal';
import { useDispatch, useSelector } from 'react-redux';
import { createBatch, updateBatchData, DeleteBatchData } from '../../../actions/branch/batchAction';
import { getBranchDataByid } from '../../../actions/branch/branchAction';

const drawerWidth = 240;

const Batchdetails = () => {
  const branch = JSON.parse(sessionStorage.getItem('branchData'));
  const branchId = branch?._id;


  const [branchdata, setBranchData] = useState({
    batchName: '',
    batchTiming: '',
    branchId: branchId,
  });

  const branchData = JSON.parse(sessionStorage.getItem('branchData'));

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getBranchDataByid({ branchId: branchData._id }))
  }, [dispatch])

  const handleChange = (e) => {
    const { name, value } = e.target;
    setBranchData({ ...branchdata, [name]: value });
  };

  const handleSubmit = async () => {
    dispatch(createBatch(branchdata)).then(() => {
      setBranchData({
        batchName: '',
        batchTiming: '',
        branchId: branchId,
      });
      dispatch(getBranchDataByid({ branchId: branchData._id }));
    });
  };
  const branchesdata = useSelector(state => state.masterbranc.fetchbranch);


  const [OrderDialog, setOrderDialog] = useState(false);
  const [editModal, setEditModal] = useState(false);
  const [currentBatch, setCurrentBatch] = useState(null);
  const [deleteBatchId, setDeleteBatchId] = useState(null);

  const handelOrderDialog = (batchId) => {
    setDeleteBatchId(batchId);
    setOrderDialog(true);
  };

  const handelEditModal = (batchData) => {
    setCurrentBatch(batchData);
    setEditModal(true);
  };

  const handleCloseEditModal = () => {
    setEditModal(false);
    setCurrentBatch(null);
  };

  const handleUpdate = (updatedBatch) => {
    dispatch(updateBatchData({ ...updatedBatch, batchId: currentBatch._id })).then(() => {
      setEditModal(false);
      setCurrentBatch(null);
      dispatch(getBranchDataByid({ branchId: branchData._id }));
    });
  };

  const handleDelete = (batchId) => {
    dispatch(DeleteBatchData(batchId)).then(() => {
      setOrderDialog(false);
      dispatch(getBranchDataByid({ branchId: branchData._id }));
    });
  };



  return (
    <Box sx={{ display: 'flex' }}>
      <Drawer
        variant="permanent"
        sx={{
          width: drawerWidth,
          display: { lg:'block',xs: 'none' }
        }}
      >
        {/* Add Drawer content here */}
      </Drawer>
      <Box component="main" sx={{ flexGrow: 1 }} className="mt-[-20px]">
        <Toolbar />
        <div className="p-4">
          <div className="text-2xl text-zinc-600 mb-4">
            <a href="#" className="text-blue-600">
              🏠Home
            </a>{' '}
            /{' '}
            <a href="#" className="text-blue-600">
              Manage Master
            </a>{' '}
            / <span>Add Batch Details</span>
          </div>
          <div className="bg-gradient-to-r from-red-500 to-yellow-500 text-white p-4 rounded-t-lg">
            <h2 className="text-xl font-bold">Add batch Details</h2>
          </div>
          <div className="bg-white p-4 shadow-md rounded-b-lg">
            <div className="flex items-center mb-4">
              <div className="mr-4">
                <input
                  id="sub-subject-name"
                  type="text"
                  name="batchName"
                  value={branchdata.batchName}
                  onChange={handleChange}
                  className="border rounded p-2 w-full"
                  placeholder="Batch Name"
                />
              </div>
              <div className="mr-4">
                <input
                  id="sub-subject-name"
                  type="text"
                  name="batchTiming"
                  value={branchdata.batchTiming}
                  onChange={handleChange}
                  className="border rounded p-2 w-full"
                  placeholder="Batch Timing"
                />
              </div>

              <button
                className="bg-blue-600 text-white px-4 py-2 rounded shadow"
                onClick={handleSubmit}
              >
                Add Batch
              </button>
            </div>
            <table className="min-w-full border-collapse border border-zinc-300">
              <thead>
                <tr className="bg-blue-200 text-center">
                  <th className="border border-zinc-300 p-2">Sl. No.</th>
                  <th className="border border-zinc-300 p-2">Batch Name</th>
                  <th className="border border-zinc-300 p-2">Batch Timing</th>
                  <th className="border border-zinc-300 p-2">Actions</th>
                </tr>
              </thead>
              <tbody>
                {branchesdata && branchesdata.length > 0 && branchesdata?.map(branch => (
                  branch.batches?.map((batch, index) => (

                    <tr key={index}>
                      <td className="border border-zinc-300 p-2 text-center">{index + 1}</td>
                      <td className="border border-zinc-300 p-2 text-center">{batch.batchName}</td>
                      <td className="border border-zinc-300 p-2 text-center">{batch.batchTiming}</td>
                      <td className="border border-zinc-300 p-2 text-center">
                        <div className="flex justify-center items-center space-x-4">
                          <button
                            className="bg-yellow-500 text-white px-2 py-1 rounded shadow"
                            onClick={() => handelEditModal(batch)}
                          >
                            <MdEdit />
                          </button>
                          <button
                            className="bg-red-600 text-white px-2 py-1 rounded shadow"
                            onClick={() => handelOrderDialog(batch._id)}
                          >
                            <MdDelete />
                          </button>
                        </div>
                      </td>
                    </tr>
                  ))
                ))}
              </tbody>
            </table>
            <DeleteDialog
              open={OrderDialog}
              handleClose={() => setOrderDialog(false)}
              handleDelete={() => handleDelete(deleteBatchId)}
              title="Are you sure you want to delete this batch?"
            />
            <BatchDetails
              open={editModal}
              handleClose={handleCloseEditModal}
              initialData={currentBatch}
              handleUpdate={handleUpdate}
            />
          </div>
        </div>
      </Box>
    </Box>
  );
};

export default Batchdetails;
