import React from 'react';
import { useSelector } from 'react-redux';

const FeeCollectionData = () => {
    const allPayments = useSelector((state) => state.student.paymentData);
    console.log(allPayments);

    return (
        <div className="max-w-4xl mx-auto p-4 bg-card text-card-foreground rounded-lg shadow-md mt-24">
            <div className="mb-6">
                <h2 className="text-lg font-semibold bg-[#1565c0] text-primary-foreground p-2 rounded-t-lg">
                    All Payments
                </h2>
                <div className="p-4 border border-border rounded-b-lg">
                    <table className="min-w-full divide-y divide-gray-200">
                        <thead className="bg-gray-50">
                            <tr>
                               
                                <th
                                    scope="col"
                                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                >
                                    Paid Amount
                                </th>
                                <th
                                    scope="col"
                                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                >
                                    Rest Amount
                                </th>
                                <th
                                    scope="col"
                                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                >
                                    Payment Mode
                                </th>
                                <th
                                    scope="col"
                                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                >
                                    Date
                                </th>
                            </tr>
                        </thead>
                        <tbody className="bg-white divide-y divide-gray-200">
                            {allPayments.map((payment) => (
                                <tr key={payment._id}>
                                   
                                    <td className="px-6 py-4 whitespace-nowrap">
                                        {payment.Paidamount}
                                    </td>
                                    <td className="px-6 py-4 whitespace-nowrap">
                                        {payment.RestAmount}
                                    </td>
                                    <td className="px-6 py-4 whitespace-nowrap">
                                        {payment.paymentMode}
                                    </td>
                                    <td className="px-6 py-4 whitespace-nowrap">
                                    {payment.paymentDate}
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};

export default FeeCollectionData;