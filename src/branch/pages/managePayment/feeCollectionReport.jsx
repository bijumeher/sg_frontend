import { Box, Drawer, Toolbar } from '@mui/material'
import React from 'react'
const drawerWidth = 240;

const FeeCollectionReport = () => {
    return (
        <>
            <Box sx={{ display: 'flex' }}>
                <Drawer
                    variant="permanent"
                    sx={{
                        width: drawerWidth,
                        display: { lg:'block',xs: 'none' }
                    }}
                >
                </Drawer>
                <Box component="main" sx={{ flexGrow: 1 }} className=' mt-[-22px]'>
                    <Toolbar />

                    <div className="p-4">
                        <div className=" text-2xl text-zinc-600 mb-4">
                            <a href="#" className="text-blue-600">🏠Home</a> /
                            <a href="#" className="text-blue-600">Manage Payment</a> / <span>Fee Collection Report</span>
                        </div>


                        <div className="w-full bg-gradient-to-r from-red-500 to-orange-400 text-white p-4 rounded-t-lg">
                            <h2 className="text-lg font-semibold">Fee Collection Report</h2>
                        </div>

                        <div className="bg-white p-4 shadow-md rounded-b-lg">
                            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4 mb-4">
                                <div>
                                    <label for="from-date" className="block text-zinc-700">From Date</label>
                                    <input type="date" id="from-date" className="w-full border rounded p-2" value="2024-06-09" />
                                </div>
                                <div>
                                    <label for="to-date" className="block text-zinc-700">To Date</label>
                                    <input type="date" id="to-date" className="w-full border rounded p-2" value="2024-06-09" />
                                </div>
                                <div className="flex items-end">
                                    <button className="bg-blue-500 text-white p-2 rounded w-full">Search</button>
                                </div>
                                <div>
                                    <label for="student-name" className="block text-zinc-700">Student Name</label>
                                    <select id="student-name" className="w-full border rounded p-2">
                                        <option>Choose Student Name</option>
                                    </select>
                                </div>
                            </div>
                            <div className="flex justify-between items-center mb-4">
                                <div className="text-zinc-700 font-semibold">Total Amount : 0.00</div>
                                <button className="bg-green-500 text-white p-2 rounded">Export</button>
                            </div>

                            <div className="overflow-x-auto">
                                <table className="min-w-full bg-white border">
                                    <thead>
                                        <tr className="bg-zinc-200 text-zinc-700">
                                            <th className="border p-2">Sl No.</th>
                                            <th className="border p-2">Student Name</th>
                                            <th className="border p-2">Subject Name</th>
                                            <th className="border p-2">Batch Name</th>
                                            <th className="border p-2">Payment Date</th>
                                            <th className="border p-2">MR Number</th>
                                            <th className="border p-2">Payment Head</th>
                                            <th className="border p-2">Total Amount</th>
                                            <th className="border p-2">Amount Paid</th>
                                            <th className="border p-2">Remaining</th>
                                            <th className="border p-2">Payment Mode</th>
                                            <th className="border p-2">Next Payment Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="12" className="border p-2 text-center text-zinc-500">
                                                No Advance Added Yet...
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </Box>
            </Box>

        </>
    )
}

export default FeeCollectionReport