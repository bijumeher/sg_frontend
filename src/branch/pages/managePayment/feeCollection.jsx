import { Box, Button, Drawer, Toolbar } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { getDataBrancWise } from '../../../actions/student/studentAction';
import ViewStudentDetailsModal from '../manageStudent/modal/paymentmodal';

const drawerWidth = 240;

const FeeCollection = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
  const [currentStudent, setCurrentStudent] = useState(null);
    const [viewModal, setViewModal] = useState(false);
    const handleViewModal = (studentData) => {
        setCurrentStudent(studentData);
        setViewModal(true);
    };

    const handleCloseViewModal = () => {
        setViewModal(false);
        setCurrentStudent(null);
    };
   

    // Fetch student data from Redux state
    const studentfetch = useSelector(state => state.student.studentData);
    const branch = JSON.parse(sessionStorage.getItem('branchData'));
    const branchName = branch.branchName;
    const obj = { branchName: branchName };

    useEffect(() => {
        // Dispatch action to fetch student data branch-wise
        dispatch(getDataBrancWise(obj));
    }, [dispatch]);

   


   

    return (
        <>
            <Box sx={{ display: 'flex' }}>
                <Drawer
                    variant="permanent"
                    sx={{
                        width: drawerWidth,
                    }}
                >
                </Drawer>
                <Box component="main" sx={{ flexGrow: 1 }} className='mt-[-22px]'>
                    <Toolbar />
                    <div className='p-4'>
                        <div className=" text-2xl text-zinc-600 mb-4">
                            <a href="#" className="text-blue-600">🏠Home</a> /
                            <a href="#" className="text-blue-600">Manage Payment</a> / <span>Fee Collection</span>
                        </div>
                        <div className=" text-primary-foreground p-4 mt-6 bg-gradient-to-r from-red-500 to-orange-400 text-white rounded-lg">
                            <h1 className="text-lg font-bold">Fee Collection</h1>
                        </div>
                        <div className="p-4">
                            <div className="grid grid-cols-3 gap-4 mb-4">

                            </div>
                            <div className="overflow-x-auto">
                                <table className="min-w-full border-collapse border border-border">
                                    <thead className="bg-secondary text-secondary-foreground">
                                        <tr>
                                            <th className="border border-border p-2">Sl_No.</th>
                                            <th className="border border-border p-2">Student Name</th>
                                            <th className="border border-border p-2">Course Name</th>
                                            <th className="border border-border p-2">Course Fee</th>
                                            <th className="border border-border p-2">Registration ID</th>
                                            <th className="border border-border p-2">Payment</th>
                                        </tr>
                                    </thead>
                                    <tbody className="bg-card text-card-foreground">
                                        {studentfetch?.map((student, index) => (
                                            <tr key={student._id}>
                                                <td className="border border-border p-2 text-center">{index + 1}</td>
                                                <td className="border border-border p-2">{student.studentName}</td>
                                                <td className="border border-border p-2">{student.courseName}</td>
                                                <td className="border border-border p-2">{student.courseFees}</td>
                                                <td className="border border-border p-2">{student.registrationId}</td>
                                                <td>
                                                    <Button onClick={() => handleViewModal(student)} className='w-[170px] flex gap-2 hover:scale-105 duration-500' variant="contained">Click Here</Button>
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </Box>
            </Box>
            <ViewStudentDetailsModal
                open={viewModal}
                handleClose={handleCloseViewModal}
                studentData={currentStudent}
            />
        </>
    );
};

export default FeeCollection;
