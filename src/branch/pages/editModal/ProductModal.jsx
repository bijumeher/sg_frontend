import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField, Typography } from '@mui/material'
import React from 'react'
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';

const ProductDetails = ({ open1, handleClose1 }) => {
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));
    return (
        <>
            <Dialog
                fullScreen={fullScreen}
                open={open1}
                onClose={handleClose1}
                aria-labelledby="responsive-dialog-title"               
            >
                <Typography className='p-2'>Edit Batch Details</Typography>
                <DialogTitle id="responsive-dialog-title" className='flex flex-col gap-4'>
                    
                    <div >
                        <TextField
                        required
                        id="outlined-required"
                        label="ss ID"
                        defaultValue="Hello World"
                        style={{ marginRight: '20px' }}
                      />
                    
                     
                        <TextField
                        required
                        id="outlined-required"
                        label="Product Name"
                        defaultValue="Hello World"
                        
                      />
                    
                    </div>
                    <div >
                        <TextField
                        required
                        id="outlined-required"
                        label="Date Of Request"
                        defaultValue="Hello World"
                        style={{ marginRight: '20px' }}
                      />
                        <TextField
                        required
                        id="outlined-required"
                        label="Quantity"
                        defaultValue="Hello World"
                      />
                    </div>
                    <div >
                        <TextField
                        required
                        id="outlined-required"
                        label="Amount"
                        defaultValue="Hello World"
                        style={{ marginRight: '20px' }}
                      />
                  
                        <TextField
                        required
                        id="outlined-required"
                        label="Total Amoun"
                        defaultValue="Hello World"
                      />
                      </div>
                </DialogTitle>
                <DialogContent>

                </DialogContent>
                <DialogActions>
                <Button variant="outlined" color="primary" onClick={handleClose1} autoFocus >
                    Cancel
                    </Button>
                    <Button variant="contained" color="primary" onClick={handleClose1} autoFocus style={{ marginRight: '20px' }}>
                        Update
                    </Button>                   
                </DialogActions>
            </Dialog>
        </>
    )
}

export default ProductDetails ;