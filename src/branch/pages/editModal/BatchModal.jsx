import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField, Typography } from '@mui/material';
import React, { useState, useEffect } from 'react';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';

const BatchDetails = ({ open, handleClose, initialData, handleUpdate }) => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

  const [batchData, setBatchData] = useState({
    batchName: '',
    batchTiming: '',
  });
console.log(batchData);

  useEffect(() => {
    if (initialData) {
      setBatchData(initialData);
    }
  }, [initialData]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setBatchData({ ...batchData, [name]: value });
  };

  const handleSubmit = () => {
    handleUpdate(batchData);
  };

  return (
    <Dialog
      fullScreen={fullScreen}
      open={open}
      onClose={handleClose}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogTitle id="responsive-dialog-title">
        <Typography variant="h6">Edit Batch Details</Typography>
      </DialogTitle>
      <DialogContent>
        <TextField
          required
          fullWidth
          margin="normal"
          label="Batch Name"
          name="batchName"
          value={batchData.batchName}
          onChange={handleChange}
        />
        <TextField
          required
          fullWidth
          margin="normal"
          label="Batch Timing"
          name="batchTiming"
          value={batchData.batchTiming}
          onChange={handleChange}
        />
      </DialogContent>
      <DialogActions>
        <Button variant="outlined" color="primary" onClick={handleClose}>
          Cancel
        </Button>
        <Button variant="contained" color="primary" onClick={handleSubmit}>
          Update
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default BatchDetails;
