import React, { useState, useEffect } from 'react';
import { Modal, Box, TextField, Button } from '@mui/material';

const SessionModal = ({ isOpen, onClose, sessionData, onUpdate }) => {
  const [updatedSession, setUpdatedSession] = useState(sessionData);

  useEffect(() => {
    setUpdatedSession(sessionData);
  }, [sessionData]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUpdatedSession({ ...updatedSession, [name]: value });
  };

  const handleUpdate = () => {
    console.log('Updated Session:', updatedSession); // Log updated session data
    onUpdate(updatedSession);
  };

  return (
    <Modal open={isOpen} onClose={onClose}>
      <Box
        sx={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          width: 400,
          bgcolor: 'background.paper',
          border: '2px solid #000',
          boxShadow: 24,
          p: 4,
        }}
      >
        <h2>Edit Session</h2>
        <TextField
          label="Session Name"
          name="sessionName"
          value={updatedSession.sessionName}
          onChange={handleChange}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Session Date"
          name="sessionDate"
          type="date"
          value={updatedSession.sessionDate}
          onChange={handleChange}
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          label="Start Time"
          name="startTime"
          type="time"
          value={updatedSession.startTime}
          onChange={handleChange}
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          label="End Time"
          name="endTime"
          type="time"
          value={updatedSession.endTime}
          onChange={handleChange}
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <Box display="flex" justifyContent="space-between" mt={2}>
          <Button variant="contained" color="primary" onClick={handleUpdate}>
            Update
          </Button>
          <Button variant="contained" color="secondary" onClick={onClose}>
            Cancel
          </Button>
        </Box>
      </Box>
    </Modal>
  );
};

export default SessionModal;
