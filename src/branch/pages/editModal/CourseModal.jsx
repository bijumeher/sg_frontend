import React, { useState, useEffect } from 'react';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField, Select, MenuItem, FormControl, InputLabel } from '@mui/material';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import close from '../../../assets/images/icons8-close-100.png';

const CourseEditDialog = ({ open, handleClose, initialData, handleUpdate }) => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

  const [courseData, setCourseData] = useState({
    courseName: '',
    courseDuration: '',
    coursefees: '',
    courseType: '',
  });

  const courseTypes = ['Short Term', 'Long Term'];

  useEffect(() => {
    if (initialData) {
      setCourseData({
        courseName: initialData.courseName || '',
        courseDuration: initialData.courseDuration || '',
        coursefees: initialData.coursefees || '',
        courseType: initialData.courseType || '',
      });
    }
  }, [initialData]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setCourseData({ ...courseData, [name]: value });
  };

  const handleSave = () => {
    handleUpdate(courseData);
  };

  return (
    <Dialog
      fullScreen={fullScreen}
      open={open}
      onClose={handleClose}
      aria-labelledby="responsive-dialog-title"
      className="rounded-md"
    >
      <DialogTitle id="responsive-dialog-title">
        <div className="flex justify-between items-center">
          <span className="text-blue-600">Edit Course</span>
          <button onClick={handleClose}>
            <img src={close} alt="close" className="w-6 h-6" />
          </button>
        </div>
      </DialogTitle>
      <DialogContent>
        <div className="grid grid-cols-1 gap-4">
          <TextField
            id="course-name"
            label="Course Name"
            name="courseName"
            value={courseData.courseName}
            onChange={handleChange}
            fullWidth
            variant="outlined"
          />
          <TextField
            id="course-duration"
            label="Duration"
            name="courseDuration"
            value={courseData.courseDuration}
            onChange={handleChange}
            fullWidth
            variant="outlined"
          />
          <TextField
            id="course-fees"
            label="Fees"
            name="coursefees"
            value={courseData.coursefees}
            onChange={handleChange}
            fullWidth
            variant="outlined"
          />
          <FormControl fullWidth variant="outlined">
            <InputLabel id="course-type-label">Course Type</InputLabel>
            <Select
              labelId="course-type-label"
              id="course-type"
              name="courseType"
              value={courseData.courseType}
              onChange={handleChange}
              label="Course Type"
            >
              {courseTypes.map((type, index) => (
                <MenuItem key={index} value={type}>{type}</MenuItem>
              ))}
            </Select>
          </FormControl>
        </div>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={handleClose}
          className="bg-gray-200 text-gray-600 hover:bg-gray-300"
        >
          Cancel
        </Button>
        <Button
          onClick={handleSave}
          autoFocus
          className="bg-gray-200 text-gray-600 hover:bg-gray-300"
        >
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default CourseEditDialog;
