import React, { useState, useEffect } from 'react';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField, Typography, MenuItem, Select, InputLabel, FormControl, Checkbox } from '@mui/material';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import { useDispatch, useSelector } from 'react-redux';
import { getBranchDataByid } from '../../../actions/branch/branchAction';

const StudentDetailsEditModal = ({ open, handleClose, initialData, handleUpdate }) => {
  const branchData = JSON.parse(sessionStorage.getItem('branchData'));
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getBranchDataByid({ branchId: branchData._id }));
  }, [dispatch, branchData._id]);

  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));
  const branchesdata = useSelector(state => state.masterbranc.fetchbranch);

  const [studentData, setStudentData] = useState({
    studentName: '',
    fatherName: '',
    gender: '',
    caste: '',
    email: '',
    aadharNumber: '',
    courseName: '',
    mobNumber: '',
    registrationId: '',
    dateOfRegister: '',
    branchName: '',
    batchName: '',
  });

  useEffect(() => {
    if (initialData) {
      setStudentData(initialData);
    }
  }, [initialData]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setStudentData({ ...studentData, [name]: value });
  };

  const handleSubmit = () => {
    handleUpdate(studentData);
  };

  return (
    <Dialog
      fullScreen={fullScreen}
      open={open}
      onClose={handleClose}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogTitle id="responsive-dialog-title" className='flex flex-col gap-4'>
        <Typography className='p-2'>Edit Student Details</Typography>
        <TextField
          required
          label="Student Name"
          name="studentName"
          value={studentData.studentName}
          onChange={handleChange}
        />
        <TextField
          required
          label="Father Name"
          name="fatherName"
          value={studentData.fatherName}
          onChange={handleChange}
        />
        <TextField
          required
          label="Gender"
          name="gender"
          value={studentData.gender}
          onChange={handleChange}
        />
        <TextField
          required
          label="Caste"
          name="caste"
          value={studentData.caste}
          onChange={handleChange}
        />
        <TextField
          required
          label="Email"
          name="email"
          value={studentData.email}
          onChange={handleChange}
        />
        <TextField
          required
          label="Aadhar Number"
          name="aadharNumber"
          value={studentData.aadharNumber}
          onChange={handleChange}
        />
        <FormControl fullWidth>
          <InputLabel>Course Name</InputLabel>
          <Select
            multiple
            name="courseName"
            value={studentData.courseName || []} // Ensure this is an array
            onChange={handleChange}
            renderValue={(selected) => selected.join(', ')} // Display selected values as comma-separated
          >
            {branchesdata && branchesdata.length > 0 && branchesdata.flatMap(branch =>
              branch.courses ? branch.courses.map(course => (
                <MenuItem key={course._id} value={course.courseName}>
                  <Checkbox checked={studentData.courseName.indexOf(course.courseName) > -1} />
                  {course.courseName}
                </MenuItem>
              )) : []
            )}
          </Select>
        </FormControl>

        <TextField
          required
          label="Mobile Number"
          name="mobNumber"
          value={studentData.mobNumber}
          onChange={handleChange}
        />
        <TextField
          label="Registration ID"
          name="registrationId"
          value={studentData.registrationId}
          InputProps={{
            readOnly: true,
          }}
        />
        <TextField
          required
          label="Date of Registration"
          name="dateOfRegister"
          value={studentData.dateOfRegister}
          onChange={handleChange}
        />
        <TextField
          required
          label="Branch Name"
          name="branchName"
          value={studentData.branchName}
          onChange={handleChange}
        />
        <FormControl fullWidth>
          <InputLabel>Batch Name</InputLabel>
          <Select
            name="batchName"
            value={studentData.batchName || ""}
            onChange={handleChange}
          >
            {branchesdata && branchesdata.length > 0 && branchesdata.flatMap(branch => (
              branch.batches ? branch.batches.map(batch => (
                <MenuItem key={batch._id} value={batch.batchName}>{batch.batchName}</MenuItem>
              )) : []
            ))}
          </Select>
        </FormControl>
      </DialogTitle>
      <DialogContent />
      <DialogActions>
        <Button variant="outlined" color="primary" onClick={handleClose} autoFocus>
          Cancel
        </Button>
        <Button variant="contained" color="primary" onClick={handleSubmit} autoFocus>
          Update
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default StudentDetailsEditModal;
