import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import alertReducer from "./apialert/apialert";
import adminReducerdata from "./admin/adminReducer";
import batchReducerdata from "./batch/batchReducer";
import courseReducerdata from "./course/courseReducer";
import branchReducer from "./branch/branchReducer";
import studentReducer from "./student/student";
import sessionReducerdata from "./session/sessionReducer";
import examReducer from "./exam/examReducer";
const rootReducer = (history) =>
  combineReducers({
    router: routerReducer(history),
    alert:alertReducer,
    admin:adminReducerdata,
    batch:batchReducerdata,
    course:courseReducerdata,
    masterbranc:branchReducer,
    student:studentReducer,
    session:sessionReducerdata,
    exam:examReducer
  });
// const rootReducer = combineReducers({
//   router: routerReducer(),
//   alert: AlertReducer,
// });

export default rootReducer;