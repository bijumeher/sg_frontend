const initialState = {
    sessionData: null,
    allSessionData: []
};


const sessionReducerdata = (state = initialState, action) => {
    switch (action.type) {
        case "ADD_SESSION_ALERT":
            return {
                ...state,
                sessionData: action.payload,

            };
        case "CLEAR_BATCH_DATA":
            return {
                ...state,
                sessionData: null
            };
        default:
            return state;
    }
};

export default sessionReducerdata;