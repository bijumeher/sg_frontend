const initialState = {
  studentData: null,
  loading: false,
  attendanceData: [],
  registrationId: null,
  paymentData: null,
  registerData: null,
  paymentsData: null,
  studentattendancedata: null,
  fetchattendance: null,
  studentid: []

};

const studentReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_BRANCHE_DATA':
      return {
        ...state,
        studentData: action.payload,
        loading: false
      };
    case 'GET_STUDENTS_BY_BATCH':
      return {
        ...state,
        studentData: action.payload,
      };
    case 'SET_REGISTRATION_ID':
      return {
        ...state,
        registrationId: action.payload,
      };
    case "ADD_PAYMENT_ALERT":
      return {
        ...state,
        paymentData: action.payload,

      };
    case "ADD_STUDENT_ALERT":
      return {
        ...state,
        registerData: action.payload,

      };
    case 'FETCH_PAYMENT_ID_SUCCESS':
      return {
        ...state,
        paymentsData: action.payload,
      };
    case "ADD_ATTENDANCE_ALERT":
      return {
        ...state,
        studentattendancedata: action.payload,

      };
    case 'FETCH_ATTENDANCE_ID_SUCCESS':
      return {
        ...state,
        fetchattendance: [action.payload],
        loading: false
      };
    case 'ADD_STUDENTID_ALERT':
      return {
        ...state,
        studentid: action.payload,
      };
    default:
      return state;
  }
};

export default studentReducer;