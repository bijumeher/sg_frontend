const initialState = {
    coursesData: null,
    allCourseData: []
};


const courseReducerdata = (state = initialState, action) => {
    switch (action.type) {
        case "ADD_COURSE_ALERT":
            return {
                ...state,
                courseData: action.payload,

            };
        case "CLEAR_BATCH_DATA":
            return {
                ...state,
                courseData: null
            };
        default:
            return state;
    }
};

export default courseReducerdata;