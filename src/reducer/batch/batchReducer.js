const initialState = {
    batchesData: null,
    allBatchData: []
};


const batchReducerdata = (state = initialState, action) => {
    switch (action.type) {
        case "ADD_BATCH_ALERT":
            return {
                ...state,
                batchData: action.payload,

            };
        case 'UPDATE_BATCH_SUCCESS':
            console.log('Batch updated successfully:', action.payload);
            return { ...state, batches: action.payload };
        case "CLEAR_BATCH_DATA":
            return {
                ...state,
                batchData: null
            };
        default:
            return state;
    }
};

export default batchReducerdata;