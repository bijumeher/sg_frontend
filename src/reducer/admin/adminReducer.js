// alertReducer.js
const initialState = {
    userData: {},
    token: '',
    loginData: null
};


const adminReducerdata = (state = initialState, action) => {
    switch (action.type) {
        case "ADD_ADMIN_DATA":
            return {
                ...state,
                loginData: {
                    token: action.payload.token,
                    userData: action.payload.userdata
                }

            };
        case "CLEAR_ADMIN_DATA":
            return {
                ...state,
                loginData: null
            };
        default:
            return state;
    }
};

export default adminReducerdata;