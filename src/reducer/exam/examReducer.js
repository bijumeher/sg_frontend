// reducers/examReducer.js

const initialState = {
    examData: null,  // Holds the list of exams
    examdata: null
};

const examReducer = (state = initialState, action) => {
    switch (action.type) {
        case "ADD_EXAM_DATA":
            return {
                ...state,
                examData: action.payload
            };
        case 'REMOVE_EXAM_DATA':
            return {
                ...state,
                examdata: state.examdata.filter(exam => exam._id !== action.payload), // Remove the exam by its ID
            };
        case 'CLEAR_EXAM_DATA':  // Optional: To clear exam data if needed
            return {
                ...state,
                examData: [],
            };
        default:
            return state;
    }
};

export default examReducer;
