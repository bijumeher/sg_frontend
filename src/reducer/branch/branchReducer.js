const initialState = {
  fetchbranch: null,
  allbranch: null,
  loading: false
};

const branchReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'FETCH_BRANCHES_SUCCESS':
      return {
        ...state,
        allbranch: action.payload,
        loading: false
      };
    case 'FETCH_BRANCHES_ID_SUCCESS':
      return {
        ...state,
        fetchbranch:[action.payload],
        loading: false
      };
    default:
      return state;
  }
};

export default branchReducer;
