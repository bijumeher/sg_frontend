import { Box, Drawer, Toolbar } from '@mui/material'
import React from 'react'
const drawerWidth = 270;

const StudyMateriall = () => {
    return (
        <>
            <Box sx={{ display: 'flex' }}>
                <Drawer
                    variant="permanent"
                    sx={{
                        width: drawerWidth,
                    }}
                >
                </Drawer>

                <Box component="main" sx={{ flexGrow: 1 }} className='  mt-[-20px]' >
                    <Toolbar />
                    <div className="p-4">
                    <div className=" text-xl text-zinc-600 mb-4">
                            <a href="#" className="text-blue-600">🏠Home</a> /
                             <span>Study Material</span>
                        </div>
                        <div className="bg-gradient-to-r from-red-500 to-yellow-500 text-white p-4 rounded-t-lg">Download Study Material</div>
                        <table className="min-w-full bg-white border border-zinc-200">
                            <thead>
                                <tr className="bg-zinc-100">
                                    <th className="py-2 px-4 border-b">Sl. No.</th>
                                    <th className="py-2 px-4 border-b">Subject Name</th>
                                    <th className="py-2 px-4 border-b">Material</th>
                                    <th className="py-2 px-4 border-b">Material</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className="py-2 px-4 border-b text-center">1</td>
                                    <td className="py-2 px-4 border-b">PGDCA</td>
                                    <td className="py-2 px-4 border-b">HTML</td>
                                    <td className="py-2 px-4 border-b">
                                        <a href="#" className="text-blue-500">HTML NOTES.pdf</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="py-2 px-4 border-b text-center">2</td>
                                    <td className="py-2 px-4 border-b">PGDCA</td>
                                    <td className="py-2 px-4 border-b">FUNDAMENTAL OF COMPUTER</td>
                                    <td className="py-2 px-4 border-b">
                                        <a href="#" className="text-blue-500">computer_fundamental.pdf</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="py-2 px-4 border-b text-center">3</td>
                                    <td className="py-2 px-4 border-b">PGDCA</td>
                                    <td className="py-2 px-4 border-b">POWER POINT BOOKS</td>
                                    <td className="py-2 px-4 border-b">
                                        <a href="#" className="text-blue-500">POWER POINT BOOKS.pdf</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </Box>
            </Box>
        </>
    )
}

export default StudyMateriall