import { Box, Drawer, Toolbar } from '@mui/material'
import myy from '../../assets/images/biju.jpeg'
import React, { useEffect } from 'react'
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import { Height } from '@mui/icons-material';
import { useDispatch, useSelector } from 'react-redux';
import { getStudentdataById } from '../../../actions/studentregister/studentRegisterAction';
// import Tab from '@mui/material/Tab';
// import TabContext from '@mui/lab/TabContext';
// import TabList from '@mui/lab/TabList';
// import TabPanel from '@mui/lab/TabPanel';



// Tab section start//
// Tab section End//
const drawerWidth = 270;
const MyDashboard = () => {
    const dispatch = useDispatch();

    const registerData = JSON.parse(sessionStorage.getItem('registerData'));

    useEffect(() => {
        if (registerData && registerData._id) {
            dispatch(getStudentdataById(registerData._id));
        }
    }, [dispatch, registerData?._id]);

    const studentfetch = useSelector(state => state.student.studentid) || [];
    return (
        <Box sx={{ display: 'flex' }}>
            <Drawer
                variant="permanent"
                sx={{
                    width: drawerWidth,
                }}
            >
            </Drawer>

            <Box component="main" sx={{ flexGrow: 1 }} className=' mt-[-60px]' >
                <Toolbar />
                <div className="p-4">
                    <h1 className="text-2xl font-bold mb-4">Student Dashboard</h1>
                    <div className="bg-zinc-200 p-2 mb-4 flex items-center">
                        <span className="mr-2">🏠</span>
                        <a href="#" className="text-blue-500">Home</a>
                    </div>
                    <Grid container sx={{ display: 'flex', justifyContent: 'space-between' }} gap={2}>
                        <Grid item sx={4} sm={12} md={6}>

                            <div className="border-b-2 border-zinc-300 mb-4">
                                <ul className="flex">
                                    <li className="mr-4 pb-2 border-b-2 border-blue-500 text-blue-500">Basic Information</li>
                                </ul>
                            </div>
                            <div className="flex justify-between mb-4">
                                <div>
                                    <p><strong>Student Name:</strong> {studentfetch.studentName}</p>
                                    <p><strong>Student Email:</strong> {studentfetch.email}</p>
                                    <p><strong>Student Mobile:</strong> {studentfetch.mobNumber}</p>
                                    <p><strong>Date of Admission:</strong> {studentfetch.dateOfRegister}</p>
                                    <p><strong>Course Assigned:</strong> {studentfetch.courseName}</p>
                                    <p><strong>Batch Assigned:</strong> {studentfetch.batchName}</p>
                                </div>
                                <div>
                                    <img 
                                        src={studentfetch.uploadPhoto} 
                                        alt="Student Photo" 
                                        className="w-24 h-24 object-cover border border-zinc-300 rounded" 
                                    />
                                </div>
                            </div>
                            <div className="flex space-x-4">
                                <button className="bg-green-500 text-white px-4 py-2 rounded">View My ID Card</button>
                                <button className="bg-red-500 text-white px-4 py-2 rounded">Print</button>
                            </div>
                        </Grid>
                        <Grid item sx={6} sm={4} md={5}>
                            <Card sx={{ textAlign: 'center', border: '4px solid rgba(255 242 0)' }}>
                                <u className="  border-blue-500 text-blue-500 text-2xl items-center " ><b >Notice Board</b></u>
                                <p >Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ut qui similique, magnam quod, reprehenderit, ex nesciunt consequatur quasi quae excepturi dolor aliquam accusantium ea? Iste, pariatur! Id sapiente nostrum natus.</p>
                            </Card>

                        </Grid>
                    </Grid>
                    <div className="w-full mt-4">
                        <h2 className="text-xl font-bold mb-2">Payment History Details</h2>
                        <table className="w-full border-collapse border border-zinc-300">
                            <thead>
                                <tr className="bg-zinc-100">
                                    <th className="border border-zinc-300 p-2">Sl No.</th>
                                    <th className="border border-zinc-300 p-2">MR Number</th>
                                    <th className="border border-zinc-300 p-2">Payment Head</th>
                                    <th className="border border-zinc-300 p-2">Payment Date</th>
                                    <th className="border border-zinc-300 p-2">Total Amount</th>
                                    <th className="border border-zinc-300 p-2">Paid Amount</th>
                                    <th className="border border-zinc-300 p-2">Remaining</th>
                                    <th className="border border-zinc-300 p-2">Payment Mode</th>
                                    <th className="border border-zinc-300 p-2">Next Payment Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className="border border-zinc-300 p-2" colspan="9">No Advance Added Yet...</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>

            </Box>
        </Box>
    )
}

export default MyDashboard