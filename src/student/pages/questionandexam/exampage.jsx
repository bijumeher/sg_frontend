import React, { useState, useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { Button, Modal } from 'react-bootstrap';
import { FaChevronLeft, FaChevronRight } from 'react-icons/fa';
import { deleteExamPaper } from '../../../actions/exam/examAction';
import { useDispatch } from 'react-redux';
import { toast } from 'react-hot-toast';

const ExamPage = () => {
    const location = useLocation();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const exam = location.state?.exam || {}; // Ensure exam is defined
    const student = location.state?.student || {};
    const [currentQuestionIndex, setCurrentQuestionIndex] = useState(
        () => JSON.parse(localStorage.getItem('currentQuestionIndex')) || 0
    );
    const [timer, setTimer] = useState(() => JSON.parse(localStorage.getItem('timer')) || 1 * 60); // 1 minute timer
    const [selectedAnswers, setSelectedAnswers] = useState(
        () => JSON.parse(localStorage.getItem('selectedAnswers')) || []
    );
    const [showConfirmModal, setShowConfirmModal] = useState(false); // Confirmation modal

    // Save the exam state to localStorage
    useEffect(() => {
        localStorage.setItem('currentQuestionIndex', JSON.stringify(currentQuestionIndex));
        localStorage.setItem('timer', JSON.stringify(timer));
        localStorage.setItem('selectedAnswers', JSON.stringify(selectedAnswers));
    }, [currentQuestionIndex, timer, selectedAnswers]);

    useEffect(() => {
        if (timer === 0) {
            handleAutoSubmit(); // Automatically submit when time is up
            return;
        }
        const intervalId = setInterval(() => {
            setTimer(prev => {
                if (prev <= 0) {
                    clearInterval(intervalId);
                    return 0;
                }
                return prev - 1;
            });
        }, 1000);
        return () => clearInterval(intervalId);
    }, [timer]);

    const handleNext = () => {
        if (currentQuestionIndex < exam.questions.length - 1) {
            setCurrentQuestionIndex(prev => prev + 1);
        }
    };

    const handlePrevious = () => {
        if (currentQuestionIndex > 0) {
            setCurrentQuestionIndex(prev => prev - 1);
        }
    };

    const handleAnswerChange = (event, option) => {
        const newSelectedAnswers = [...selectedAnswers];
        newSelectedAnswers[currentQuestionIndex] = option === newSelectedAnswers[currentQuestionIndex] ? '' : option;
        setSelectedAnswers(newSelectedAnswers);
    };

    const handleQuestionClick = (index) => {
        setCurrentQuestionIndex(index);
    };

    const handleSubmit = async (examId) => {
        try {
            // Prepare request payload
            const payload = {
                studentId: student._id, // Ensure student ID is available
                examId,
                responses: selectedAnswers,
            };
    
            // Send POST request to submit exam
            const response = await fetch('http://localhost:3500/api/student/submit/exam', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(payload),
            });
    
            const result = await response.json();
    
            if (response.ok) {
                // Handle success
                toast.success('Exam submitted successfully!', {
                    position: 'top-center',
                });
                navigate('/examdetails')
            } else {
                // Handle error
                toast.error(`Error: ${result.error}`, {
                    position: 'top-center',
                });
            }
    
            // Clear local storage
            localStorage.removeItem('currentQuestionIndex');
            localStorage.removeItem('timer');
            localStorage.removeItem('selectedAnswers');
            dispatch(deleteExamPaper(examId));
        } catch (error) {
            toast.error(`Submission failed: ${error.message}`, {
                position: 'top-center',
            });
        }
    };
    

    const handleAutoSubmit = () => {
        handleSubmit(exam._id); // Automatically submit the exam
    };

    const formatTime = (seconds) => {
        const minutes = Math.floor(seconds / 60);
        const secs = seconds % 60;
        return `${minutes}:${secs < 10 ? '0' : ''}${secs}`;
    };

    return (
        <div className="flex flex-col min-h-screen p-4">
            <div className="flex items-center justify-between p-4 mb-4 bg-gray-100 rounded shadow-md">
                <div>
                    <h2 className="text-2xl font-bold">Exam: {exam.title || 'N/A'}</h2>
                    <div>End Exam: {formatTime(timer)}</div>
                </div>
                <div className="flex items-center">
                    <div className="text-right mr-4">
                        <p className="text-lg font-semibold">Student Name:</p>
                        <p>{student.studentName}</p>
                    </div>
                    {student.uploadPhoto && (
                        <img src={student.uploadPhoto} alt="Student" className="w-20 h-20 rounded-full border shadow-md" />
                    )}
                </div>
            </div>

            <div className="flex flex-grow">
                <div className="flex-grow">
                    {exam.questions && exam.questions.length > 0 ? (
                        <div>
                            <div className="mb-2">
                                Question {currentQuestionIndex + 1} of {exam.questions.length}
                            </div>
                            <div className="mb-4">
                                <p className="font-bold">
                                    {currentQuestionIndex + 1}. {exam.questions[currentQuestionIndex]?.question}
                                </p>
                                <div className="mt-4">
                                    {exam.questions[currentQuestionIndex]?.options.map((option, i) => {
                                        const optionLabels = ['A', 'B', 'C', 'D'];
                                        return (
                                            <div key={i} className="flex items-center mb-2">
                                                <input
                                                    type="checkbox"
                                                    id={`q${currentQuestionIndex}o${i}`}
                                                    name={`q${currentQuestionIndex}`}
                                                    value={option}
                                                    checked={selectedAnswers[currentQuestionIndex] === option}
                                                    onChange={(e) => handleAnswerChange(e, option)}
                                                    className="mr-2"
                                                />
                                                <label htmlFor={`q${currentQuestionIndex}o${i}`} className="cursor-pointer">
                                                    <span className="font-semibold mr-2">{optionLabels[i]}.</span>
                                                    {option}
                                                </label>
                                            </div>
                                        );
                                    })}
                                </div>
                            </div>
                            <div className="flex gap-4 mb-4">
                                <Button
                                    onClick={handlePrevious}
                                    disabled={currentQuestionIndex === 0}
                                    style={{
                                        backgroundColor: '#007bff',
                                        border: 'none',
                                        color: 'white',
                                        padding: '10px 20px',
                                        fontSize: '16px',
                                        borderRadius: '4px',
                                        display: 'flex',
                                        alignItems: 'center',
                                    }}
                                >
                                    <FaChevronLeft style={{ marginRight: '5px' }} />
                                    Previous
                                </Button>
                                <Button
                                    onClick={handleNext}
                                    disabled={currentQuestionIndex === exam.questions.length - 1}
                                    style={{
                                        backgroundColor: '#007bff',
                                        border: 'none',
                                        color: 'white',
                                        padding: '10px 20px',
                                        fontSize: '16px',
                                        borderRadius: '4px',
                                        display: 'flex',
                                        alignItems: 'center',
                                    }}
                                >
                                    Next
                                    <FaChevronRight style={{ marginLeft: '5px' }} />
                                </Button>
                            </div>
                            <div className="flex justify-start mb-4">
                                <Button variant="danger" onClick={() => setShowConfirmModal(true)} style={{ backgroundColor: '#dc3545', border: 'none' }}>Submit Exam</Button>
                            </div>
                        </div>
                    ) : (
                        <div>No questions available</div>
                    )}
                </div>

                <div className="w-1/4 ml-4 p-4 border-3 border-solid border-gray-700 rounded shadow-md">
                    <h4 className="text-center mb-4 font-bold">Questions</h4>
                    <div className="grid grid-cols-5 gap-2">
                        {exam.questions?.map((_, index) => (
                            <div
                                key={index}
                                className={`flex items-center justify-center text-center cursor-pointer ${selectedAnswers[index] ? 'bg-green-500 text-white' : 'bg-gray-300'} ${index === currentQuestionIndex ? 'border-2 border-blue-500' : ''}`}
                                onClick={() => handleQuestionClick(index)}
                                style={{
                                    width: '40px',
                                    height: '40px',
                                    borderRadius: '50%',
                                    padding: '10px',
                                    color: 'white', // Ensure text color is white for contrast
                                }}
                            >
                                {index + 1}
                            </div>
                        ))}
                    </div>
                </div>
            </div>

            {/* Confirmation Modal */}
            <Modal show={showConfirmModal} onHide={() => setShowConfirmModal(false)}>
                <Modal.Header closeButton>
                    <Modal.Title>Submit Exam</Modal.Title>
                </Modal.Header>
                <Modal.Body>Are you sure you want to submit the exam?</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => setShowConfirmModal(false)}>
                        Cancel
                    </Button>
                    <Button variant="danger" onClick={() => handleSubmit(exam._id)}>
                        Submit Exam
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
};

export default ExamPage;
