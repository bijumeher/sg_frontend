import { Box, Drawer, Toolbar } from '@mui/material'
import React from 'react'
const drawerWidth = 270;
const ViewExamResultsdetails = () => {
    return (
        <>
            <Box sx={{ display: 'flex' }}>
                <Drawer
                    variant="permanent"
                    sx={{
                        width: drawerWidth,
                    }}
                >
                </Drawer>

                <Box component="main" sx={{ flexGrow: 1 }} className=' mt-[-20px]' >
                    <Toolbar />
                    <div class="p-4">
                        <div class="text-xl font-bold mb-2">Student Dashboard | View Exam Result</div>
                        <div className=" text-xl text-zinc-600 mb-4">
                            <a href="#" className="text-blue-600">🏠Home</a> /
                            <a href="#" className="text-blue-600">Question & Exam </a> / <span>View Exam Results</span>
                        </div>
                        <div class="bg-gradient-to-r from-red-500 to-yellow-500 text-white p-4 rounded-t-lg">view Exam Result</div>
                        <div class="overflow-x-auto">
                            <table class="min-w-full bg-white border border-zinc-200">
                                <thead>
                                    <tr class="bg-zinc-800 text-white">
                                        <th class="py-2 px-4 border">Student Name</th>
                                        <th class="py-2 px-4 border">D.O.B</th>
                                        <th class="py-2 px-4 border">BatchName</th>
                                        <th class="py-2 px-4 border">Course</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="py-2 px-4 border">Aliva Jena</td>
                                        <td class="py-2 px-4 border">25-May-2000</td>
                                        <td class="py-2 px-4 border">PGDCA (9.00 - 10.00) (9.00 - 10.00)</td>
                                        <td class="py-2 px-4 border">PGDCA</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="overflow-x-auto mt-4">
                            <table class="min-w-full bg-white border border-zinc-200">
                                <thead>
                                    <tr class="bg-zinc-800 text-white">
                                        <th class="py-2 px-4 border">Name of Examination</th>
                                        <th class="py-2 px-4 border">Date of Examination</th>
                                        <th class="py-2 px-4 border">Total Question</th>
                                        <th class="py-2 px-4 border">Answer Given</th>
                                        <th class="py-2 px-4 border">Correct Answer</th>
                                        <th class="py-2 px-4 border">Wrong Answer</th>
                                        <th class="py-2 px-4 border">Total Mark</th>
                                        <th class="py-2 px-4 border">Mark Obtain</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </Box>
            </Box>
        </>
    )
}

export default ViewExamResultsdetails