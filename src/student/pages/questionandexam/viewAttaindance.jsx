import React, { useEffect, useState } from 'react';
import { Box, Drawer, Toolbar, Button, Modal, Typography } from '@mui/material';
import { PieChart, Pie, Cell, Tooltip, Legend } from 'recharts';
import { useDispatch, useSelector } from 'react-redux';
import { getStudentdataById } from '../../../actions/studentregister/studentRegisterAction';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CancelIcon from '@mui/icons-material/Cancel';

const COLORS = ['#0088FE', '#FF8042', '#00C49F', '#FFBB28']; // Colors for the pie chart

const drawerWidth = 270;

const ViewAttendancedetails = () => {
    const dispatch = useDispatch();
    const registerData = JSON.parse(sessionStorage.getItem('registerData'));

    useEffect(() => {
        if (registerData && registerData._id) {
            dispatch(getStudentdataById(registerData._id));
        }
    }, [dispatch, registerData?._id]);

    const studentfetch = useSelector(state => state.student.studentid);

    const [selectedSubjectsDetails, setSelectedSubjectsDetails] = useState([]);
    const [openModal, setOpenModal] = useState(false);

    const handleDetailsClick = (subjectName) => {
        const subjectDetails = studentfetch.attendances.filter(
            (attendance) => attendance.subjectname === subjectName
        );
        setSelectedSubjectsDetails(subjectDetails);
        setOpenModal(true); // Open the modal
    };

    const handleClose = () => setOpenModal(false); // Close the modal

    const uniqueSubjects = (studentfetch.attendances || []).reduce((acc, curr) => {
        if (!acc.some(item => item.subjectname === curr.subjectname)) {
            acc.push(curr);
        }
        return acc;
    }, []);

    // Calculate attendance data for each subject
    const calculateAttendanceDataForSubjects = () => {
        return uniqueSubjects.flatMap(subject => {
            const subjectAttendances = studentfetch.attendances.filter(item => item.subjectname === subject.subjectname);
            const total = subjectAttendances.length;
            const presentCount = subjectAttendances.filter(item => item.attendancestatus === 'present').length;
            const absentCount = total - presentCount;

            return [
                { name: `${subject.subjectname} - Present`, value: total > 0 ? (presentCount / total) * 100 : 0 },
                { name: `${subject.subjectname} - Absent`, value: total > 0 ? (absentCount / total) * 100 : 0 }
            ];
        });
    };

    return (
        <>
            <Box sx={{ display: 'flex' }}>
                <Drawer
                    variant="permanent"
                    sx={{
                        width: drawerWidth,
                    }}
                />
                <Box component="main" sx={{ flexGrow: 1 }} className='mt-[-20px]'>
                    <Toolbar />
                    <div className="p-4">
                        <div className="text-lg font-semibold mb-2">Student Dashboard | View Attendance Details</div>
                       
                        <div className="bg-gradient-to-r from-red-500 to-yellow-500 text-white p-4 rounded-t-lg">View Attendance  Details</div>
                        <div className="border border-t-0 rounded-b p-4">
                            <div className="flex flex-col md:flex-row">
                                {/* Table Section */}
                                <div className="w-full md:w-1/2 p-2">
                                    <table className="w-full text-left border-collapse">
                                        <thead>
                                            <tr>
                                                <th className="px-4 py-2 border">Subject Name</th>
                                                <th className="px-4 py-2 border">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {uniqueSubjects.map((subject, index) => (
                                                <tr key={index}>
                                                    <td className="px-4 py-2 border">{subject.subjectname}</td>
                                                    <td className="px-4 py-2 border">
                                                        <Button
                                                            variant="outlined"
                                                            size="small"
                                                            onClick={() => handleDetailsClick(subject.subjectname)}
                                                        >
                                                            Details
                                                        </Button>
                                                    </td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                                {/* Pie Chart Section */}
                                <div className="w-full md:w-1/2 p-2">
                                    <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                        <Typography variant="h6" component="div" className="font-semibold mb-2">
                                            Subject-wise Attendance Summary
                                        </Typography>
                                        <PieChart width={500} height={400}>
                                            <Pie
                                                data={calculateAttendanceDataForSubjects()}
                                                cx={250}
                                                cy={160}
                                                outerRadius={120} // Increased radius to make the pie chart bigger
                                                labelLine={false}
                                                label={({ value }) => `${value.toFixed(1)}%`}
                                                fill="#8884d8"
                                                dataKey="value"
                                            >
                                                {calculateAttendanceDataForSubjects().map((entry, index) => (
                                                    <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                                                ))}
                                            </Pie>
                                            <Tooltip formatter={(value, name) => [`${value.toFixed(1)}%`, name]} />
                                            <Legend
                                                wrapperStyle={{ marginTop: '20px' }} // Add margin to move the legend down
                                            />
                                        </PieChart>
                                    </Box>
                                </div>
                            </div>
                        </div>
                    </div>
                </Box>
            </Box>

            <Modal
                open={openModal}
                onClose={handleClose}
                aria-labelledby="modal-title"
                aria-describedby="modal-description"
            >
                <Box sx={{
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                    transform: 'translate(-50%, -50%)',
                    width: 500,
                    maxHeight: '80vh', // Limit the height of the modal
                    bgcolor: 'background.paper',
                    borderRadius: '8px',
                    boxShadow: 24,
                    p: 4,
                    display: 'flex',
                    flexDirection: 'column',
                    overflowY: 'auto',
                }}>
                    <Typography id="modal-title" variant="h6" component="h2">
                        Attendance Details
                    </Typography>
                    {selectedSubjectsDetails.length > 0 && (
                        <Box sx={{ display: 'flex', flexDirection: 'row', mt: 2, width: '100%' }}>
                            <Box sx={{ width: '100%', px: 2 }}>
                                <table className="w-full text-left border-collapse">
                                    <thead>
                                        <tr>
                                            <th className="px-4 py-2 border">Attendance Date</th>
                                            <th className="px-4 py-2 border">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {selectedSubjectsDetails.map((details, index) => (
                                            <tr key={index}>
                                                <td className="px-4 py-2 border">{details.attendancedate}</td>
                                                <td className="px-4 py-2 border">
                                                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                                                        {details.attendancestatus === 'present' ? (
                                                            <>
                                                                <CheckCircleIcon color="success" sx={{ marginRight: 1 }} />
                                                                <Typography variant="body2" color="textSecondary">Present</Typography>
                                                            </>
                                                        ) : (
                                                            <>
                                                                <CancelIcon color="error" sx={{ marginRight: 1 }} />
                                                                <Typography variant="body2" color="textSecondary">Absent</Typography>
                                                            </>
                                                        )}
                                                    </Box>
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </Box>
                        </Box>
                    )}
                </Box>
            </Modal>
        </>
    );
};

export default ViewAttendancedetails;
