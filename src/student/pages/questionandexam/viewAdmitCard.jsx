import { Box, Drawer, Toolbar } from '@mui/material'
import React from 'react'
const drawerWidth = 270;
const ViewAdmitCard = () => {
    return (
        <Box sx={{ display: 'flex' }}>
            <Drawer
                variant="permanent"
                sx={{
                    width: drawerWidth,
                }}
            >
            </Drawer>

            <Box component="main" sx={{ flexGrow: 1 }}  className=' mt-[-20px]' >
                <Toolbar />
                <div class="p-4">
                    <div class="text-xl font-semibold mb-2">Student Dashboard | View Admit Card</div>
                    <div className=" text-xl text-zinc-600 mb-4">
                            <a href="#" className="text-blue-600">🏠Home</a> /
                             <span>View AdmitCard</span>
                        </div>
                    <div class="border rounded-lg overflow-hidden">
                        <div class="bg-gradient-to-r from-red-500 to-yellow-500 text-white p-4 rounded-t-lg">Admit Card Details</div>
                        <table class="min-w-full bg-white">
                            <thead>
                                <tr class="w-full bg-zinc-100">
                                   
                                    
                                    <th class="py-2 px-4 border-b">Student Name</th>
                                    <th class="py-2 px-4 border-b">Date Of Birth</th>
                                    <th class="py-2 px-4 border-b">Student ID</th>
                                    <th class="py-2 px-4 border-b">Roll Number</th>
                                    <th class="py-2 px-4 border-b">View</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="py-2 px-4 border-b text-center" colspan="6">No Data Found...</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </Box>
        </Box>

    )
}

export default ViewAdmitCard