import { Box, Drawer, Toolbar, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Checkbox, FormControlLabel } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import { getStudentdataById } from '../../../actions/studentregister/studentRegisterAction';

const drawerWidth = 270;

const ViewExamdetails = () => {
    const [exams, setExams] = useState([]);
    const [open, setOpen] = useState(false);
    const [selectedExam, setSelectedExam] = useState(null);
    const [acknowledged, setAcknowledged] = useState(false);

    const dispatch = useDispatch();
    const navigate = useNavigate();
    const studentfetch = useSelector(state => state.student.studentid);

    // Fetch student data from sessionStorage
    const registerData = JSON.parse(sessionStorage.getItem('registerData'));

    useEffect(() => {
        if (registerData && registerData._id) {
            dispatch(getStudentdataById(registerData._id));
        }
    }, [dispatch, registerData?._id]);

    useEffect(() => {
        if (studentfetch && studentfetch.exams) {
            setExams(studentfetch.exams); // Update the exams from the fetched data
        }
    }, [studentfetch]);

    const handleClickOpen = (exam) => {
        setSelectedExam(exam);
        setOpen(true);  // Open the confirmation dialog
    };

    const handleClose = (confirmed) => {
        setOpen(false); // Close the dialog
        if (confirmed && acknowledged && selectedExam) {
            // Pass exam and student data to the exam page
            navigate('/exam', { state: { exam: selectedExam, student: studentfetch } });
        }
    };

    const handleAcknowledgmentChange = (event) => {
        setAcknowledged(event.target.checked);
    };

    return (
        <>
            <Box sx={{ display: 'flex' }}>
                <Drawer
                    variant="permanent"
                    sx={{ width: drawerWidth }}
                />
                <Box component="main" sx={{ flexGrow: 1 }} className='mt-[-20px]'>
                    <Toolbar />
                    <div className="p-4">
                        <div className="text-xl font-bold mb-2">Student Dashboard | View Exam details</div>
                        <div className="text-xl text-zinc-600 mb-4">
                            <Link to="/" className="text-blue-600">🏠Home</Link> /
                            <Link to="/" className="text-blue-600">Question & Exam </Link> / <span>View Exam details</span>
                        </div>
                        <div className="bg-gradient-to-r from-red-500 to-yellow-500 text-white p-4 rounded-t-lg">View Exam details</div>
                        <div className="overflow-x-auto">
                            <table className="min-w-full bg-white border border-zinc-200">
                                <thead>
                                    <tr className="bg-zinc-800 text-white">
                                        <th className="py-2 px-4 border">Sl No.</th>
                                        <th className="py-2 px-4 border">Exam Name</th>
                                        <th className="py-2 px-4 border">Subject</th>
                                        <th className="py-2 px-4 border">Take Exam</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {exams.length > 0 ? exams.map((exam, index) => (
                                        <tr key={exam._id}>
                                            <td className="py-2 px-4 border">{index + 1}</td>
                                            <td className="py-2 px-4 border">{exam.title}</td>
                                            <td className="py-2 px-4 border">{exam.subject}</td>
                                            <td className="py-2 px-4 border">
                                                <Button onClick={() => handleClickOpen(exam)}>Take Exam</Button>
                                            </td>
                                        </tr>
                                    )) : (
                                        <tr>
                                            <td colSpan="4" className="py-4 px-4 text-center">No exams available</td>
                                        </tr>
                                    )}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </Box>
            </Box>

            {/* Confirmation Dialog */}
            <Dialog
                open={open}
                onClose={() => handleClose(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"Confirm Exam Start"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Before starting the exam, please carefully read and acknowledge the following instructions:
                    </DialogContentText>

                    <ul className="mt-4 space-y-2 list-disc ml-6">
                        <li>
                            <strong>Time Management:</strong> You must complete the exam within the allotted time. Once the time is up, your answers will be automatically submitted, and you will not be able to make any changes. Ensure that you keep track of the timer visible on your screen.
                        </li>
                        <li>
                            <strong>Cheating and Malpractice:</strong> Any form of cheating, including but not limited to switching windows, using external materials, or communicating with others, will lead to immediate disqualification from the exam. The exam software will monitor your activity, and any suspicious behavior will be reported.
                        </li>
                        <li>
                            <strong>Internet Connection:</strong> Ensure that your device is connected to a stable internet connection for the entire duration of the exam. If your connection is interrupted, you may attempt to log back in, but repeated disconnections may affect your ability to complete the exam.
                        </li>
                        <li>
                            <strong>Device Requirements:</strong> You must use a device with a working camera and microphone. These will be used to monitor the exam environment. Make sure that your camera and microphone are enabled and accessible to the exam software.
                        </li>
                        <li>
                            <strong>Prohibited Materials:</strong> The use of any external materials, such as calculators, textbooks, notes, or mobile phones (unless mobile is being used for the exam), is strictly prohibited. Rough sheets for calculations are allowed, but these should not be shared with others.
                        </li>
                        <li>
                            <strong>No Breaks Allowed:</strong> Once the exam begins, you are not allowed to take any breaks or leave your desk. Make sure that you are well-prepared and in a comfortable setting before starting the exam.
                        </li>
                        <li>
                            <strong>System Issues:</strong> In the event of a system crash or if your device freezes, do not panic. Simply close the browser and re-login using the provided credentials. You will be able to resume the exam from where you left off.
                        </li>
                        <li>
                            <strong>Final Submission:</strong> Before final submission, please review all your answers carefully. Once submitted, you will not be able to go back and make any changes.
                        </li>
                        <li>
                            <strong>Environment Requirements:</strong> You must ensure that you are in a quiet room with no disturbances or distractions. Any background noise or other people in the room may be flagged by the monitoring software.
                        </li>
                    </ul>

                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={acknowledged}
                                onChange={handleAcknowledgmentChange}
                                name="acknowledgment"
                                color="primary"
                            />
                        }
                        label="I have read and agree to the terms and conditions"
                    />
                </DialogContent>

                <DialogActions>
                    <Button onClick={() => handleClose(false)} color="secondary">
                        No
                    </Button>
                    <Button
                        onClick={() => handleClose(true)}
                        color="primary"
                        disabled={!acknowledged}
                        autoFocus
                    >
                        Yes
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default ViewExamdetails;
