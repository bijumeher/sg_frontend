import React, { useState } from 'react';
import { AppBar, Box, CssBaseline, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Divider, Drawer, IconButton, List, ListItem, ListItemButton, ListItemIcon, ListItemText, Toolbar, Typography } from '@mui/material';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';
import { Button, Menu, MenuItem } from '@mui/material';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import logoo from '../../assets/images/logo.png'
import { useNavigate } from 'react-router-dom';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { useDispatch } from 'react-redux';
import { CgProfile } from "react-icons/cg";
import LogoutIcon from '@mui/icons-material/Logout';
import styles from './ButtonStyles.module.css'
import { logout } from '../../actions/student/studentAction'
import CloseIcon from '@mui/icons-material/Close';


const drawerWidth = 270;
const Common = () => {
  const registerData = JSON.parse(sessionStorage.getItem('registerData'));
  // console.log(registerData);
  
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const studentName = sessionStorage.getItem('studentName');
  const uploadSignature = sessionStorage.getItem('uploadSignature');

  // Event handlers for the First menu
  const [openMenu1, setOpenMenu1] = useState(false);
  const [anchorElMenu1, setAnchorElMenu1] = useState(null);
  const handleClickMenu1 = (event) => {
    setOpenMenu1(true);
    setAnchorElMenu1(event.currentTarget);
  };

  const handleCloseMenu1 = () => {
    setOpenMenu1(false);
  };
  const [profileDialogOpen, setProfileDialogOpen] = useState(false);

  const handleProfileDialogOpen = () => {
    setProfileDialogOpen(true);
  };

  const handleProfileDialogClose = () => {
    setProfileDialogOpen(false);
  };

  const [openLogoutModal, setOpenLogoutModal] = useState(false)



  // Event handlers for the second menu
  const [openMenu2, setOpenMenu2] = useState(false);
  const [anchorElMenu2, setAnchorElMenu2] = useState(null);
  const handleClickMenu2 = (event) => {
    setOpenMenu2(true);
    setAnchorElMenu2(event.currentTarget);
  };

  const handleCloseMenu2 = () => {
    setOpenMenu2(false);
  };


  const open = Boolean(anchorEl);


  const navigate = useNavigate();
  const handleNavigate = () => {
    navigate('/student')
  }
  const handleNavigateAdd = () => {
    navigate('/managestudent')
    setOpenMenu2(false);
  }

  const ViewAttendancedetails = () => {
    navigate('/attendance')
    setOpenMenu1(false);
  }
  const ViewExamdetails = () => {
    navigate('/examdetails')
    setOpenMenu1(false);
  }
  const ViewExamResultsdetails = () => {
    navigate('/resultdetails')
    setOpenMenu1(false);
  }
  const ViewAdmitCard = () => {
    navigate('/admit')
    setOpenMenu1(false);
  }

  const StudyMateriall = () => {
    navigate('/studymaterialls')
    setOpenMenu2(false);
  }

  // const MasterData = () => {
  //   navigate('/masterdata')
  //   setOpenMenu2(false);
  // }
  window.addEventListener('popstate', function (event) {
    if (document.location.pathname === '/login') {
      sessionStorage.clear();
    }
  });

  const handleLogout = () => {
    dispatch(logout());
    navigate('/');
  };
  const handleOpenLogoutModal = () => setOpenLogoutModal(true);
  const handleCloseLogoutModal = () => setOpenLogoutModal(false);
  const handleConfirmLogout = () => {
    handleLogout();
    handleCloseLogoutModal();
  };
  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar position="fixed" sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}>
        <Toolbar class="bg-gradient-to-r from-red-500 via-pink-500 to-purple-500 p-2 flex justify-between items-center">
          <div class="flex items-center">
            <img src={logoo} alt="Admin logo" class="h-10 w-10 mr-2" />
          </div>
          <div class="text-white font-bold text-lg">Admin Adress</div>
          <div class="flex items-center">
            studentName: {registerData.studentName},
            <CgProfile size={35} />
            <IconButton onClick={handleProfileDialogOpen}>
              <MoreVertIcon className="text-gray-200 cursor-pointer ml-2" />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          [`& .MuiDrawer-paper`]: { width: drawerWidth, boxSizing: 'border-box' },
        }}
      >
        <Toolbar />
        <Box sx={{ overflow: 'auto' }}>
          <List>
            <Button
              className={styles.BtnOne}
              style={{ width: "100%" }}
              aria-haspopup="true"
              aria-expanded={open ? 'true' : undefined}
              variant="contained"
              onClick={handleNavigate}
            >
              My Dashboard
            </Button>

          </List>
          <List>
            <Button
              className={styles.BtnOne}
              style={{ width: '100%' }}
              id="basic-button"
              aria-controls={openMenu1 ? 'basic-menu-1' : undefined}
              aria-haspopup="true"
              aria-expanded={openMenu1 ? 'true' : undefined}
              variant="contained"
              onClick={handleClickMenu1}
              endIcon={<ArrowDropDownIcon />}
            >
              Question&Exam
            </Button>
            <Menu
              style={{ width: "100%" }}
              id="basic-menu-1"
              anchorEl={anchorElMenu1}
              open={openMenu1}
              onClose={handleCloseMenu1}
              MenuListProps={{
                'aria-labelledby': 'basic-button',
              }}
            >
              <MenuItem onClick={ViewAttendancedetails}>View Attendance Details</MenuItem>
              <MenuItem onClick={ViewAdmitCard} >View Admit Card </MenuItem>
              <MenuItem onClick={ViewExamdetails} >View Exam Details</MenuItem>
              <MenuItem onClick={ViewExamResultsdetails} >View Exam Result</MenuItem>
            </Menu>
          </List>
          <List>
            <Button
              className={styles.BtnOne}
              style={{ width: '100%' }}
              id="basic-button-one"
              aria-controls={openMenu2 ? 'basic-menu-2' : undefined}
              aria-haspopup="true"
              variant="contained"
              onClick={handleClickMenu2}
              endIcon={<ArrowDropDownIcon />}
            >
              Study Material
            </Button>
            <Menu
              id="basic-menu-2"
              anchorEl={anchorElMenu2}
              open={openMenu2}
              onClose={handleCloseMenu2}
              MenuListProps={{
                'aria-labelledby': 'basic-button-one',
              }}
            >
              <MenuItem onClick={StudyMateriall}>Study Material</MenuItem>


            </Menu>
          </List>
        </Box>
      </Drawer>
      <Dialog
        open={profileDialogOpen}
        onClose={handleProfileDialogClose}
        PaperProps={{
          sx: { position: 'absolute', left: '78%', bottom: '63%', width: '15%' }
        }}
      >
        <DialogTitle>
          <IconButton
            aria-label="close"
            onClick={handleProfileDialogClose}
            sx={{
              position: 'absolute',
              right: 0,
              top: 0,
              color: (theme) => theme.palette.grey[500],
            }}
          >
            <CloseIcon style={{ color: 'red' }} />
          </IconButton>
        </DialogTitle>
        <DialogContent sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
          <AccountCircleIcon sx={{ fontSize: 64, color: '#2196f3' }} />
          <Button onClick={handleOpenLogoutModal} sx={{
            mt: 2, backgroundColor: 'blue', '&:hover': {
              backgroundColor: 'black',
            }
          }} className=" text-white px-4 py-2 ">
            LOGOUT
          </Button>
        </DialogContent>
      </Dialog>
      <Dialog
        sx={{ position: 'absolute', left: '76%', bottom: '57%' }}
        open={openLogoutModal}
        onClose={handleCloseLogoutModal}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title" sx={{textAlign:'center'}}>{"Logout"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description" sx={{textAlign:'center', color:'black'}}>
            Are you sure you want to logout?
          </DialogContentText>
        </DialogContent>
        <DialogActions sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', marginRight:'8%'}}>
          <Button onClick={handleCloseLogoutModal}  sx={{ mt: 2, backgroundColor: 'blue', '&:hover': {
              backgroundColor: 'black',
            }}} className=" text-white px-4 py-2 ">
            Cancel
          </Button>
          <Button onClick={handleConfirmLogout} autoFocus sx={{ mt: 2, backgroundColor: 'blue', '&:hover': {
              backgroundColor: 'black',
            }}} className=" text-white px-4 py-2 ">
            Logout
          </Button>
        </DialogActions>
      </Dialog>
      <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
      </Box>
    </Box>
  )
}

export default Common