import React from 'react'
import ApiAlert from './components/alert/alert'

function AlertProvider({ children }) {
    return (
        <div>
            {children}
            <ApiAlert/>
        </div>
    )
}

export default AlertProvider
