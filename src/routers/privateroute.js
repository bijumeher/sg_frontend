import React from 'react'
import { Navigate, Outlet } from 'react-router-dom'

export default function PrivateRoute({ roles }) {
  const branchData = JSON.parse(sessionStorage.getItem('branchData'));
  const registerData = JSON.parse(sessionStorage.getItem('registerData'));

  if (
    (!registerData || !roles.includes(registerData.authType)) &&
    (!branchData || !roles.includes(branchData.authType))
  ) {
    return <Navigate to="/login" />
  }

  return <Outlet />
}
