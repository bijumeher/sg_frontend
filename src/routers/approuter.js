import React from 'react'
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Coursedetails from '../branch/pages/manageMaster/coursedetails.jsx';
import Sessiondetails from '../branch/pages/manageMaster/sessiondetails.jsx';
import Batchdetails from '../branch/pages/manageMaster/batchdetails.jsx';
// import Productrequest from '../branch/pages/manageMaster/productrequest.jsx';
import StudyMaterial from '../student/pages/studymaterial/studyMaterial.jsx';
import MyDashboardData from '../branch/pages/myDashboard/dashboard.jsx';
import Navbar from '../branch/common/navbar/navbar.jsx';
import { Box } from '@mui/material';
import Studentdetails from '../branch/pages/manageStudent/studentdetails.jsx';
import FeeCollection from '../branch/pages/managePayment/feeCollection.jsx';
import FeeCollectionReport from '../branch/pages/managePayment/feeCollectionReport.jsx';
import Admitcarddetails from '../branch/pages/questionNanswer/admitcarddetails.jsx';
import Viewstudntresult from '../branch/pages/questionNanswer/viewstudntresult.jsx';
import StudentAttaindance from '../branch/pages/manageStudent/studentAttaindance.jsx';
// import Studentrepoprts from '../branch/pages/allreeports/studentrepoprts.jsx';
import Studymateriall from '../branch/pages/studyMaterial/studymaterial.jsx';
import NotFound from '../components/notfound.js';
import PrivateRoute from './privateroute.js';
import Home from '../pages/home/home.jsx';
import Login from '../pages/auth/login/login.jsx';
import ContactForm from '../pages/auth/contact/contact.jsx';
import AboutUs from '../pages/aboutus/aboutus.jsx';
import ApplyLoan from '../components/Header/apply_loan.jsx';
import Header from '../components/Header/header.jsx';
import Footer from '../components/Footer/footer.jsx';
import Common from '../student/common/common.jsx';
import ViewAdmitCard from '../student/pages/questionandexam/viewAdmitCard.jsx';
import ViewExamResultsdetails from '../student/pages/questionandexam/viewExamResultsdetails.jsx';
import MyDashboard from '../student/pages/myDashboard/myDashboard.jsx';
import ForgotPassword from '../pages/auth/login/forgetpassword.jsx';
import ViewExamdetails from '../student/pages/questionandexam/ViewExamdetails.jsx'
import Payment from '../pages/auth/payment/payment.jsx';
import FeeCollectionData from '../branch/pages/managePayment/feedata.jsx';
import PaymentReceipt from '../pages/auth/paymentreceipt/paymentReceipt.jsx';
import Studentdetail from '../pages/auth/registration/next-page.jsx';
import Certificate from '../certificate.jsx';
import Paymentreceiptt from '../pages/auth/paymentreceipt/paymentreceiptt.jsx';
import BranchRegister from '../branch/pages/manageStudent/branchregister.jsx';
import UserRegister from '../pages/auth/registration/registration.jsx';
import Admitcard from '../pages/auth/admitCard/admitcard.jsx';
import Identity from '../pages/auth/identityCard/identity.jsx';
import Verifiedcertificate from '../pages/auth/verifiedCertificate/verifiedcertificate.jsx';
import OriginalCertificate from '../pages/auth/OriginalCertificate/OriginalCertificate.jsx';
import ExamPage from '../student/pages/questionandexam/exampage.jsx';
import ViewAttendancedetails from '../student/pages/questionandexam/viewAttaindance.jsx';


function AppRouter() {
  return (
    <Router>

      <Routes>
        {/* Public Routes */}
        <Route path='/' element={<><Home /><Header /><Footer /></>} />
        <Route path='/login' element={<><Login /><Header /><Footer /></>} />
        <Route path='/payment' element={<><Payment /><Header /><Footer /></>} />
        <Route path='/contact' element={<><ContactForm /><Header /><Footer /></>} />
        <Route path='/register' element={<><UserRegister /><Header /><Footer /></>} />
        <Route path='/about' element={<><AboutUs /><Header /><Footer /></>} />
        <Route path='/apply' element={<><ApplyLoan /><Header /><Footer /></>} />
        <Route path='/forgot-password' element={<><ForgotPassword /><Header /><Footer /></>} />
        <Route path='/certificate' element={<Certificate />} />
        <Route path='/ocertificate' element={<OriginalCertificate />} />
        <Route path='/paymentreceiptt' element={<><Paymentreceiptt /><Header /><Footer /></>} />
        <Route path='/admitcard' element={<><Admitcard /><Header /><Footer /></>} />
        <Route path='/identity' element={<><Identity/><Header /><Footer /></>} />
        <Route path='/verifiedc' element={<><Verifiedcertificate/><Header /><Footer /></>} />

        


        {/* Branch Routes */}
        <Route element={<PrivateRoute roles={['branch']} />}>
          <Route path='/branch' element={<Box><MyDashboardData /><Navbar /></Box>} />

          {/* Manage Master Routes */}
          <Route path='/sessiondetails' element={<Box><Sessiondetails /><Navbar /></Box>} />
          <Route path='/coursedetails' element={<Box><Coursedetails /><Navbar /></Box>} />
          {/* <Route path='/productrequest' element={<Box><Productrequest /><Navbar /></Box>} /> */}
          <Route path='/batchdetails' element={<Box><Batchdetails /><Navbar /></Box>} />
          <Route path='/next-page' element={<Box><Studentdetail /><Navbar /></Box>} />


          {/* Manage Student Routes */}
          <Route path='/StudentAttaindance' element={<Box><StudentAttaindance /><Navbar /></Box>} />
          <Route path='/Payment/:studentId' element={<Box><Payment /></Box>} />
          <Route path='/registrationdata' element={<Box><BranchRegister /><Navbar /></Box>} />
          <Route path='/studentdetails' element={<Box><Studentdetails /><Navbar /></Box>} />

          {/* Manage Payment Routes */}
          <Route path='/collection' element={<Box><FeeCollection /><Navbar /></Box>} />
          <Route path='/collectionreport' element={<Box><FeeCollectionReport /><Navbar /></Box>} />
          <Route path='/FeeCollectionData' element={<Box><FeeCollectionData /><Navbar /></Box>} />
          <Route path='/paymentreceipt' element={<PaymentReceipt />} />



          {/* Question & Exam Routes */}
          <Route path='/admitcard' element={<Box><Admitcarddetails /><Navbar /></Box>} />
          <Route path='/studentresult' element={<Box><Viewstudntresult /><Navbar /></Box>} />

          {/* Study Material Routes */}
          <Route path='/studymateriall' element={<Box><Studymateriall /><Navbar /></Box>} />

          {/* Student Reports Routes */}
          {/* <Route path='/studentreports' element={<Box><Studentrepoprts /><Navbar /></Box>} /> */}
        </Route>
        {/* student router start */}

        <Route element={<PrivateRoute roles={['student']} />}>

          <Route path='/student' element={<Box><MyDashboard /><Box><Common /></Box></Box>} />
          {/* question n exam router start */}
          <Route path='/attendance' element={<Box><ViewAttendancedetails /><Box><Common /></Box></Box>} />
          <Route path='/resultdetails' element={<Box><ViewExamResultsdetails /><Box><Common /></Box></Box>} />
          <Route path='/admit' element={<Box><ViewAdmitCard /><Box><Common /></Box></Box>} />
          <Route path='/examdetails' element={<Box><ViewExamdetails /><Box><Common /></Box></Box>} />
          <Route path="/exam" element={<ExamPage />} />
          {/* question n exam router end */}

          {/* study material router start */}
          <Route path='/studymaterialls' element={<Box><StudyMaterial /><Box><Common /></Box></Box>} />
          {/* study material router end */}
        </Route>
        {/* 404 Route */}
        <Route path="*" element={<NotFound />} />
      </Routes>
    </Router>
  );
}

export default AppRouter;
