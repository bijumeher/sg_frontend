import React, { useState } from 'react';
import { PDFDownloadLink, Document, Page, Text, View, StyleSheet } from '@react-pdf/renderer';

const styles = StyleSheet.create({
  page: {
    flexDirection: 'row',
    backgroundColor: '#ffffff',
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1,
  },
  header: {
    textAlign: 'center',
    marginBottom: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  content: {
    textAlign: 'center',
  },
  signature: {
    textAlign: 'right',
    marginTop: 20,
  },
});

const Certificate = () => {
  const [formData, setFormData] = useState({
    name: '',
    course: '',
    completionDate: ''
  });
  const [showCertificate, setShowCertificate] = useState(false);

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setShowCertificate(true);
  };

  const CertificateDocument = () => (
    <Document>
      <Page size="A4" style={styles.page}>
        <View style={styles.section}>
          <Text style={styles.title}>Certificate of Completion</Text>
          <Text>{formData.name} has successfully completed the course:</Text>
          <Text style={{ marginTop: 10 }}>{formData.course}</Text>
          <Text style={{ marginTop: 10 }}>Awarded on: {formData.completionDate}</Text>
          <View style={styles.signature}>
            <Text>Authorized Signature</Text>
            <Text>Institute Name</Text>
          </View>
        </View>
      </Page>
    </Document>
  );

  return (
    <div>
      <h2>Certificate Generator</h2>
      <form onSubmit={handleSubmit}>
        {/* Form inputs */}
        <button type="submit">Generate Certificate</button>
      </form>
      {showCertificate && (
        <div>
          <PDFDownloadLink document={<CertificateDocument />} fileName="certificate.pdf">
            {({ blob, url, loading, error }) =>
              loading ? 'Generating PDF...' : 'Download PDF'
            }
          </PDFDownloadLink>
        </div>
      )}
    </div>
  );
};

export default Certificate;
