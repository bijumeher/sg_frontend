import React from 'react'
import { useTypewriter, Cursor } from 'react-simple-typewriter';
import md from '../../assets/images/mdd.png'
import { HiArrowRight } from 'react-icons/hi'
import programming from '../../assets/images/programming.svg'
import franchies from '../../assets/images/francheis.svg'
import training from '../../assets/images/training.svg'
import courses from '../../assets/images/courses.svg'
import education from '../../assets/images/education.svg'
import graduation from '../../assets/images/graduation.svg'
import InstagramIcon from '@mui/icons-material/Instagram';
import FacebookIcon from '@mui/icons-material/Facebook';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import { Link } from 'react-router-dom';


const Aboutus = () => {
  const [typeEffectt] = useTypewriter({
    words: ["I am Shreenibas Ghadai"],
    loop: {},
    typeSpeed: 100,
    deleteSpeed: 40
  });
  return (
    <>
      {/* wecome start */}
      <div className="flex items-center justify-center bg-gradient-to-r from-[#40399b] to-[#4985ea] text-foreground">
        <div className="flex flex-col md:flex-row items-center max-w-4xl p-6">
          <div className="md:w-1/2 text-center md:text-left mt-28">
            <h1 className="text-2xl font-semibold mb-2 text-white">Hi,{typeEffectt}<Cursor /></h1>
            <h2 className="text-2xl text-white font-semibold mb-4 ">Welcome to <span className='text-yellow-100'>S. G. GROUP of MEDIA.</span> </h2>
            <p className="mb-6 text-gray-100" data-aos="fade-right">
              S. G. GROUP of MEDIA is a company incorporated under the companies Act 1956, Ministry of Corporate Affairs, Under the Govt. of India bearing the registration No.: UDYAM-OD-05-0011126 having its Head Office at Bhadrak,ODISHA. “S. G. GROUP of MEDIA” is an ISO 9001: 2015 Certified Company to promote IT awareness and to provide IT education through franchisees centers.
            </p>
            <button className='flex items-center justify-center px-6 py-3 my-2 text-white border-2 group rounded-md hover:bg-[#e311e8]' ><span data-aos="fade-right">View More...</span>
              <span className='duration-300 group-hover:rotate-90'> <HiArrowRight className='ml-3' /></span>
            </button>
            <div className='flex justify-between my-6 flex-cols md:w-[75% py-4'>
          <Link to="https://www.instagram.com/_can_u_call_me_sg_?utm_source=ig_web_button_share_sheet&igsh=ZDNlZDc0MzIxNw==" target="_blank">
            < InstagramIcon style={{ color: '#ff69b4' }}/>
          </Link>
          <Link to="https://www.facebook.com/profile.php?id=100007901175515&mibextid=ZbWKwL" target="_blank">
            < FacebookIcon />
          </Link>
          < LinkedInIcon />
          < WhatsAppIcon />
        </div>
          </div>
          <div className="md:w-1/2 mt-6 md:mt-0 ">
            <img src={md} data-aos="zoom-in-up" alt="Profile" className="drop-shadow-2xl rounded-full" />
          </div>
          
        </div>

      </div>
      {/* wecome end*/}
      {/* about start */}
      <div className='w-full bg-[#e4efff] text-black'>
        <div className='flex flex-col items-center justify-center w-full h-full'>
          <div className='max-w-[1000px]  grid grid-cols-2 gap-8 mt-20'>
            <div className='pb-8 pl-4 sm:text-right'>
              <p className='inline text-4xl font-bold border-b-4 border-[#e311e8]'>About</p>
            </div>
          </div>
          <div className='max-w-[1000px] w-full grid sm:grid-cols-2 gap-8 px-4'>
            <div className='text-4xl font-bold sm:text-right'>
              <p data-aos="fade-right" data-aos-duration="2000">Welcome to S. G. GROUP of MEDIA.</p>
            </div>
            <div data-aos="fade-left" data-aos-duration="2000">
              S. G. GROUP of MEDIA is an independent Computer Education Institute conducting computer-job oriented courses. This Institute was established to cater to the basic needs in computer education.
              It offers courses of six months, one year, one and a half year and short-term duration courses. Along-with his various other courses are also offered through our institutes. These courses mainly benefits School and College students. Office Going Executives, Housewives, Educated Unemployed Youth and those, interested in taking up computer as their career.
            </div>
          </div>
        </div>
        <div className='w-full mt-20  bg-[#e4efff] text-black'>
          <div className='flex flex-col items-center justify-center w-full h-full'>
            <div className='max-w-[1000px]  grid grid-cols-2 gap-8'>
              <div className='pb-8 pl-4 sm:text-right'>
              </div>
            </div>
            <div className='max-w-[1000px] w-full grid sm:grid-cols-2 gap-8 px-4'>
              <div data-aos="fade-right" data-aos-duration="2000">
                S. G. GROUP of MEDIA  is an independent Computer Education Institute conducting job oriented computer courses. This Institute was established to cater the basic needs in computer education.
              </div>
              <div className='text-4xl font-bold sm:text-right'>
                <img className='h-[270px]' data-aos="fade-left" data-aos-duration="2000" src={programming} alt="programmingg" />
              </div>

            </div>
          </div>
        </div>
        <div className='w-full  mt-20 bg-[#e4efff] text-black' >
          <div className='flex flex-col items-center justify-center w-full h-full'>
            <div className='max-w-[1000px]  grid grid-cols-2 gap-8'>
              <div className='pb-8 pl-4 sm:text-right'>
              </div>
            </div>
            <div className='max-w-[1000px] w-full grid sm:grid-cols-2 gap-8 px-4'>
              <div className='text-4xl font-bold sm:text-right'>
                <img className='h-[270px]' data-aos="fade-right" data-aos-duration="2000" src={graduation} alt="programmingg" />
              </div>
              <div data-aos="fade-left" data-aos-duration="2000">
                S. G. GROUP of MEDIA  Education also provides rewarding franchise opportunities to passionate business entrepreneurs for imparting value addition services for computer training and education. S. G. GROUP of MEDIA Education is an established brand in computer education and training with many successes & smiles.
              </div>
            </div>
          </div>
        </div>
        <div className='w-full  mt-20 bg-[#e4efff] text-black'>
          <div className='flex flex-col items-center justify-center w-full h-full'>
            <div className='max-w-[1000px]  grid grid-cols-2 gap-8'>
              <div className='pb-8 pl-4 sm:text-right'>
              </div>
            </div>
            <div className='max-w-[1000px] w-full grid sm:grid-cols-2 gap-8 px-4'>
              <div data-aos="fade-right" data-aos-duration="2000">
                S. G. GROUP of MEDIA  has taken up the activity to elevate business people to set up a foundation for giving training certificate courses to the students. Today we have got a very structured and wide network of authorized training centers. S. G. GROUP of MEDIA has proven itself as a major institution in the IT education and training sector by offering job-oriented short and long-term certification courses in Software and Professional Sectors. S. G. GROUP of MEDIA Top Management attempts to give new opportunities to the students as well as their centers.
              </div>
              <div className='text-4xl font-bold sm:text-right'>
                <img className='h-[270px]' data-aos="fade-left" data-aos-duration="2000" src={training} alt="programmingg" />
              </div>
            </div>
          </div>
        </div>
        <div className='w-full  mt-20 bg-[#e4efff] text-black'>
          <div className='flex flex-col items-center justify-center w-full h-full'>
            <div className='max-w-[1000px]  grid grid-cols-2 gap-8'>
              <div className='pb-8 pl-4 sm:text-right'>
              </div>
            </div>
            <div className='max-w-[1000px] w-full grid sm:grid-cols-2 gap-8 px-4'>
              <div className='text-4xl font-bold sm:text-right'>
                <img className='h-[270px]' data-aos="fade-right" data-aos-duration="2000" src={courses} alt="programmingg" />
              </div>
              <div data-aos="fade-left" data-aos-duration="2000"  >
                S. G. GROUP of MEDIA offers short term as well as regular courses related to IT. Along with this various other course are also offered through our institutes. These courses mainly benefits School and College students, Office Going Executives, Housewives, Educated Unemployed Youth and those interested in taking up IT as their career.
              </div>
            </div>
          </div>
        </div>
        <div className='w-full mt-20 bg-[#e4efff] text-black mb-20'>
          <div className='flex flex-col items-center justify-center w-full h-full'>
            <div className='max-w-[1000px]  grid grid-cols-2 gap-8'>
              <div className='pb-8 pl-4 sm:text-right'>
                <p className='inline text-4xl font-bold border-b-4 border-[#e311e8]'>Our Aim</p>
              </div>
            </div>
            <div className='max-w-[1000px] w-full grid sm:grid-cols-2 gap-8 px-4'>
              <div data-aos="fade-right" data-aos-duration="2000">
                Our aim is to provide basic and advanced computer education to everyone under the guidance of S. G. GROUP of MEDIA Education. Our courses are designed keeping in mind the duration and cost which is efficient and optimal.
              </div>
              <div className='text-4xl font-bold sm:text-right'>
                <img className='h-[270px]' data-aos="fade-left" data-aos-duration="2000" src={education} alt="programmingg" />
              </div>
            </div>
          </div>
        </div>
        {/* about end */}
        {/* available start */}
        <div className='p-8' data-aos="zoom-in" data-aos-duration="2000">
          <div className="bg-gradient-to-r from-[#EC63FF] to-[#E68AD2] text-white p-6 rounded-lg flex justify-between items-center">
            <div>
              <p className="text-sm font-semibold">NOW AVAILABLE</p>
              <h2 className="text-xl font-bold">Well, what are you waiting for?</h2>
              <p className="text-sm">Head over to your boards and start getting things done!</p>
            </div>
            <button className="bg-white text-blue-800 font-semibold py-2 px-4 rounded-lg shadow-md hover:scale-105 transition duration-300">Sign up</button>
          </div>
        </div>
        {/* available start */}
        
      </div>

    </>
  )
}

export default Aboutus