import React from 'react';


const inputClasses = "mt-1 block w-full px-3 py-2 border border-zinc-300 rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm";
const labelClasses = "block text-sm font-medium text-zinc-700";

const UserDetails = () => {
   
  
    return (
        <div className="max-w-lg mx-auto p-8 bg-white shadow-md py-16 mt-10">
            <h1 className="text-2xl font-bold text-center mb-6">User Details Form</h1>
            <form>
                <div className="mb-4">
                    <label htmlFor="first-name" className={labelClasses}>First Name:*</label>
                    <input type="text" id="first-name" name="first-name" required className={inputClasses} />
                </div>
                <div className="mb-4">
                    <label htmlFor="last-name" className={labelClasses}>Last Name:*</label>
                    <input type="text" id="last-name" name="last-name" required className={inputClasses} />
                </div>
                <div className="mb-4">
                    <label htmlFor="email" className={labelClasses}>Email:*</label>
                    <input type="email" id="email" name="email" required className={inputClasses} />
                </div>
                <div className="mb-4">
                    <label htmlFor="username" className={labelClasses}>Username:*</label>
                    <input type="text" id="username" name="username" required className={inputClasses} />
                </div>
                <fieldset className="mb-6">
                    <legend className={labelClasses}>Gender</legend>
                    <div className="mt-2 space-y-4">
                        <div className="flex items-center">
                            <input id="male" name="gender" type="radio" value="male" className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-zinc-300" />
                            <label htmlFor="male" className="ml-3 block text-sm font-medium text-zinc-700">Male</label>
                        </div>
                        <div className="flex items-center">
                            <input id="female" name="gender" type="radio" value="female" className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-zinc-300" />
                            <label htmlFor="female" className="ml-3 block text-sm font-medium text-zinc-700">Female</label>
                        </div>
                    </div>
                </fieldset>
                <button type="submit" className="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">SEND</button>
            </form>
        </div>
    );
};

export default UserDetails;

