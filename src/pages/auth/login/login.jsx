import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { toast, Toaster } from "react-hot-toast";
import { useNavigate } from 'react-router-dom';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import img from '../../../assets/images/login.jpg';
import { login } from '../../../actions/branch/branchAction';
import { Studentlogin } from '../../../actions/student/studentAction';

const Login = () => {
  const [authType, setAuthType] = useState('');
  const [email, setEmail] = useState('');``
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [showForgotPassword, setShowForgotPassword] = useState(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (authType === 'branch') {
      dispatch(login(email, password, authType, navigate))
    } else if (authType === 'student') {
      dispatch(Studentlogin(email, password, authType, navigate)).then(result => {
        if (!result) {
          setShowForgotPassword(true); 
        }
      }).catch(error => {
        // Optionally handle errors here
        toast.error("Login failed, please try again.");
      });
    } else {
      toast.error("Select auth type");
    }
  };

  const handleForgotPasswordClick = () => {
    navigate('/forgot-password');
  };

  return (
    <div className='flex items-center justify-center min-h-screen bg-gray-50 mt-16'>
      <div className='flex max-w-3xl p-2 bg-gray-100 shadow-lg rounded-2xl'>
        <Toaster toastOptions={{ duration: 4000 }} />
        <div className='px-16 sm:w-1/2'>
          <h2 className='text-2xl font-bold text-[#002D74]'>Log In</h2>
          <p className='mt-4 text-sm text-[#002D74]'>If you're already a member, easily log in</p>
          <form className='flex flex-col gap-4' onSubmit={handleSubmit}>
            <select
              className='p-2 border rounded-xl'
              name='authType'
              value={authType}
              onChange={(e) => setAuthType(e.target.value)}
            >
              <option disabled value="">
                Select Role
              </option>
              <option value='branch'>Branch</option>
              <option value='student'>Student</option>
            </select>
            <input
              className='p-2 border rounded-xl'
              type='text'
              name='email'
              placeholder='Email'
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <div className='relative'>
              <input
                className='w-full p-2 border rounded-xl'
                type={showPassword ? 'text' : 'password'}
                name='password'
                placeholder='Password'
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <button
                type='button'
                className='absolute right-2 top-2'
                onClick={() => setShowPassword(!showPassword)}
              >
                {showPassword ? <VisibilityOff /> : <Visibility />}
              </button>
            </div>
            <button className='py-2 text-white bg-[#002D74] rounded-xl' type='submit'>
              Login
            </button>
            {showForgotPassword && (
              <div className='mt-4'>
                <button
                  type='button'
                  onClick={handleForgotPasswordClick}
                  className='text-blue-500 hover:underline'
                >
                  Forgot Password?
                </button>
              </div>
            )}
          </form>
          <div className='grid items-center grid-cols-3 mt-10 text-gray-400'>
            <hr className='border-gray-600' />
            <p className='text-center'>OR</p>
            <hr className='border-gray-600' />
          </div>
        </div>
        <div className='w-1/2 p-2'>
          <img className='hidden bg-cyan-100 rounded-2xl sm:block' src={img} alt='Login' />
        </div>
      </div>
    </div>
  );
};

export default Login;
