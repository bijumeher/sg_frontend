import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { registerUserName, forgotPassword } from '../../../actions/student/studentAction';
import { toast, Toaster } from 'react-hot-toast';

const ForgotPassword = () => {
  const [email, setEmail] = useState('');
  const [step, setStep] = useState(1);
  const [name, setName] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleEmailSubmit = async (e) => {
    e.preventDefault();
    const result = await dispatch(registerUserName(email));
    if (result.success) {
      setName(result.name);
      setStep(2);
    } else {
      toast.error(result.message);
    }
  };

  const handlePasswordSubmit = async (e) => {
    e.preventDefault();
    dispatch(forgotPassword(email, newPassword, navigate));
  };

  return (
    <div className='flex items-center justify-center min-h-screen bg-gray-50'>
      <Toaster toastOptions={{ duration: 4000 }} />
      <div className='flex flex-col max-w-md p-8 bg-gray-100 shadow-lg rounded-2xl'>
        {step === 1 ? (
          <>
            <h2 className='text-2xl font-bold text-[#002D74]'>Forgot Password</h2>
            <p className='mt-4 text-sm text-[#002D74]'>Enter your email to reset your password</p>
            <form className='flex flex-col gap-4' onSubmit={handleEmailSubmit}>
              <input
                className='p-2 border rounded-xl'
                type='email'
                name='email'
                placeholder='Email'
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <button className='py-2 text-white bg-[#002D74] rounded-xl' type='submit'>
                Verify Email
              </button>
            </form>
          </>
        ) : (
          <>
            <h2 className='text-2xl font-bold text-[#002D74]'>Reset Password</h2>
            <p className='mt-4 text-sm text-[#002D74]'>Hello, {name}. Set your new password below.</p>
            <form className='flex flex-col gap-4' onSubmit={handlePasswordSubmit}>
              <input
                className='p-2 border rounded-xl'
                type='password'
                name='newPassword'
                placeholder='New Password'
                value={newPassword}
                onChange={(e) => setNewPassword(e.target.value)}
              />
              <button className='py-2 text-white bg-[#002D74] rounded-xl' type='submit'>
                Reset Password
              </button>
            </form>
          </>
        )}
      </div>
    </div>
  );
};

export default ForgotPassword;
