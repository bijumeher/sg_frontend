import React, { useState } from 'react';
import logo from '../../../assets/images/sg3.png'
import { useDispatch } from 'react-redux';
import { contactRegister, getAllContactData } from '../../../actions/contact/contact';

const inputClasses = "border p-2 rounded w-full";
const selectClasses = "border p-2 rounded w-full";
const buttonClasses = " text- bg-black dark:bg-white text-white dark:text-black p-2 rounded w-full";

const ContactForm = () => {
    const dispatch = useDispatch();
    const [contactData, setContactData] = useState({
        firstname: "",
        lastname: "",
        mobile: "",
        email: "",
        type: "",
        source: ""
    });

    const handelChangeInput = (event) => {
        const { name, value } = event.target;
        setContactData({
            ...contactData,
            [name]: value,
        });
    };

    const handelSaveContactData = async (event) => {
        event.preventDefault();
        dispatch(contactRegister({ ...contactData }))
        setContactData({
            firstname: "",
            lastname: "",
            mobile: "",
            email: "",
            type: "",
            source: ""
        });
    };
    console.log(contactData);
    return (
        <div className="bg-white dark:bg-zinc-800 p-8 mt-10 py-16 ">
            <div className="max-w-4xl mx-auto">
                <header className="mb-6">
                    <div className="flex justify-between items-center mb-4">
                        <img src={logo} alt="Company Logo" className='h-[50px] w-[100px]' />
                        <span className="text-sm text-zinc-600 dark:text-zinc-400">SG-GROUP</span>
                    </div>
                    <h1 className="text-3xl font-bold text-zinc-900 dark:text-white">How can we help you?</h1>
                    <p className="text-zinc-600 dark:text-zinc-400 mt-2">
                        Thank you for your interest in SG-GROUP. Please use this form to contact us. We will get back to you as soon as we can.
                    </p>
                </header>
                <form
                    onSubmit={handelSaveContactData}

                >
                    <div className="grid grid-cols-1 md:grid-cols-2 gap-4 mb-4">
                        <input
                            name="firstname"
                            value={contactData?.firstname}
                            onChange={handelChangeInput}
                            type="text"
                            placeholder="First Name*"
                            className={inputClasses} />
                        <input
                            name="lastname"
                            value={contactData?.lastname}
                            onChange={handelChangeInput}
                            type="text"
                            placeholder="Last Name*"
                            className={inputClasses} />
                    </div>
                    <div className="grid grid-cols-1 md:grid-cols-2 gap-4 mb-4">
                        <input
                            name="mobile"
                            value={contactData?.mobile}
                            onChange={handelChangeInput}
                            type="number" placeholder="Mobile Number" className={inputClasses} />
                        <input
                            name="email"
                            value={contactData?.email}
                            onChange={handelChangeInput}
                            type="email" placeholder="Email*" className={inputClasses} />
                    </div>
                    <div className="grid grid-cols-1 md:grid-cols-2 gap-4 mb-4">
                        <select required
                            name="type"
                            value={contactData?.type}
                            onChange={handelChangeInput}
                            className={selectClasses}>
                            <option disabled >I'm interested in*</option>
                            <option value="loan" >Loan</option>
                            <option value="education" >Education</option>
                        </select>
                        <select required
                            name="source"
                            value={contactData?.source}
                            onChange={handelChangeInput}
                            className={selectClasses}>
                            <option disabled >Where did you hear about us?</option>
                            <option value="google" >Google</option>
                            <option value="instagram" >Instagram Add</option>
                        </select>
                    </div>
                    <button type="submit" className={buttonClasses}>
                        Submit
                    </button>
                </form>
            </div>
        </div>
    );
};

export default ContactForm;

