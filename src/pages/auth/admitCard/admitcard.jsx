import React from 'react'
import adlogo from '../../../assets/images/adlogo.png'
import sign from '../../../assets/images/siggn.png'


const Admitcard = () => {
    return (
        <>
            <div className='p-8'>
                <div className="w-[700px] mx-auto p-6 bg-white dark:bg-card rounded-lg shadow-lg mt-20">
                    <div class="border-2 border-black py-4 text-center">
                        <img aria-hidden="true" alt="Institute Logo" src={adlogo} class="mx-auto mb-2 w-[100px]" />
                        <h4 class="text-xl font-bold ">Sharma Institute of Technology (SIT) | An ISO 9001:2015 Certified</h4>
                        <p class="text-muted-foreground">A Unit of Sharma Education India Pvt. Ltd.</p>
                    </div>
                    <h1 class="text-2xl font-bold text-center mt-4">Admit Card</h1>
                    <div class="flex justify-between mt-4">
                        <div class="flex-1">
                            <p class="text-muted">Name: <span class="font-semibold text-primary-foreground">Subrat Das</span></p>
                            <p class="text-muted">Roll NO: <span class="font-semibold text-primary-foreground">SIT/004</span></p>
                            <p class="text-muted">TC: <span class="font-semibold text-primary-foreground">Tapaswini & Jagyasen Institute Of Technology</span></p>
                            <p class="text-muted">Course: <span class="font-semibold text-primary-foreground">PGDCA/TALLY</span></p>
                            <p class="text-muted">Exam: <span class="font-semibold text-primary-foreground">PGDCA</span></p>
                        </div>
                        <div class="ml-4">
                            <img aria-hidden="true" alt="Subrat Das" src="https://placehold.co/100x100?text=Image" class=" border border-border" />
                            <p class="text-center text-muted-foreground mt-2">Subrat Das</p>
                        </div>
                    </div>
                    <div>
                        <table className="min-w-full border-collapse border border-zinc-300">
                            <thead>
                                <tr className="bg-zinc-100 dark:bg-zinc-800">
                                    <th className="border border-zinc-300 p-2 text-left text-white ">Appearing Subjects</th>
                                    <th className="border border-zinc-300 p-2 text-left text-white">Date Of Exam</th>
                                    <th className="border border-zinc-300 p-2 text-left text-white">Time</th>
                                    <th className="border border-zinc-300 p-2 text-left text-white">Category</th>
                                    <th className="border border-zinc-300 p-2 text-left text-white">Marks</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className="border border-zinc-300 p-2">FUNDAMENTAL OF COMPUTER</td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                </tr>
                                <tr>
                                    <td className="border border-zinc-300 p-2">WINDOWS</td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                </tr>
                                <tr>
                                    <td className="border border-zinc-300 p-2">WORD, EXCEL, PPT</td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                </tr>
                                <tr>
                                    <td className="border border-zinc-300 p-2">INTERNET, PRINTING, SCANNING</td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                </tr>
                                <tr>
                                    <td className="border border-zinc-300 p-2">TALLY</td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                </tr>
                                <tr>
                                    <td className="border border-zinc-300 p-2">ADOBE PHOTOSHOP</td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                </tr>
                                <tr>
                                    <td className="border border-zinc-300 p-2">ADOBE PAGEMAKER</td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                </tr>
                                <tr>
                                    <td className="border border-zinc-300 p-2">ADOBE CORELDRAW</td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                </tr>
                                <tr>
                                    <td className="border border-zinc-300 p-2">C PROGRAMMING</td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                </tr>
                                <tr>
                                    <td className="border border-zinc-300 p-2">PROJECT</td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                    <td className="border border-zinc-300 p-2"></td>
                                </tr>
                            </tbody>
                        </table>
                        <div className="flex justify-between items-center p-4 ">
                            <div className="text-left">
                                <p className="text-black">Signature of Training Centre Head</p>
                            </div>
                            <div className="text-right">
                                <img className='w-20' src={sign} alt="" />
                                <p className="font-bold">Controller of Examinations</p>
                                <p>Sharma Institute of Technology (SIT)</p>
                                <p>ODISHA (INDIA)</p>
                            </div>
                        </div>
                    </div>
                    <div className="p-4">
                        <h2 className="text-xl font-bold mb-4">N.B.</h2>
                        <ol className="list-decimal list-inside space-y-2">
                            <li>Refer to your Admit Card for Day and Time of the Test. The log-in for candidates will start five minutes before the commencement of the exam. Please log-in within time.</li>
                            <li>Candidates can give the test on Laptop and Mobile. Laptop users can give the exam on Chrome and Mozilla browser and Mobile users can access the link as provided student panel.</li>
                            <li>Camera and Microphone enabled devices are necessary for this online examination. Candidates must ensure allowing the Camera and Mic access to the software.</li>
                            <li>
                                Make sure that you have an internet connection for the examination (Uninterrupted internet for the entire duration of the examination) and your laptop/mobile does not have any internet
                                issues at the time of login and during examination.
                            </li>
                            <li>Please make sure that your laptop/mobile is fully charged before appearing in the examination as a measure against unexpected power outage.</li>
                            <li>You are not allowed to leave for any break during examination.</li>
                            <li>If candidate’s system hangs or face problems in the internet connectivity. Don’t worry, kindly close the browser of the exam and relog-in examination from where he/she had left.</li>
                            <li>
                                The candidates can use sheets and pencils for rough work while taking the examination. Use of calculators, papers, log tables, dictionaries or any other printed/online reference material
                                during the examinations is STRICTLY PROHIBITED. Use of any such devices or resources will be detected by the camera and may lead to cancellation of examination.
                            </li>
                        </ol>
                    </div>

                </div>
            </div>

        </>
    )
}

export default Admitcard