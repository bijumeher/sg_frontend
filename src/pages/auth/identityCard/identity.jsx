import React from 'react'
import { CgProfile } from "react-icons/cg";
import sign from '../../../assets/images/siggn.png'
import logoadd from '../../../assets/images/adlogo.png'
import { PDFDownloadLink, Document, Page, Text, View, Font, StyleSheet, Image, PDFViewer } from '@react-pdf/renderer';
import { Button } from '@mui/material';
const width = 54.00 * 2.83465; // 242.6 points
const height = 85.60 * 2.83465; // 153.1 points
const Identity = () => {
  const IdentityCardDocument = () => (
    <Document >
      <Page size={{ width, height }} style={styles.page}  // Replace with your image path
      >
        
        <Image
          style={styles.backgroundImage}
          src={require('../../../assets/image2/Blue Bold Student ID Card.png')} // Replace with your image path
        />
        <View style={{flexDirection:'column', justifyContent:'center',textAlign:'center',alignItems:'center',fontSize:'5px'}}>
          <Image style={{width:'40px',marginTop:'2px'}}src={require('../../../assets/image2/Untitled-1.png')}/>
          <Text style={{color:'white',fontSize:'5px',marginTop:'1px',fontWeight:'bold'}}>TAPASWINI & JAGGYASENI INSTITUTE OF TECHNOLOGY</Text>
          <Text style={{fontSize:'7px',color:'white'}}>Mobile No:9556909848</Text>
          <Text style={{color:'white'}}>Jagannathpur Bhadark,Near Bhadrak Autonomous College,Odisha-756100</Text>
        </View>
        <View style={{
          borderWidth: 3, // Use borderWidth instead of border for React Native
          borderColor: '#76b5c5',
          height: 60, // Ensure height and width are the same for a perfect circle
          width: 60,
          borderRadius: 40, // Half of the height and width to make it circular
          backgroundColor: 'white',
          marginTop: 2,
          marginLeft: '46px'
        }}>
          
        </View>
        <Text style={styles.Namecontent}>Saroj Kumar pradhan</Text>
        <View style={styles.contactbox}>
          <View>
            <Text style={styles.content}>Course Name</Text>
            <Text style={styles.content}>Roll No</Text>
            <Text style={styles.content}>Date of Birth</Text>
            <Text style={styles.content}>Contact No</Text>
            <Text style={styles.content}>Address</Text>
          </View>
          <View>
            <Text style={styles.content}>:</Text>
            <Text style={styles.content}>:</Text>
            <Text style={styles.content}>:</Text>
            <Text style={styles.content}>:</Text>
            <Text style={styles.content}>:</Text>
          </View>
        </View>
      </Page>
    </Document>
  );

  const styles = StyleSheet.create({
    backgroundImage: {
      position: 'absolute',
      left: 0,
      top: 0,
      right: 0,
      bottom: 0,

    },
    page: {
      flexDirection: 'column',
      // padding: 40,
      // paddingRight: '60px',
      // paddingLeft: '60px'
    },
    content: {
    padding:'1px',
    marginLeft:'5px'
    },
    contactbox:{
      flexDirection: 'row',
      fontSize: '8px',
      marginTop:'9px'
    },
    Namecontent:{
      fontSize:'10px',
      fontWeight:'800',
      textAlign:'center'
    }
  });
  return (
    <>
      <div className='mt-20 p-16'>
        <div className="max-w-sm mx-auto  text-card-foreground shadow-lg rounded-t-full overflow-hidden">
          <div className="bg-gradient-to-r from-[#146b76] to-[#05d3d5] text-white p-4 text-center">
            <div className='mt-10'>
              <h2 className="text-lg font-bold text-yellow-300">TAPASWINI & JAGGYASENI</h2>
              <h3 className="text-md text-yellow-300">INSTITUTE OF TECHNOLOGY</h3>
              <p className="text-sm">www.tjinstituteoftechnology.in</p>
            </div>
          </div>
          <div className='flex justify-center items-center mt-2' >
            <CgProfile size={60} />
          </div>
          <div >
            <h4 className="text-lg font-semibold text-center">DUSMANT MEHER</h4>
            <p className="text-sm ml-4">Course Name: PG-DCA</p>
            <p className="text-sm  ml-4">Roll No: 006/2022</p>
            <p className="text-sm  ml-4">Date of Birth: 24/01/2001</p>
            <p className="text-sm  ml-4">Contact No: 7205256479</p>
            <p className="text-sm  ml-4">Address: At/Po: Baladhiamal Dist: Kalahandi,Ps: Junagarh, Block: Junagarh,Pin: 766023</p>

          </div>
          <div className="bg-muted text-muted-foreground p-4 flex gap-16">
            <div>
              <img className="h-[100px] w-[200px]" src={logoadd} alt="logo" />
            </div>
            <div>
              <img className=' w-20' src={sign} alt="sign" />
              <p className="text-sm ">Authorised Signature</p>
              <p>Authorized Training Center of SIT</p>
              <p>(A Unit Of Sharma Education PVT.ltd)</p>
            </div>
          </div>
        </div>
      </div>
      <div className='text-center  mt-8'>
        <PDFDownloadLink document={<IdentityCardDocument />} fileName="Identity Card.pdf">
          {({ blob, url, loading, error }) =>
            loading ? 'Generating PDF...' : <Button variant="contained" color="primary">Download Payment Slip</Button>
          }
        </PDFDownloadLink>
        <PDFViewer width="100%" height="600">
          <IdentityCardDocument />
        </PDFViewer>
      </div>
    </>
  )
}

export default Identity