import { Box, Drawer, Toolbar, Button } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import toast, { Toaster } from "react-hot-toast";


import { getDataBrancWise } from '../../../actions/student/studentAction';

const drawerWidth = 240;

const Studentdetail = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();
  const [searchQuery, setSearchQuery] = useState('');
  const [isSearchPerformed, setIsSearchPerformed] = useState(false);
  const studentfetch = useSelector(state => state.student.studentData);
  const branch = JSON.parse(sessionStorage.getItem('branchData'));
  const branchName = branch.branchName;
  const obj = { branchName: branchName };

  const allstudent = useSelector((state) => state.student.paymentData);
  console.log(allstudent);

  const handleSearch = (e) => {
    setSearchQuery(e.target.value);
  };

  const handleSearchClick = () => {
    setIsSearchPerformed(true);
  };

  const handleNavigateToMasterdata = (studentId) => {
    navigate(`/Payment/${studentId}`);
  };

  useEffect(() => {
    dispatch(getDataBrancWise(obj));
    const pathname = location.pathname;

    if (pathname === '/next-page') {
      toast.success('Successfully created');
    }
  }, [dispatch,location.pathname]);

  return (
    <Box sx={{ display: 'flex' }}>
      <Drawer
        variant="permanent"
        sx={{
          width: drawerWidth,
        }}
      >
        {/* Add Drawer content here */}
      </Drawer>

      <Box component="main" sx={{ flexGrow: 1 }} className='mt-[-22px]'>
        <Toolbar />
        <div className="p-4">
          <Toaster toastOptions={{ duration: 4000 }} />
          <div className="mt-4 flex items-center">
            <input
              type="text"
              placeholder="Type Candidate Name or Mobile Number"
              className="border p-2 flex-grow rounded-l"
              value={searchQuery}
              onChange={handleSearch}
            />
            <button className="bg-blue-500 text-white p-2 rounded-r" onClick={handleSearchClick}>
              Search
            </button>
          </div>

          {isSearchPerformed && (
            <div className="mt-4">
              <table className="min-w-full border-collapse border border-zinc-300">
                <thead className="bg-blue-200">
                  <tr>
                    <th className="border border-zinc-300 p-2">Sl No</th>
                    <th className="border border-zinc-300 p-2">Student Name</th>
                    <th className="border border-zinc-300 p-2">Father Name</th>
                    <th className="border border-zinc-300 p-2">Gender</th>
                    <th className="border border-zinc-300 p-2">Caste</th>
                    <th className="border border-zinc-300 p-2">Email ID</th>
                    <th className="border border-zinc-300 p-2">Adhar No</th>
                    <th className="border border-zinc-300 p-2">Course Name</th>
                    <th className="border border-zinc-300 p-2">Mobile No</th>
                    <th className="border border-zinc-300 p-2">Reg ID</th>
                    <th className="border border-zinc-300 p-2">Date of Regd</th>
                    <th className="border border-zinc-300 p-2">Branch Name</th>
                    <th className="border border-zinc-300 p-2">Batch Name</th>
                    <th className="border border-zinc-300 p-2">Payment</th>
                  </tr>
                </thead>
                <tbody>
                  {studentfetch?.filter(student => {
                    const searchString = searchQuery.toLowerCase();
                    return (
                      student.studentName.toLowerCase().includes(searchString) ||
                      (student.mobNumber && typeof student.mobNumber === 'string' && student.mobNumber.toLowerCase().includes(searchString))
                    );
                  }).map((student, index) => (
                    <tr key={student._id} className="text-sm text-gray-700">
                      <td className="border border-zinc-300 p-2 text-center">{index + 1}</td>
                      <td className="border border-zinc-300 p-2">{student.studentName}</td>
                      <td className="border border-zinc-300 p-2">{student.fatherName}</td>
                      <td className="border border-zinc-300 p-2">{student.gender}</td>
                      <td className="border border-zinc-300 p-2">{student.caste}</td>
                      <td className="border border-zinc-300 p-2">{student.email}</td>
                      <td className="border border-zinc-300 p-2">{student.aadharNumber}</td>
                      <td className="border border-zinc-300 p-2">{student.courseName}</td>
                      <td className="border border-zinc-300 p-2">{student.mobNumber}</td>
                      <td className="border border-zinc-300 p-2">{student.registrationId}</td>
                      <td className="border border-zinc-300 p-2">{student.dateOfRegister}</td>
                      <td className="border border-zinc-300 p-2">{student.branchName}</td>
                      <td className="border border-zinc-300 p-2">{student.batchName}</td>
                      <td className="border border-zinc-300 p-2">
                        <Button
                          className='w-[100px] flex gap-2 hover:scale-105 duration-500' variant="contained"
                          onClick={() => handleNavigateToMasterdata(student._id)}
                        >
                          ADD
                        </Button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          )}
        </div>
      </Box>
    </Box>
  );
};

export default Studentdetail;
