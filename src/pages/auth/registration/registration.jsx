import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Box, Drawer, FormControl, IconButton, InputAdornment, InputLabel, OutlinedInput, Toolbar } from '@mui/material';
import { getallBranchData } from '../../../actions/branch/branchAction';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import Visibility from '@mui/icons-material/Visibility';
import { studentRegister } from '../../../actions/studentregister/studentRegisterAction';
import { useNavigate } from 'react-router-dom';


const inputClasses = " w-full h-40px border-zinc-300 rounded p-2 border rounded-lg";
const selectClasses = "w-full border-zinc-300 rounded p-2  border rounded-lg";
const labelClasses = " mb-1 block";
const drawerWidth = 240;
const errorClass = "border-red-500";
const MAX_FILE_SIZE = 50 * 1024;



const UserRegister = () => {
    const branchData = JSON.parse(sessionStorage.getItem('branchData'));
    const dispatch = useDispatch()
    const navigate = useNavigate();
    const branchesdata = useSelector(state => state.masterbranc.allbranch);



    console.log(branchesdata);


    useEffect(() => {
        dispatch(getallBranchData());
    }, [dispatch]);

    const [registerData, setRegisterData] = useState({
        studentName: '',
        fatherName: '',
        motherName: '',
        dateOfRegister: '',
        email: '',
        password: '',
        mobNumber: '',
        dateOfBirth: '',
        religion: '',
        motherTongue: '',
        gender: '',
        caste: '',
        presentAddress: '',
        permanentAddress: '',
        aadharNumber: '',
        uploadPhoto: '',
        uploadSignature: '',
        branchName: '',
        batchName: '',
        courseName: '',
        courseFees: '',
        courseDuration: '',
    });
    // console.log(registerData);

    const [errors, setErrors] = useState({});
    useEffect(() => {
        const currentDate = new Date().toISOString().slice(0, 10);
        setRegisterData(prevState => ({
            ...prevState,
            dateOfRegister: currentDate,
        }));
    }, []);




    const [image, setImage] = useState(null);
    const [userImage, setUserImage] = useState(null);
    // password
    const [showPassword, setShowPassword] = React.useState(false);

    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const imagebasse64 = (file) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        const data = new Promise((resolve, reject) => {
            reader.onload = () => resolve(reader.result);
            reader.onerror = (err) => reject(err);
        });
        return data;
    };


    const handleFileChange = (event) => {
        const file = event.target.files[0];
        if (file.size > MAX_FILE_SIZE) {
            alert('File size exceeds 50KB. Please upload a smaller file.');
            event.target.value = null;
            return;
        }
        const reader = new FileReader();

        reader.onloadend = async () => {
            setImage(reader.result);
            const image = await imagebasse64(file);
            setUserImage(image)
        };

        if (file) {
            reader.readAsDataURL(file);
        }
    };
    const [signetur, setSignetur] = useState(null);
    const [signatureImage, setSignatureImage] = useState(null);

    const FileChange = async (event) => {
        const files = event.target.files[0];
        if (files.size > MAX_FILE_SIZE) {
            alert('File size exceeds 50KB. Please upload a smaller file.');
            event.target.value = null;
            return;
        }
        const reader = new FileReader();

        reader.onloadend = async () => {
            setSignetur(reader.result);
            const image = await imagebasse64(files);
            setSignatureImage(image)
        };

        if (files) {
            reader.readAsDataURL(files);
        }
    };


    const validateFields = () => {
        const requiredFields = ['studentName', 'fatherName', 'motherName', 'dateOfRegister', 'email', 'password', 'mobNumber', 'dateOfBirth', 'religion', 'motherTongue', 'gender', 'caste', 'presentAddress', 'permanentAddress', 'aadharNumber'];
        const newErrors = {};

        // Check each required field
        requiredFields.forEach(field => {
            if (!registerData[field]) {
                newErrors[field] = true;
            } else {
                newErrors[field] = false;
            }
        });
        if (registerData.aadharNumber && !/^\d{12}$/.test(registerData.aadharNumber)) {
            newErrors.aadharNumber = true;
        }


        setErrors(newErrors);
        return Object.values(newErrors).every(error => !error);
    };


    const handleChange = (e) => {
        const { name, value } = e.target;
        if (name === 'aadharNumber' && value.length > 12) {
            return;
        }
        setRegisterData({
            ...registerData,
            [name]: value,
        });

        setErrors({
            ...errors,
            [name]: false,
        });
        if (name === 'courseName') {
            let selectedCourse = null;
            branchesdata.forEach(branch => {
                branch.courses.forEach(course => {
                    if (course.courseName === value) {
                        selectedCourse = course;
                    }
                });
            });
            // console.log(selectedCourse);
            setRegisterData(prevData => ({
                ...prevData,
                courseFees: selectedCourse ? selectedCourse.courseFees : '',
                courseDuration: selectedCourse ? selectedCourse.courseDuration : '',

            }));
            if (name === 'branchName') {
                const selectedBranch = branchesdata.find(branch => branch.branchName === value);
                if (selectedBranch) {
                    // Update batches and courses in registerData
                    setRegisterData(prevData => ({
                        ...prevData,
                        batchName: '', // Clear batch selection
                        courseName: '', // Clear course selection
                    }));
                }
            }
        }

    };
    const handleregisterData = async () => {

        if (!validateFields()) {
            alert('Please fill in all required fields');
            return;
        }


        if (!image || !signetur) {
            alert('Please upload both images: image and signature');
            return;
        }
        const studentData = {
            ...registerData,
            uploadSignature: signatureImage,
            uploadPhoto: userImage,

        };



        await dispatch(studentRegister(studentData, navigate)).then(() => {
            setRegisterData({
                studentName: '',
                fatherName: '',
                motherName: '',
                dateOfRegister: '',
                email: '',
                password: '',
                mobNumber: '',
                dateOfBirth: '',
                religion: '',
                motherTongue: '',
                gender: '',
                caste: '',
                presentAddress: '',
                permanentAddress: '',
                aadharNumber: '',
                uploadSignature: '',
                uploadPhoto: '',
                branchName: '',
                batchName: '',
                courseName: '',
                courseFees: '',
                courseDuration: '',
            });

        });
        console.log(registerData);


    };





    return (
        <>
            <Box sx={{ display: 'flex' }}>
                <Drawer
                    variant="permanent"
                    sx={{
                        width: drawerWidth,
                    }}
                >
                </Drawer>

                <Box component="main" sx={{ flexGrow: 1 }} className='mt-[-54px]' >
                    <Toolbar />
                    <div style={{ marginTop: "2rem" }} className="p-8 bg-zinc-100">
                        <div className="max-w-4xl p-6 mx-auto bg-white rounded-lg shadow">
                            <h2 className="mb-4 text-lg font-semibold">Student Basic Details</h2>
                            <div className="grid grid-cols-3 gap-4 mb-4">
                                <div>
                                    <label className={labelClasses}>Student Name</label>
                                    <input
                                        type="text"
                                        name="studentName"
                                        value={registerData.studentName}
                                        onChange={handleChange}
                                        className={`${inputClasses} ${errors.studentName ? errorClass : ''}`}
                                    />
                                    {errors.studentName && <p className="text-red-500" >Please fill this field.</p>}
                                </div>
                                <div>
                                    <label className={labelClasses}>Father Name</label>
                                    <input
                                        type="text"
                                        name="fatherName"
                                        value={registerData.fatherName}
                                        onChange={handleChange}
                                        className={`${inputClasses} ${errors.fatherName ? errorClass : ''}`}
                                    />
                                    {errors.fatherName && <p className="text-red-500" >Please fill this field.</p>}
                                </div>
                                <div>
                                    <label className={labelClasses}>Mother Name</label>
                                    <input
                                        type="text"
                                        name="motherName"
                                        value={registerData.motherName}
                                        onChange={handleChange}
                                        className={`${inputClasses} ${errors.motherName ? errorClass : ''}`}
                                    />
                                    {errors.motherName && <p className="text-red-500" >Please fill this field.</p>}
                                </div>
                                <div>
                                    <label className={labelClasses} >Date of Reg.</label>
                                    <input
                                        type="date"
                                        name="dateOfRegister"
                                        value={registerData.dateOfRegister}
                                        onChange={handleChange}
                                        readOnly

                                    />
                                </div>
                                <div>
                                    <label className={labelClasses}>Email</label>
                                    <input
                                        type="email"
                                        name="email"
                                        onblur="validateEmail(this)"
                                        value={registerData.email}
                                        onChange={handleChange}
                                        className={`${inputClasses} ${errors.email ? errorClass : ''}`}
                                    />
                                    {errors.email && <p className="text-red-500" >Please fill this field.</p>}


                                </div>

                                <div>
                                    <label className={labelClasses}>Mobile</label>
                                    <input
                                        type="tel"
                                        name="mobNumber"
                                        value={registerData.mobNumber}
                                        onChange={handleChange}
                                        className={`${inputClasses} ${errors.mobNumber ? errorClass : ''}`}
                                    />
                                    {errors.mobNumber && <p className="text-red-500" >Please fill this field.</p>}


                                </div>
                                <div>
                                    <label className={labelClasses}>Date of Birth</label>
                                    <input
                                        type="date"
                                        name="dateOfBirth"
                                        value={registerData.dateOfBirth}
                                        onChange={handleChange}
                                        className={`${inputClasses} ${errors.dateOfBirth ? errorClass : ''}`}
                                    />
                                    {errors.dateOfBirth && <p className="text-red-500" >Please fill this field.</p>}


                                </div>
                                <div>
                                    <label className={labelClasses}>Religion</label>
                                    <select
                                        name="religion"
                                        value={registerData.religion}
                                        onChange={handleChange}
                                        className={`${selectClasses} ${errors.religion ? errorClass : ''}`}
                                    >
                                        <option value='' disabled>Choose Religion</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Muslim">Muslim</option>
                                        <option value="Christian">Christian</option>
                                        <option value="Sikh">Sikh</option>
                                    </select>
                                    {errors.religion && <p className="text-red-500" >Please fill this field.</p>}
                                </div>
                                <div>
                                    <label className={labelClasses}>Mother Tongue</label>
                                    <select
                                        name="motherTongue"
                                        value={registerData.motherTongue}
                                        onChange={handleChange}
                                        className={`${selectClasses} ${errors.motherTongue ? errorClass : ''}`}
                                    >
                                        <option value='' disabled>Choose Mother Tongue</option>
                                        <option value="odia">Odia</option>
                                        <option value="english">English</option>
                                        <option value="hindi">Hindi</option>
                                    </select>
                                    {errors.motherTongue && <p className="text-red-500" >Please fill this field.</p>}

                                </div>
                                <div>
                                    <label className={labelClasses}>Gender</label>
                                    <select
                                        name="gender"
                                        value={registerData.gender}
                                        onChange={handleChange}
                                        className={`${selectClasses} ${errors.gender ? errorClass : ''}`}
                                    >
                                        <option value='' disabled>Choose Gender</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                        <option value="other">Other</option>
                                    </select>
                                    {errors.gender && <p className="text-red-500" >Please fill this field.</p>}
                                </div>
                                <div>
                                    <label className={labelClasses}>Caste</label>
                                    <select
                                        name="caste"
                                        value={registerData.caste}
                                        onChange={handleChange}
                                        className={`${inputClasses} ${errors.caste ? errorClass : ''}`}
                                    >
                                        <option value='' disabled>Choose Caste</option>
                                        <option value="general">General</option>
                                        <option value="sc">SC</option>
                                        <option value="st">ST</option>
                                        <option value="obc">OBC</option>
                                    </select>
                                    {errors.caste && <p className="text-red-500">Please fill this field.</p>}

                                </div>
                                <div>
                                    <label className={labelClasses}>Select Branch</label>
                                    <select
                                        name='branchName'
                                        className={`${selectClasses} ${errors.branch ? errorClass : ''}`}
                                        value={registerData.branchName}
                                        onChange={handleChange}
                                    >
                                        <option disabled value="">Choose Branch</option>
                                        {branchesdata && branchesdata.length > 0 && branchesdata.map(branch => (
                                            <option key={branch._id} value={branch.branchName}>{branch.branchName}</option>
                                        ))}
                                    </select>
                                    {errors.branch && <p className="text-red-500">Please fill this field.</p>}

                                </div>
                                <div>
                                    <label className={labelClasses}>Present Address</label>
                                    <input
                                        type="text"
                                        name="presentAddress"
                                        value={registerData.presentAddress}
                                        onChange={handleChange}
                                        className={`${inputClasses} ${errors.presentAddress ? errorClass : ''}`}
                                    />


                                    {errors.presentAddress && <p className="text-red-500">Please fill this field.</p>}

                                </div>
                                <div>
                                    <label className={labelClasses}>Permanent Address</label>
                                    <input
                                        type="text"
                                        name="permanentAddress"
                                        value={registerData.permanentAddress}
                                        onChange={handleChange}
                                        className={`${inputClasses} ${errors.permanentAddress ? errorClass : ''}`}
                                    />


                                    {errors.permanentAddress && <p className="text-red-500">Please fill this field.</p>}

                                </div>
                                <div>
                                    <label className={labelClasses}>Aadhar Number</label>
                                    <input
                                        type="number"
                                        name="aadharNumber"
                                        value={registerData.aadharNumber}
                                        maxLength="12"
                                        onChange={handleChange}
                                        className={`${inputClasses} ${errors.aadharNumber ? errorClass : ''}`}
                                    />
                                    {errors.aadharNumber && <p className="text-red-500" >Please fill this field.</p>}


                                </div>
                                <FormControl sx={{ width: '31ch' }} variant="outlined">
                                    <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-password"
                                        name="password"
                                        type={showPassword ? 'text' : 'password'}
                                        value={registerData.password}
                                        onChange={handleChange}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={handleClickShowPassword}
                                                    // onMouseDown={handleMouseDownPassword}
                                                    edge="end"
                                                >
                                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                        label="Password"
                                    />
                                </FormControl>
                            </div>
                            <div className=" p-2">

                                {image && (
                                    <div className="mb-2">
                                        <img src={image} alt="Uploaded Signature" style={{ maxWidth: '100%', maxHeight: '50px' }} />
                                    </div>
                                )}

                                <input type="file" className={inputClasses} name='uploadPhoto' onChange={handleFileChange} />
                                <label className={labelClasses}>Upload photo</label>
                            </div>
                            <div className="mt-1 p-2">
                                {signetur && (
                                    <div className="mb-2">
                                        <img src={signetur} alt="Uploaded Signature" style={{ maxWidth: '100%', maxHeight: '50px' }} />
                                    </div>
                                )}

                                <input type="file" className={inputClasses} onChange={FileChange} name='uploadSignature' />
                                <label className={labelClasses}>Upload Signature</label>
                            </div>
                            <div className="max-w-4xl p-6 mx-auto rounded-lg shadow bg-sky-300">
                                <h2 className="mb-4 text-lg font-semibold">Student Admission Details</h2>
                                <div className="grid grid-cols-3 gap-4 mb-4">
                                    <div>
                                        <label className={labelClasses}>Batch Name</label>
                                        <select
                                            value={registerData.batchName}
                                            name='batchName'
                                            onChange={handleChange}
                                            className={`${selectClasses} ${errors.selectedIds ? errorClass : ''}`}
                                        >
                                            <option disabled value="">Choose Batch</option>
                                            {branchesdata && branchesdata.length > 0 && branchesdata?.find(branch => branch.branchName === registerData.branchName)?.batches?.map(batch => (
                                                <option key={batch._id} value={batch.batchName}>{batch.batchName}</option>
                                            ))}
                                        </select>
                                        {errors.selectedIds && <p className="text-red-500" >Please fill this field.</p>}

                                    </div>
                                    <div>
                                        <label className={labelClasses}>Course Name</label>
                                        <select
                                            value={registerData.courseName}
                                            name='courseName'
                                            onChange={handleChange}
                                            className={`${selectClasses} ${errors.selectedIds ? errorClass : ''}`}
                                        >
                                            <option disabled value="">Choose Course Name</option>
                                            {branchesdata && branchesdata.length > 0 && branchesdata?.find(branch => branch.branchName === registerData.branchName)?.courses?.map(course => (
                                                <option key={course._id} value={course.courseName}>{course.courseName}</option>
                                            ))}
                                        </select>
                                        {errors.selectedIds && <p className="text-red-500" >Please fill this field.</p>}
                                    </div>
                                    <div>
                                        <label className={labelClasses}>Course Fee</label>
                                        <input
                                            type="text"
                                            name='courseFees'
                                            value={registerData.courseFees || ''}
                                            readOnly
                                            className={`${inputClasses} ${errors.courseFees ? errorClass : ''}`}
                                        />
                                        {errors.courseFees && <p className="text-red-500" >Please fill this field.</p>}

                                    </div>
                                    <div>
                                        <label className={labelClasses}>Course Duration</label>
                                        <input
                                            type="text"
                                            name='courseDuration'
                                            value={registerData.courseDuration || ''}
                                            className={`${inputClasses} ${errors.courseDuration ? errorClass : ''}`}
                                            readOnly
                                        />
                                        {errors.courseDuration && <p className="text-red-500" >Please fill this field.</p>}

                                    </div>
                                </div>
                            </div>
                            <button onClick={handleregisterData} className="px-4 py-2 mt-4 text-white bg-blue-500 rounded hover:bg-sky-300 hover:text-black">Register</button>
                        </div>

                    </div>
                </Box>
            </Box>
        </>
    );
};

export default UserRegister;