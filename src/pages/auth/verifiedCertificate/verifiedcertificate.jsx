import React from 'react'
import sign from '../../../assets/images/siggn.png'

const Verifiedcertificate = () => {
    return (
        <>
          
                <div className="max-w-3xl mx-auto bg-card p-6 rounded-lg shadow-lg mt-24 mb-20">
                    <div className="bg-gradient-to-r from-blue-500 to-purple-500 p-6 text-center">
                        <img undefinedhidden="true" alt="Institute Logo" src="https://openui.fly.dev/openui/24x24.svg?text=🏫" className="mx-auto mb-4" />
                        <h1 className="text-white text-xl font-bold">Sharma Institute of Technology (SIT) | An ISO 9001:2015 Certified</h1>
                        <p className="text-white text-lg mt-2">A Unit of Sharma Education India Pvt. Ltd.</p>
                        <p className="text-white text-xs mt-4">Regd. No: U80930R2021PTC036955 | Recognized under Ministry of Corporate Affairs, MSME Govt. of India.</p>
                    </div>
                    <h2 className="text-xl font-semibold text-primary text-center bg-yellow-300 mt-4">Certificate Verification Report</h2>
                    <div className="flex items-center my-4">
                        <img src="https://placehold.co/100x100" alt="Profile Picture" className="rounded-full border-2 border-primary mr-4" />
                        <div>
                            <h3 className="text-lg font-bold">Pankaj Kumar Nayak</h3>
                            <p className="text-muted-foreground">D.O.B: 23-07-2004</p>
                            <p className="text-muted-foreground">Guardian Name: Ratnakar Nayak</p>
                        </div>
                    </div>
                    <table className="min-w-full border-collapse border border-border">
                        <thead>
                            <tr className="bg-secondary text-secondary-foreground">
                                <th className="border border-border p-2">Field</th>
                                <th className="border border-border p-2">Details</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td className="border border-border p-2">Course</td>
                                <td className="border border-border p-2">PGDCA/TALLY</td>
                            </tr>
                            <tr>
                                <td className="border border-border p-2">Date of Admission</td>
                                <td className="border border-border p-2">14-12-2022</td>
                            </tr>
                            <tr>
                                <td className="border border-border p-2">Date of Issue</td>
                                <td className="border border-border p-2">30-04-2024</td>
                            </tr>
                            <tr>
                                <td className="border border-border p-2">Total Percentage</td>
                                <td className="border border-border p-2">89.00%</td>
                            </tr>
                            <tr>
                                <td className="border border-border p-2">D.O.B</td>
                                <td className="border border-border p-2">24/01/2001</td>
                            </tr>
                            <tr>
                                <td className="border border-border p-2">Guardian Name</td>
                                <td className="border border-border p-2">xyz</td>
                            </tr>
                            <tr>
                                <td className="border border-border p-2">ATC Center.</td>
                                <td className="border border-border p-2">xyz</td>
                            </tr>
                            <tr>
                                <td className="border border-border p-2">ATC Code.</td>
                                <td className="border border-border p-2">xyz</td>
                            </tr>
                            <tr>
                                <td className="border border-border p-2">Registration No.</td>
                                <td className="border border-border p-2">6723742</td>
                            </tr>
                            <tr>
                                <td className="border border-border p-2">Grade</td>
                                <td className="border border-border p-2">A+</td>
                            </tr>
                            <tr>
                                <td className="border border-border p-2">Status</td>
                                <td className="border border-border p-2">Successfully Completed</td>
                            </tr>
                        </tbody>
                    </table>
                    <div>
                        <h2 className="text-xl font-semibold text-primary text-center bg-yellow-300 mt-4">This Certificate is verified by SG-GROUP.</h2>
                    </div>
                    <div className='text-right'>
                        <img className='w-[130px] float-right' src={sign} alt="sign" />
                        <p className="mt-4 text-muted-foreground">EXAM EXECUTIVE</p>
                    </div>
                    <div className="text-right mt-6">
                        <p className="text-muted-foreground">SG-GROUP.</p>
                    </div>
                </div>
           
        </>
    )
}

export default Verifiedcertificate