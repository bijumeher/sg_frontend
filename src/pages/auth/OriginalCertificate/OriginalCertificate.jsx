import React from 'react'
import logo from '../../../assets/images/logo.png'
import side1 from '../../../assets/images/side1.png'
import side2 from '../../../assets/images/side2.png'
import sign from '../../../assets/images/siggn.png'
import side3 from '../../../assets/images/side3.png'
import barcode from '../../../assets/images/barcode.png'
import sidebarcode1 from '../../../assets/images/sidebarcode1.png'
import sidebarcode2 from '../../../assets/images/sidebarcode2.png'
import iso from '../../../assets/images/isoo.png'








const OriginalCertificate = () => {
    return (
        <>
            <div className="max-w-3xl mx-auto bg-white dark:bg-card border border-border rounded-lg shadow-lg mt-20 mb-20 p-12">
                <div className='border  border-black p-4 '>
                    <div className='flex justify-between'>
                        <img className='w-64 border border-black' src={sidebarcode1} alt="logo"/>
                        <img className='w-64  border border-black' src={sidebarcode2} alt="logo"/>
                    </div>
                    <div className="text-center">
                        <img src={logo} className="mx-auto mb-4 h-44 w-44" />
                        <p className="mt-2 text-muted-foreground text-2xl font-bold">An ISO 9001: 2015 Certified Organisation</p>
                        <img src={side1} alt="side1" />
                        <h1 className="text-4xl font-bold text-primary font-serif">CERTIFICATE</h1>
                        <img src={side2} alt="side2" />
                        <p className="mt-2 text-muted-foreground italic font-serif font-extrabold">In acceptance to the terms and conditions certified that</p>
                    </div>
                    <div className="mt-4 text-center">
                        <h2 className="text-2xl font-bold text-primary">TAPASWINI & JAGYASENI INSTITUTE OF TECHNOLOGY (TJIT)</h2>
                        <p className="text-muted-foreground">Jagannathpur Bhadrak, Near Bhadrak Autonomous College, Odisha - 756100</p>
                    </div>
                    <div className=" flex justify-between">
                        <div>   <p className="font-semibold">
                            Director of Institute Name: <span className="text-muted-foreground">SHREENIBAS GHADAI</span>
                        </p>
                            <p className="font-semibold">
                                Email ID: <span className="text-muted-foreground">tjitcomputercenterbdk@gmail.com</span>
                            </p></div>
                        <div> <img className='h-20 w-20' src={barcode} alt="barcode" /></div>
                    </div>
                    <div className="mt-6 text-center">
                        <div className=" text-secondary-foreground p-2  bg-green-500">
                            <span className="font-bold text-white">Authorised Training Centre (ATC)</span>
                        </div>
                        <p className="mt-2">To conduct software, hardware, short term courses, vocational courses designed and developed by</p>
                        <p className="font-semibold bg-red-500 text-white">Sharma Institute of Technology (SIT),</p>
                        <p className="text-muted-foreground text-red-500">A unit of Sharma Educom India Private Limited (Ministry Corporate Affairs, U80903OR2021PTC036955)</p>
                    </div>
                    <div className="flex justify-between">
                        <p className="font-semibold">
                            Date of Issue: <span className="text-muted-foreground">04.04.2023</span>
                        </p>
                        <p className="font-semibold">
                            (Valid up to 31<sup>st</sup> March 2025, subject to renewal)
                        </p>
                    </div>
                    <div className="mt-6 flex justify-between">
                        <div>
                            <p className="font-semibold">BDK/101</p>
                            <p className="text-muted-foreground">ATC CODE</p>
                        </div>
                        <div>
                            <p className="font-semibold">siteducations.in</p>
                        </div>
                    </div>
                    <div className="mt-6 flex justify-between">
                        <div>
                           <img className='w-60 h-36' src={iso} alt="iso" />
                        </div>
                        <div>
                            <img className='w-20' src={sign} alt="" />
                            <p className="font-semibold">Authorised Signatory</p>
                            <p className="font-semibold">SG Group.</p>

                        </div>
                    </div>
                </div>

            </div>

        </>
    )
}

export default OriginalCertificate