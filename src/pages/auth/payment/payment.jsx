import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { createpayment, getDataBrancWise, getPaymentAlldata } from '../../../actions/student/studentAction';
import { MdClose } from 'react-icons/md';


const Payment = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const { studentId } = useParams();

    const students = useSelector((state) => state.student.studentData);
    const branch = JSON.parse(sessionStorage.getItem('branchData'));
    const branchName = branch?.branchName;

    const student = students.find(student => student._id === studentId);



    useEffect(() => {
        const obj = { branchName: branchName };
        dispatch(getDataBrancWise(obj));
    }, [dispatch, branchName]);

    const [paymentdata, setPaymentData] = useState({
        studentId: studentId,
        Paidamount: '',
        RestAmount: '',
        paymentMode: '',

        paymentDate: '',
        installmentPeriod: '',
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setPaymentData({ ...paymentdata, [name]: value });
    };


    const formatDateToInput = (dateString) => {
        const date = new Date(dateString);
        const year = date.getFullYear();
        const month = String(date.getMonth() + 1).padStart(2, '0');
        const day = String(date.getDate()).padStart(2, '0');
        return `${year}-${month}-${day}`;
    };

    const handleDateChange = (e) => {
        const paymentDate = e.target.value;
        setPaymentData({ ...paymentdata, paymentDate: paymentDate }); // Date input is already in YYYY-MM-DD format
    };

    const handleSubmit = async () => {
        const { Paidamount, RestAmount, paymentMode, paymentDate, installmentPeriod } = paymentdata;
        if (!Paidamount || !RestAmount || !paymentMode || !paymentDate || !installmentPeriod) {
            alert("Please fill in all required fields.");
            return;
        }
     

      
        dispatch(createpayment(paymentdata)).then(() => {
                setPaymentData({
                    studentId: '',
                    Paidamount: '',
                    RestAmount: '',
                    paymentMode: '',
                    paymentDate: '',
                    installmentPeriod: '1',
                });
                dispatch(getPaymentAlldata());
                navigate(`/paymentreceiptt`, { state: {  paymentdata, student } });
        });
    };
    // console.log(student);
    if (!student) {
        return <div>Loading...</div>;
    }
    return (
        <>
            <div className="relative">
                <div className="max-w-4xl mx-auto p-4 bg-card text-card-foreground rounded-lg shadow-md mt-24 relative">
                    <MdClose
                        size={24}
                        className="absolute top-1 right-3 cursor-pointer text-red-500 z-10"
                        onClick={() => navigate(-1)}
                    />
                    <div className="mb-6">
                        <h2 className="text-lg font-semibold bg-[#1565c0] text-primary-foreground p-2 rounded-t-lg">Payment Details</h2>
                        <div className="p-4 border border-border rounded-b-lg">
                            <div className="grid grid-cols-2 gap-4">
                                <div>
                                    <div>
                                        <label className="block text-sm font-medium mb-1" htmlFor="card-number">Course Name:</label>
                                        <input id="card-number"
                                            type="text"
                                            className="w-full p-2 border border-input rounded"
                                            value={student.courseName}
                                            readOnly />
                                    </div>
                                    <div>
                                        <label className="block text-sm font-medium mb-1" htmlFor="Enter an amount">Total Paid amount :</label>
                                        <input id="Enter an amount" type="number" className="w-full p-2 border border-input rounded" name="Paidamount"
                                            value={paymentdata.Paidamount}
                                            onChange={handleChange} />
                                    </div>
                                    <label className="block text-sm font-medium mb-1" htmlFor="payment-mode">Select Payment Method:</label>
                                    <select id="payment-mode" className="w-full p-2 border border-input rounded" name='paymentMode' value={paymentdata.paymentMode}
                                        onChange={handleChange}>
                                        <option value='' disabled>Choose Type</option>
                                        <option value="online">Online</option>
                                        <option value="offline">Offline</option>
                                    </select>
                                </div>
                                <div>
                                    <div>
                                        <label className="block text-sm font-medium mb-1" htmlFor="card-number">Course Fee:</label>
                                        <input id="card-number"
                                            type="text"
                                            className="w-full p-2 border border-input rounded"
                                            value={student.courseFees}
                                            readOnly />
                                    </div>
                                    <div>
                                        <label className="block text-sm font-medium mb-1" htmlFor="Enter an amount">Rest Of Amount:</label>
                                        <input id="Enter an amount" type="number" className="w-full p-2 border border-input rounded" name='RestAmount' value={paymentdata.RestAmount}
                                            onChange={handleChange} />
                                    </div>
                                    <div>
                                        <label className="block text-sm font-medium mb-1" htmlFor="Enter date">Select Date:</label>
                                        <input id="Enter date" type="date" className="w-full p-2 border border-input rounded" value={formatDateToInput(paymentdata.paymentDate)}
                                            onChange={handleDateChange} />
                                    </div>
                                    <div>
                                        <label className="block text-sm font-medium mb-1" htmlFor="installment-period">Installment Period:</label>
                                        <select id="installment-period" className="w-full p-2 border border-input rounded" name='installmentPeriod' value={paymentdata.installmentPeriod}
                                            onChange={handleChange}>
                                            <option value='' disabled>Choose Months</option>
                                            {[...Array(12).keys()].map((month) => (

                                                <option key={month + 1} value={month + 1}>{month + 1} Month{month + 1 > 1 ? 's' : ''}</option>
                                            ))}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="text-center">
                        <button className="bg-[#1565c0] text-primary-foreground hover:bg-primary/80 px-4 py-2 rounded" onClick={handleSubmit}>Submit</button>
                    </div>
                </div>
            </div>

        </>
    );
};

export default Payment;
