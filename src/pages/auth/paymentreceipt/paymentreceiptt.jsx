import { Button } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { PDFDownloadLink, Document, Page, Text, View, StyleSheet, Image } from '@react-pdf/renderer';
import { red } from '@mui/material/colors';
import { useDispatch, useSelector } from 'react-redux';

import logoFO from '../../../assets/images/logo.png';
const Paymentreceiptt = () => {
    const location = useLocation();
    const dispatch = useDispatch();
    const { paymentdata, payment, student } = location.state || {};
    const students = useSelector((state) => state.student.studentData);
    console.log("students:", students);

    const [receiptId, setReceiptId] = useState('');

    useEffect(() => {
        // Retrieve payment data from localStorage
        const storedPaymentData = localStorage.getItem('paymentData');
        if (storedPaymentData) {
            const paymentData = JSON.parse(storedPaymentData);
            setReceiptId(paymentData.receiptId);
        }
    }, []);




    console.log(paymentdata);




    const formatDateToInput = (dateString) => {
        const date = new Date(dateString);
        const year = date.getFullYear();
        const month = String(date.getMonth() + 1).padStart(2, '0');
        const day = String(date.getDate()).padStart(2, '0');
        return `${year}-${month}-${day}`;
    };





    const CombinedReceiptDocument = () => (
        <Document>
            <Page size="A4" style={styles.page}>
                <View style={styles.section}>
                    <View style={styles.border}>
                        <Image source={require('../../../assets/images/logo.png')} style={styles.image} />
                        <Text style={styles.header}>Tapaswini & Jagyaseni Institute Of Technology | Mobile - 7008847717 </Text>
                        <Text style={styles.para}>Jagannathpur Bhadrak, Near Bhadrak Autonomous College, Odisha - 756100</Text>
                        <Text style={styles.para}>Email - tjitcomputercenterbdk@gmail.com</Text>
                    </View>
                </View>

                {/* Student Payment Slip */}
                <View style={styles.section}>
                    <Text style={styles.title}>Student Payment Slip</Text>
                    <View style={styles.row}>
                        <Text style={styles.label}>M.R No: BDK/101/24-25/07/07</Text>
                        <Text style={styles.label}>Date: {formatDateToInput(paymentdata?.paymentDate || payment?.paymentDate || '')}</Text>
                    </View>
                    <View style={styles.background}>
                        <View style={styles.row}>
                            <Text style={styles.label}>Received From:</Text>
                            <Text style={styles.dottedLine}>{student?.studentName || ''}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.label}>a sum of rupees:</Text>
                            <Text style={styles.dottedLine}></Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.label}>By:</Text>
                            <Text style={styles.dottedLine}>{paymentdata?.paymentMode || payment?.paymentMode}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.label}>On account of:</Text>
                            <Text style={styles.dottedLine}></Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.label}>Installment Period:</Text>
                            <Text style={styles.dottedLine}>{paymentdata?.installmentPeriod || payment?.installmentPeriod || ''} months</Text>
                        </View>
                        <View style={styles.amountcontainer}>
                            <Image source={require('../../../assets/images/icons8-rupee-48.png')} style={styles.rupeeSymbol} />
                            <Text style={styles.amount}>{paymentdata?.Paidamount}</Text>
                            <Text style={styles.institute}>For Tapaswini & Jagyaseni Institute Of Technology Authorized Signatory</Text>
                        </View>
                        <View style={styles.rows}>
                            <Text>Note: Cheque / D.D are valid subject to realisation of amount</Text>
                        </View>
                    </View>
                </View>

                {/* Branch Payment Slip */}
                <View style={styles.section}>
                    <View style={styles.border}>
                    <Image source={require('../../../assets/images/logo.png')} style={styles.image} />
                        <Text style={styles.header}>Tapaswini & Jagyaseni Institute Of Technology | Mobile - 7008847717 </Text>
                        <Text style={styles.para}>Jagannathpur Bhadrak, Near Bhadrak Autonomous College, Odisha - 756100</Text>
                        <Text style={styles.para}>Email - tjitcomputercenterbdk@gmail.com</Text>

                    </View>
                </View>
                <View style={styles.section}>
                    <Text style={styles.title}>Branch Payment Slip</Text>
                    <View style={styles.row}>
                        <Text style={styles.label}>M.R No: BDK/101/24-25/07/07</Text>
                        <Text style={styles.label}>Date: {formatDateToInput(paymentdata?.paymentDate || payment?.paymentDate || '')}</Text>
                    </View>
                    <View style={styles.background}>
                        <View style={styles.row}>
                            <Text style={styles.label}>Received From:</Text>
                            <Text style={styles.dottedLine}>{student?.studentName || ''}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.label}>a sum of rupees:</Text>
                            <Text style={styles.dottedLine}></Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.label}>By:</Text>
                            <Text style={styles.dottedLine}>{paymentdata?.paymentMode || payment?.paymentMode}</Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.label}>On account of:</Text>
                            <Text style={styles.dottedLine}></Text>
                        </View>
                        <View style={styles.row}>
                            <Text style={styles.label}>Installment Period:</Text>
                            <Text style={styles.dottedLine}>{paymentdata?.installmentPeriod || payment?.installmentPeriod || ''} months</Text>
                        </View>
                        <View style={styles.amountcontainer}>
                            <Image source={require('../../../assets/images/icons8-rupee-48.png')} style={styles.rupeeSymbol} />
                            <Text style={styles.amount}>{paymentdata?.Paidamount}</Text>
                            <Text style={styles.institute}>For Tapaswini & Jagyaseni Institute Of Technology Authorized Signatory</Text>
                        </View>
                        <View style={styles.rows}>
                            <Text>Note: Cheque / D.D are valid subject to realisation of amount</Text>
                        </View>
                    </View>
                </View>
            </Page>
        </Document>
    );

    const styles = StyleSheet.create({
        page: {
            padding: 20,
        },
        section: {
            marginBottom: 10,
        },
        border: {
            border: '1px dotted black',
            padding: 5,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'

        },
        para: {
            fontSize: 10,
            textAlign: 'center'
        },
        date: {
            textAlign: 'right'
        },
        header: {
            fontSize: 15,
            textAlign: 'center',
            marginBottom: 10,
        },
        title: {
            fontSize: 16,
            textAlign: 'center',
            marginBottom: 10,
        },
        background: {
            backgroundColor: '#f5f5f5'
        },
        row: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginBottom: 10,
        },
        amountcontainer: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 10,
        },
        rupeeSymbol: {
            border: '1px solid black',
            width: '5%'
        },
        amount: {
            border: '1px solid black',
            padding: '2px',
            width: '25%',
            marginRight: '30%',
        },
        label: {
            width: '20%',
            fontSize: 11
        },
        labels: {
            width: '100%'
        },
        image: {
            width: 50,
            height: 50,
            display: 'flex',
            resizeMode: 'contain',
            justifyContent: 'space-between',
            alignItems: 'center',
           
        },
        value: {
            width: '100%',
            borderBottomStyle: 'dotted',
        },
        dottedLine: {
            width: '100%',
            borderBottomWidth: 1,
            borderBottomStyle: 'dotted',
            borderBottomColor: '#000',
            fontSize: 11
        },
        rows: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            fontSize: 9,
            textAlign: 'center'
        },
        institute: {
            fontSize: 9,
            textAlign: 'center',
            marginRight: '20%'
        },
        imageContainer: {
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: 20,
        },

    });

    return (
        <div className='mt-20 p-8'>
            <div className="max-[666px] mx-auto p-4 bg-card text-foreground rounded-lg shadow-lg">
                <div className='border border-dotted d-flex flex-col items-center justify-center'>
                    <img src={logoFO} alt="Institute Logo" className="w-23 h-20 object-cover text-center " />
                    <h1 className="text-2xl font-bold text-center">Tapaswini & Jagyaseni Institute Of Technology | Mobile - 7008847717</h1>
                    <p className="text-center">
                        Jagannathpur Bhadrak, Near Bhadrak Autonomous College, Odisha - 756100, Email -{' '}
                        <a href="mailto:tjitcomputercenterbdk@gmail.com" className="text-primary">
                            tjitcomputercenterbdk@gmail.com
                        </a>
                    </p>
                </div>

                <div className='mt-8'>
                    <h2 className="text-lg font-semibold text-center">Student Payment Slip</h2>
                    {/* Student Payment Slip Form */}
                    <div className='flex gap-10 float-right'>
                        <label className="block">Date</label>
                        <input type="date" className="border-b border-dotted border-zinc-400 w-full focus:outline-none focus:border-primary" value={formatDateToInput(paymentdata?.paymentDate || payment?.paymentDate || '')} />
                    </div>

                    {/* {student.payments.map(pay => ( */}
                    <p className="mt-4" >

                        {receiptId ? (
                            <p>M.R No: {receiptId}</p>
                        ) : (
                            <p>No receipt available</p>
                        )}

                        {/* M.R No: {student.branchName}-{receiptId} */}
                    </p>
                    {/* ))} */}


                    <div className="mt-6">
                        <div className="grid gap-4">
                            <div className='flex gap-10'>
                                <label className="block">Received From</label>
                                <input type="text" className="border-b border-dotted border-zinc-400 w-full focus:outline-none focus:border-primary" value={student?.studentName || ''} readonly />
                            </div>
                            <div className='flex gap-10'>
                                <label className="block">a sum of Rupees</label>
                                <input type="text" className="border-b border-dotted border-zinc-400 w-full focus:outline-none focus:border-primary" />
                            </div>
                            <div className='flex gap-10'>
                                <label className="block">by</label>
                                <input type="text" className="border-b border-dotted border-zinc-400 w-full focus:outline-none focus:border-primary" value={paymentdata?.paymentMode || payment?.paymentMode} />
                            </div>
                            <div className='flex gap-10'>
                                <label className="block">on account of</label>
                                <input type="text" className="border-b border-dotted border-zinc-400 w-full focus:outline-none focus:border-primary" />
                            </div>
                        </div>
                        <div className="mt-4 flex gap-10">
                            <label className="block">Amount</label>
                            <div className="flex items-center">
                                <span className="mr-2">₹</span>
                                <input type="text" className="border-b border-dotted border-zinc-400 w-full focus:outline-none focus:border-primary" value={paymentdata?.Paidamount || payment?.Paidamount || ''} />
                            </div>
                        </div>
                        <div className="mt-4 flex gap-10">
                            <label className="block">
                                Installment Period (in months)
                            </label>
                            <input id="installment-period" type="text" className="border-b border-dotted border-zinc-400 w-full focus:outline-none focus:border-primary" value={paymentdata?.installmentPeriod || payment?.installmentPeriod || ''} readOnly />
                        </div>
                    </div>

                    <div className="flex gap-10">
                        <div>
                            <p className="mt-4 text-sm text-muted-foreground">Note: Cheque / D.D are valid subject to realisation of amount</p>
                        </div>
                        <div>
                            <p className="text-sm">For Tapaswini & Jagyaseni Institute Of Technology</p>
                            <p className="text-sm">Authorized Signatory</p>
                        </div>
                    </div>
                </div>

                <div className="mt-8">
                    <h2 className="text-lg font-semibold text-center">Branch Payment Slip  </h2>
                    <div className='flex gap-10 float-right'>
                        <label className="block">Date</label>
                        <input type="date" className="border-b border-dotted border-zinc-400 w-full focus:outline-none focus:border-primary" value={formatDateToInput(paymentdata?.paymentDate || payment?.paymentDate || '')} />
                    </div>
                    <p className="mt-4" >
                    {receiptId ? (
                            <p>M.R No: {receiptId}</p>
                        ) : (
                            <p>No receipt available</p>
                        )}
                        </p>
                    <div className="mt-6">
                        <div className="grid gap-4">
                            <div className='flex gap-10'>
                                <label className="block">Received From</label>
                                <input type="text" className="border-b border-dotted border-zinc-400 w-full focus:outline-none focus:border-primary" value={student?.studentName || ''} readonly />
                            </div>


                            <div className='flex gap-10'>
                                <label className="block">a sum of Rupees</label>
                                <input type="text" className="border-b border-dotted border-zinc-400 w-full focus:outline-none focus:border-primary" />
                            </div>
                            <div className='flex gap-10'>
                                <label className="block">by</label>
                                <input type="text" className="border-b border-dotted border-zinc-400 w-full focus:outline-none focus:border-primary" value={paymentdata?.paymentMode || payment?.paymentMode} />
                            </div>
                            <div className='flex gap-10'>
                                <label className="block">on account of</label>
                                <input type="text" className="border-b border-dotted border-zinc-400 w-full focus:outline-none focus:border-primary" />
                            </div>
                        </div>
                        <div className="mt-4 flex gap-10">
                            <label className="block">Amount</label>
                            <div className="flex items-center">
                                <span className="mr-2">₹</span>
                                <input type="text" className="border-b border-dotted border-zinc-400 w-full focus:outline-none focus:border-primary" value={paymentdata?.Paidamount || payment?.Paidamount || ''} />
                            </div>
                        </div>
                        <div className=' mt-4 flex gap-10'>
                            <label className="block">Installment Period (in months)</label>
                            <input type="text" className="border-b border-dotted border-zinc-400 w-full focus:outline-none focus:border-primary" value={paymentdata?.installmentPeriod || payment?.installmentPeriod || ''} readOnly />
                        </div>
                    </div>
                    <div className="flex gap-10">
                        <div>
                            <p className="mt-4 text-sm text-muted-foreground">Note: Cheque / D.D are valid subject to realisation of amount</p>
                        </div>
                        <div>
                            <p className="text-sm">For Tapaswini & Jagyaseni Institute Of Technology</p>
                            <p className="text-sm">Authorized Signatory</p>
                        </div>
                    </div>
                </div>

                <div className='text-center  mt-8'>
                    <PDFDownloadLink document={<CombinedReceiptDocument />} fileName="combined_payment_receipt.pdf">
                        {({ blob, url, loading, error }) =>
                            loading ? 'Generating PDF...' : <Button variant="contained" color="primary">Download Payment Slip</Button>
                        }
                    </PDFDownloadLink>
                </div>
            </div>
        </div>
    );
};


export default Paymentreceiptt;
