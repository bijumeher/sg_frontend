import React, { useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { PDFDownloadLink, Document, Page, Text, View, StyleSheet } from '@react-pdf/renderer';
import { MdClose } from 'react-icons/md';

const styles = StyleSheet.create({
  page: {
    backgroundColor: '#ffffff',
    padding: 24,
  },
  section: {
    marginBottom: 12,
    borderBottom: '1 solid #000000',
    paddingBottom: 8,
  },
  title: {
    fontSize: 24,
    textAlign: 'center',
    marginBottom: 12,
    color: '#2C3E50',
  },
  label: {
    fontSize: 12,
    marginBottom: 4,
    color: '#34495E',
  },
  value: {
    fontSize: 14,
    marginBottom: 8,
    color: '#2C3E50',
  },
  signature: {
    marginTop: 20,
    fontSize: 14,
    color: '#34495E',
  },
  bold: {
    fontWeight: 'bold',
  },
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  flexColumn: {
    display: 'flex',
    flexDirection: 'column',
  },
  closeButton: {
    position: 'absolute',
    top: '13%',
    right: '31%',
    cursor: 'pointer',
    color: '#ff0000',
    zIndex: 10, 
  },
});

const PaymentReceipt = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const { paymentdata, payment, student } = location.state || {};
  const [preparedBy, setPreparedBy] = useState('');
  const [receivedBy, setReceivedBy] = useState('');
  const formatDateToInput = (dateString) => {
    const date = new Date(dateString);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    return `${year}-${month}-${day}`; // Format for <input type="date">
};

  const ReceiptDocument = () => (
    <Document>
      <Page style={styles.page}>
        <View style={styles.section}>
          <Text style={styles.title}>PAYMENT RECEIPT</Text>
        </View>
        <View style={styles.section}>
          <Text style={[styles.label, styles.bold]}>Student Name:</Text>
          <Text style={styles.value}>{student?.studentName || ''}</Text>
        </View>
        <View style={styles.section}>
          <Text style={[styles.label, styles.bold]}>Date:</Text>
          <Text style={styles.value}>{formatDateToInput(paymentdata?.paymentDate || payment?.paymentDate || '')}</Text>
        </View>
        <View style={styles.section}>
          <Text style={[styles.label, styles.bold]}>Paid Amount:</Text>
          <Text style={styles.value}>{paymentdata?.Paidamount || payment?.Paidamount || ''}</Text>
        </View>
        <View style={styles.section}>
          <Text style={[styles.label, styles.bold]}>Rest Amount:</Text>
          <Text style={styles.value}>{paymentdata?.RestAmount || payment?.RestAmount || ''}</Text>
        </View>
        <View style={styles.section}>
          <Text style={[styles.label, styles.bold]}>Course Name:</Text>
          <Text style={styles.value}>{student?.courseName || ''}</Text>
        </View>
        <View style={styles.section}>
          <Text style={[styles.label, styles.bold]}>Course Fee:</Text>
          <Text style={styles.value}>{student?.courseFees || ''}</Text>
        </View>
        <View style={styles.section}>
          <Text style={[styles.label, styles.bold]}>Installment Period:</Text>
          <Text style={styles.value}>{paymentdata?.installmentPeriod || payment?.installmentPeriod || ''} months</Text>
        </View>
        <View style={styles.container}>
          <View style={styles.flexColumn}>
            <Text style={styles.signature}>Prepared By: {preparedBy}</Text>
          </View>
          <View style={styles.flexColumn}>
            <Text style={styles.signature}>Received By: {receivedBy}</Text>
          </View>
        </View>
      </Page>
    </Document>
  );
 

  return (
    <div className=' p-16 '>
      <MdClose
        size={24}
        style={styles.closeButton}
        onClick={() => navigate(-1)}
      />
      <div className="max-w-xl mx-auto p-4 bg-card text-card-foreground border border-border rounded-lg shadow-lg  relative ">
        <h2 className="text-center text-xl font-bold mb-4">PAYMENT RECEIPT</h2>
        <div className="flex justify-between mb-4">
          <div className="flex space-x-2">
            <label htmlFor="course-name" className="self-center">
              Student Name
            </label>
            <input id="course-name" type="text" className="border-b-4 outline-none px-2 py-1" value={student?.studentName || ''} readOnly />
          </div>
          <div className="flex space-x-2">
            <label htmlFor="date" className="self-center">
              Date:
            </label>
            <input id="date" className="border-b-4 outline-none px-2 py-1" value={formatDateToInput(paymentdata?.paymentDate || payment?.paymentDate || '')} readOnly />
          </div>
        </div>
        <div className="mb-4">
          <label htmlFor="sum" className="block mb-1">
            Enter Paid Amount:
          </label>
          <input id="sum" type="number" className="w-full border-b-4 outline-none px-2 py-1" value={paymentdata?.Paidamount || payment?.Paidamount || ''} readOnly />
        </div>
        <div className="mb-4">
          <label htmlFor="words" className="block mb-1">
            Enter Rest Amount:
          </label>
          <input id="words" type="text" className="w-full border-b-4 outline-none px-2 py-1" value={paymentdata?.RestAmount || payment?.RestAmount || ''} readOnly />
        </div>
        <div className="mb-4">
          <label htmlFor="course-name" className="block mb-1">
            Course Name
          </label>
          <input id="course-name" type="text" className="w-full border-b-4 outline-none px-2 py-1" value={student?.courseName || ''} readOnly />
        </div>
        <div className="mb-4">
          <label htmlFor="paid-to" className="block mb-1">
            Course Fee
          </label>
          <input id="paid-to" type="text" className="w-full border-b-4 outline-none px-2 py-1" value={student?.courseFees || ''} readOnly />
        </div>
        <div className="mb-4">
          <label htmlFor="installment-period" className="block mb-1">
            Installment Period (in months)
          </label>
          <input id="installment-period" type="text" className="w-full border-b-4 outline-none px-2 py-1" value={paymentdata?.installmentPeriod || payment?.installmentPeriod || ''} readOnly />
        </div>
        <div className="flex justify-between mt-4">
          <div className="flex space-x-2">
            <label htmlFor="prepared-by" className="self-center">
              Prepared By:
            </label>
            <input
              id="prepared-by"
              type="text"
              className="border-b-4 outline-none px-2 py-1"
              value={preparedBy}
              onChange={(e) => setPreparedBy(e.target.value)}
            />
          </div>
          <div className="flex space-x-2">
            <label htmlFor="received-by" className="self-center">
              Received By:
            </label>
            <input
              id="received-by"
              type="text"
              className="border-b-4 outline-none px-2 py-1"
              value={receivedBy}
              onChange={(e) => setReceivedBy(e.target.value)}
            />
          </div>
        </div>
        <div className="mt-4 text-center">
          <PDFDownloadLink document={<ReceiptDocument />} fileName="payment_receipt.pdf">
            {({ blob, url, loading, error }) =>
              loading ? 'Generating PDF...' : <button className="btn btn-primary">Download PDF</button>
            }
          </PDFDownloadLink>
        </div>
      </div>
    </div>
  );
};

export default PaymentReceipt;
