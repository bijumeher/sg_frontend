
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { applyLoan } from '../../actions/applyLoan/applyLoanAction';

const inputClasses = "w-full h-40px border-zinc-300 rounded p-2 border rounded-lg";
const labelClasses = "mb-1 block";

const ApplyLoan = () => {
  const dispatch = useDispatch();

  const [loanData, setloanData] = useState({
    name: '',
    email: '',
    mobile: '',
    aadhaarcard: '',
    pancard: '',
    address: ''
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setloanData({ ...loanData, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await dispatch(applyLoan(loanData));
      console.log('Loan application submitted successfully');
      // Optionally, you can reset the form after successful submission
      setloanData({
        name: '',
        email: '',
        mobile: '',
        aadhaarcard: '',
        pancard: '',
        address: ''
      });
    } catch (error) {
      console.error('Error submitting loan application:', error);
    }
  };

  return (
    <div style={{ marginTop: "3rem" }} className="p-8 bg-zinc-100">
      <div className="max-w-4xl p-6 mx-auto bg-white rounded-lg shadow">
        <h2 className="mb-4 text-lg font-semibold">Apply For Loan</h2>
        <form onSubmit={handleSubmit}>
          <div className="grid grid-cols-3 gap-4 mb-4">
            <div>
              <label className={labelClasses}>Your Name</label>
              <input type="text" name="name" value={loanData.name} onChange={handleChange} className={inputClasses} />
            </div>
            <div>
              <label className={labelClasses}>Email</label>
              <input type="email" name="email" value={loanData.email} onChange={handleChange} className={inputClasses} />
            </div>
            <div>
              <label className={labelClasses}>Mobile</label>
              <input type="number" name="mobile" value={loanData.mobile} onChange={handleChange} className={inputClasses} />
            </div>
            <div>
              <label className={labelClasses}>Aadhar No</label>
              <input type="number" name="aadhaarcard" value={loanData.aadhaarcard} onChange={handleChange} className={inputClasses} />
            </div>
            <div>
              <label className={labelClasses}>PAN Number</label>
              <input type="text" name="pancard" value={loanData.pancard} onChange={handleChange} className={inputClasses} />
            </div>
            <div>
              <label className={labelClasses}>Address</label>
              <input type="text" name="address" value={loanData.address} onChange={handleChange} className={inputClasses} />
            </div>
          </div>
          <button type="submit" className="px-4 py-2 mt-4 text-white bg-blue-500 rounded hover:bg-sky-300 hover:text-black">Apply Loan</button>
        </form>
      </div>
    </div>
  );
};

export default ApplyLoan;
