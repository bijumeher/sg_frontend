import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
// import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import sglogo from '../../assets/images/logo.png';
import BasicButtons from './button';
import styles from './header.module.css'
import { Link } from 'react-router-dom';
import { ExpandLess, ExpandMore } from '@mui/icons-material';
import { Collapse } from '@mui/material';

const drawerWidth = 240;

const navItems = [
    {
        id: 1,
        text: "HOME",
        url: "/"
    },
    {
        id: 2,
        text: "LOGIN",
        url: "/login"

    },
    {
        id: 3,
        text: "Loan",
        url: "https://sggroupofmedia.com/web/index.php"
    },
    {
        id: 4,
        text: "REGISTER",
        url: "/register"
    },
   
    {
        id: 5,
        text: "ABOUT US",
        url: "/about"
    },
    {
        id: 6,
        text: "CONTACT",
        url: "/contact"
    },
    {
        id: 7,
        text: "Certificate Verification",
        url: ""
    }
];

function  Header(props) {
    const { window } = props;
    const [mobileOpen, setMobileOpen] = React.useState(false);


    const handleDrawerToggle = () => {
        // setMobileOpen((prevState) => !prevState);
        setMobileOpen(false)
    };

    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
        setOpen(!open);

    };
    const nestedClick = () => {
        setOpen(false);
        setMobileOpen(false)


    };
    const drawer = (
        <Box sx={{ display: "flex", flexDirection: "column", alignItems: "center", textAlign: 'center', position: "fixed" }}>
            <Typography variant="h6" sx={{ my: 2 }} >
                <img style={{ height: "90px", cursor: "pointer" }} src={sglogo} alt="sg group" />
            </Typography>
            <Divider />
            <List
                sx={{ color: "orange" }}

            >
                <Link style={{ color: "orange" }} to="/">
                    <ListItemButton
                        onClick={() => setMobileOpen(false)}
                    >
                        <ListItemText primary="Home" />
                    </ListItemButton>
                </Link>

                <ListItemButton onClick={handleClick}>
                    <ListItemText primary="Education" />
                    {open ? <ExpandLess /> : <ExpandMore />}
                </ListItemButton>
                <Collapse in={open} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <Link style={{ color: "black" }} to="/login">
                            <ListItemButton sx={{ pl: 4 }} onClick={nestedClick} >
                                <ListItemText primary="Login" />
                            </ListItemButton>
                        </Link>
                        <Link style={{ color: "black" }} to="/contact">

                            <ListItemButton sx={{ pl: 4 }} onClick={nestedClick}>
                                <ListItemText primary="Contact" />
                            </ListItemButton>
                        </Link>
                        {/* <Link style={{ color: "black" }} to="/register">

                            <ListItemButton sx={{ pl: 4 }} onClick={nestedClick} >
                                <ListItemText primary="Register" />
                            </ListItemButton>
                        </Link> */}

                    </List>
                </Collapse>
                <Link style={{ color: "orange" }} to="/about">

                    <ListItemButton onClick={nestedClick}>
                        <ListItemText primary="About us" />
                    </ListItemButton>
                </Link>

                <Link style={{ color: "orange" }} to="https://sggroupofmedia.com/web/index.phpt">

                    <ListItemButton onClick={nestedClick}>
                        <ListItemText primary="Loan" />
                    </ListItemButton>
                </Link>
                <Link style={{ color: "orange" }} to="/">

                    <ListItemButton onClick={nestedClick}>
                        <BasicButtons />
                    </ListItemButton>
                </Link>

            </List>
        </Box>
    );

    const container = window !== undefined ? () => window().document.body : undefined;


    return (
        <Box sx={{ display: 'flex', position: "absolute" }} >
            <CssBaseline />
            <AppBar style={{ background: "#ffffff" }} component="nav" >
                <Toolbar >
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={() => setMobileOpen(true)}
                        sx={{ mr: 2, display: { sm: 'none' } }}
                    >
                        <MenuIcon sx={{color:'black'}}/>
                    </IconButton>
                    <Typography
                        variant="h6"
                        component="div"
                        sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
                    >
                        <img style={{ height: "70px" }} src={sglogo} alt="sg group" />
                    </Typography>
                    <Box sx={{ display: { xs: 'none', sm: 'block' } }}>

                        {navItems.map((item) => (
                            <Link to={item.url} >
                                <Button className={styles.btn} key={item.id} sx={{ color: 'orange', fontWeight: 700 }}>
                                    {item.text}
                                </Button>
                            </Link>
                        ))}
                        <Button>
                            <Link to="/apply">
                                <BasicButtons />
                            </Link>
                        </Button>

                    </Box>
                </Toolbar>
            </AppBar>
            <nav>
                <Drawer
                    container={container}
                    variant="temporary"
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                    sx={{
                        display: { xs: 'block', sm: 'none' },
                        '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                    }}
                >
                    {drawer}
                </Drawer>
            </nav>
            <Box component="main" sx={{ p: 3 }}>
                <Toolbar />
            </Box>
        </Box>
    );
}

Header.propTypes = {
    window: PropTypes.func,
};

export default Header;
