import React, { useState } from 'react'
import Dropdown from 'react-bootstrap/Dropdown';
import 'bootstrap/dist/css/bootstrap.min.css';
import styles from './dropdown.module.css'
import { Link } from 'react-router-dom';

function Dropdownmenu() {
    const [isOpen, setIsOpen] = useState(false);

    const handleMouseEnter = () => {
      setIsOpen(true);
    };
  
    const handleMouseLeave = () => {
      setIsOpen(false);
    };
    return (
        <div>
            <Dropdown
             show={isOpen}
             onMouseEnter={handleMouseEnter}
             onMouseLeave={handleMouseLeave} 
            >
                <Dropdown.Toggle style={{backgroundColor:"transparent",border:"none",fontSize:" 0.875rem",color:"#e8911b",fontWeight:700}} >
                    LOGIN
                </Dropdown.Toggle>

                <Dropdown.Menu className={styles.navLinkItem}>
                   
                    <Dropdown.Item className={styles.dropMenu}><Link onClick={handleMouseLeave} to={`/login`} >LOGIN</Link> </Dropdown.Item>
                    <Dropdown.Item className={styles.dropMenu} ><Link onClick={handleMouseLeave} to={`/contact`} >CONTACT</Link> </Dropdown.Item>
                    {/* <Dropdown.Item className={styles.dropMenu} > <Link onClick={handleMouseLeave} to={`/register`} >REGISTER</Link> </Dropdown.Item> */}
                </Dropdown.Menu>
            </Dropdown>
        </div>
    )
}

export default Dropdownmenu
