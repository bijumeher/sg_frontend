import React from 'react'
import { useTypewriter, Cursor } from 'react-simple-typewriter';
import service1 from '../../../assets/images/services-1.jpg'
import service2 from '../../../assets/images/services-2.jpg'
import service3 from '../../../assets/images/services-3.jpg'
import service4 from '../../../assets/images/services-4.jpg'
import Rating from '@mui/material/Rating';
import c1 from '../../../assets/images/media2.jpg'
import c3 from '../../../assets/images/media.jpg'
import c4 from '../../../assets/images/media3.jpg'
import c6 from '../../../assets/images/media4.jpg'
import banner from '../../../assets/images/tech.jpg'
import LooksOneIcon from '@mui/icons-material/LooksOne';
import LooksTwoIcon from '@mui/icons-material/LooksTwo';
import Looks3Icon from '@mui/icons-material/Looks3';
import { BsPeopleFill } from "react-icons/bs";
import { TfiCup } from "react-icons/tfi";
import { FaMobileRetro } from "react-icons/fa6";
import MultiCarousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import style from '../card/card.module.css'


const card = [
    {
        id: 0,
        img: service1,
        title: "Personal Loan"


    },
    {
        id: 1,
        img: service2,
        title: "Business Loan"


    },
    {
        id: 2,
        img: service3,
        title: "Home Loan"


    },
    {
        id: 3,
        img: service4,
        title: "Education Loan"


    },
]
// const box = [
//     {
//         id: 0,
//         pic: <MdAddHomeWork />,
//         percent: "10.50%*",
//         loan: "Personal Loan"
//     },
//     {
//         id: 1,
//         pic: <GiTakeMyMoney />,
//         percent: "10.50%*",
//         loan: "Personal Loan"
//     },
//     {
//         id: 2,
//         pic: <GiTakeMyMoney />,
//         percent: "10.50%*",
//         loan: "Personal Loan"
//     },
//     {
//         id: 3,
//         pic: <GiTakeMyMoney />,
//         percent: "10.50%*",
//         loan: "Personal Loan"
//     },

// ]
const swiper = [
    {
        id: 0,
        img: c1,
        price: "$2999"
    },
    {
        id: 1,
        img: c6,
        price: "$2999"
    },
    {
        id: 2,
        img: c3,
        price: "$2999"
    },
    {
        id: 3,
        img: c4,
        price: "$2999"
    },
]

const rating = [
    {
        id: 0,

        title: "Dusmanta Meher",
        about: '“I have applied Business loan through S.G. GROUP OF MEDIA . After 1 week i got sanction loan With cheapest roi S.G. GROUP OF MEDIA team is very aggressive,Responsive Thanks S.G. GROUP OF MEDIA & Team”',
        rating: 3

    },
    {
        id: 1,

        title: "Aradhya Meher",
        about: '“I got sanction of my personal loan in just 2 hour that was awesome for me i have received my loan just 48 hour Thanks S.G. GROUP OF MEDIA & Anjali.”',
        rating: 4
    },
    {
        id: 2,

        title: "Jayanta Meher",
        about: '“I want to say thanks to S.G. GROUP OF MEDIA Those time when i paid 31k minimum due of my credit card that was vary difficult for but S.G. GROUP OF MEDIA team help me to overcome from this. Balance transfer of my all cards convert in loan​”',
        rating: 3
    },


]

const Home = () => {

    const [typeEffect] = useTypewriter({
        words: ["Loan Product We Offers"],
        loop: {},
        typeSpeed: 100,
        deleteSpeed: 40
    });
    const [typeEffect2] = useTypewriter({
        words: ["Courses"],
        loop: {},
        typeSpeed: 100,
        deleteSpeed: 40
    });
    const [typeEffect3] = useTypewriter({
        words: ["& Easy Application Process"],
        loop: {},
        typeSpeed: 100,
        deleteSpeed: 40
    });

    /*multi card section start */

    const responsive = {
        superLargeDesktop: {
            // the naming can be any, depends on you.
            breakpoint: { max: 4000, min: 1024 },
            items: 4
        },
        desktop: {
            breakpoint: { max: 1024, min: 800 },
            items: 4
        },
        tablet: {
            breakpoint: { max: 800, min: 464 },
            items: 2
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 1
        }
    };
    /*multi card section end */
    return (
        <div>
            {/* banner start */}
            <div className='shadow-lg rounded-lg p-4'>
                <img className='w-full h-[500px] mt-4 ' src={banner} alt="banner" style={{borderRadius:'25px'}}/>
            </div>
            {/* banner end */}
            {/* video start */}
            <div className='p-4'>
                <div className="flex flex-col md:flex-row items-center bg-card p-6 rounded-lg shadow-lg">
                    <div className="flex-1 flex flex-col md:flex-row items-center bg-purple-500 p-6 rounded-lg">
                        <div className="flex-1 text-center">
                            <iframe className="mx-auto mb-4 rounded-lg" src="https://www.youtube.com/embed/iT5GRjT-Osk?si=mX9VY2FQeaEGZiOL" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
                            <p className="text-white font-semibold">Sg Group Standard</p>
                        </div>
                        <div className="w-px h-24 bg-white mx-6 hidden md:block"></div>
                        <div className="flex-1 text-center">
                            <iframe className="mx-auto mb-4 rounded-lg" src="https://www.youtube.com/embed/Ek-2VWEvQkQ?si=osQHQmV_zyfKLRoD" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
                            <p className="text-white font-semibold">Sg Group Premium</p>
                        </div>
                    </div>
                    <div className="flex-1 mt-6 md:mt-0 md:ml-6 text-center md:text-left">
                        <h2 className="text-2xl font-bold text-foreground">Preview all these productive upgrades</h2>
                        <p className="text-muted-foreground mt-2">Take a 20-minute video tour of sg group Standard.</p>
                        <button className="mt-4 px-4 py-2 border border-primary text-primary rounded-lg hover:bg-primary hover:text-primary-foreground transition">Watch webinar</button>
                    </div>
                </div>
            </div>
            {/* video end */}
            {/* BOX STRAT   */}
            {/* <div class="grid w-full grid-cols-1 pt-4 lg:grid-cols-4 sm:grid-cols-2 md:grid-cols-2 gap-4 " >
                <div class="bg-card text-card-foreground p-6 rounded-lg shadow-lg flex  items-center gap-6 hover:bg-[#ce8726] hover:text-white cursor-pointer hover:scale-105 duration-300" data-aos="zoom-in-up">
                    <IoIosHome size={40} />
                    <div class="text-xl font-bold ">
                        <p>10.50%*</p>
                        <p>Personal Loan</p>
                    </div>
                </div>
                <div class="bg-card text-card-foreground p-6 rounded-lg shadow-lg flex  items-center gap-4 hover:bg-[#ce8726] hover:text-white cursor-pointer hover:scale-105 duration-300 " data-aos="zoom-in-up">
                    < GiTakeMyMoney size={40} />
                    <div class="text-xl font-bold ">
                        <p>11.20%*</p>
                        <p>Business Loan</p>
                    </div>
                </div>
                <div class="bg-card text-card-foreground p-6 rounded-lg shadow-lg flex  items-center gap-4 hover:bg-[#ce8726] hover:text-white cursor-pointer hover:scale-105 duration-300 " data-aos="zoom-in-up">
                    <FaCarSide size={40} />
                    <div class="text-xl font-bold ">
                        <p>9.25%*</p>
                        <p>Car Loan</p>
                    </div>
                </div>
                <div class="bg-card text-card-foreground p-6 rounded-lg shadow-lg flex  items-center gap-4 hover:bg-[#ce8726] hover:text-white cursor-pointer hover:scale-105 duration-300 " data-aos="zoom-in-up">
                    <BsFillCreditCardFill size={40} />
                    <div class="text-xl font-bold ">
                        <p>12%*</p>
                        <p>CreditLoan</p>
                    </div>
                </div>
            </div> */}
            {/* BOX END */}
            {/* COURSE SECTION */}
            <div className='max-w-[2066px] w-full py-16 px-4 relative bg-gray-100 '>
                <h1 className='text-4xl font-bold text-center '>Our {typeEffect2}<Cursor /></h1>
                <p className='py-6 text-xl text-center'>Education is about learning skills and knowledge.  It also means <br /> helping people to learn how to do things and support them <br /> to think <span className='font-semibold text-[#fca300] '>about what they learn</span>.</p>

                <div className=' w-full grid-cols-1 gap-5 pt-4  lg:grid-cols-4 sm:grid-cols-2'>
                    <MultiCarousel responsive={responsive}
                        autoPlay
                        infinite
                        autoPlaySpeed={3000} // Adjust the speed as per your preference
                        transitionDuration={500} // Transition duration between slides
                        containerClass="carousel-container"
                        className='rounded-md bg-gray-100'
                    >
                        {swiper.map((e) => (
                            <div key={e.id} className='duration-300 border rounded-lg shadow-lg hover:scale-105 ' style={{ margin: '10px' }}>
                                <img className='w-full  object-cover rounded-lg' src={e.img} alt="/" />
                            </div>
                        ))}
                    </MultiCarousel>
                </div>

            </div>
            {/* COURSE SECTION END */}
            {/* LOAN SECTION START */}
            <div className='max-w-[2066px] w-full py-16 px-4 relative'>
                <h1 className='text-4xl font-bold text-center'>Find {typeEffect}<Cursor /></h1>
                <p className='text-xl text-center text-[gray] py-6'>We will match you with a loan program that meet your financial <br /> need. In short term liquidity,by striving to make funds available to them  <br />within<span className='font-bold text-[#fca300]'> 24 hours of application.</span></p>
                <div className='grid w-full grid-cols-1 gap-6 pt-4 lg:grid-cols-4 sm:grid-cols-2'>
                    {card.map((e) => (
                        <div key={e.id} className='duration-500 bg-opacity-100 border rounded-lg shadow-lg  hover:text-[#fff]  mt-4 hover:scale-105 hover:bg-gradient-to-r hover:from-[#4246a9] hover:to-[#4b91f5]' data-aos="zoom-in-up" data-aos-duration="2000">
                            <img className='rounded-t-lg ' src={e.img} alt="/" />
                            <div className='px-4'>
                                <p className='mr-2 text-2xl font-bold'>{e.title}</p>
                                <p className='mb-2 text-xl font-semibold'>{e.discount}</p>
                                <p className='text-[#ffb703] font-semibold mb-4'>{e.cost}</p>
                                <div className='items-center mb-8'>
                                    <button class="bg-blue-500 hover:bg-[#fff] font-bold py-2 px-4 rounded text-black border-none">
                                        Apply Now
                                    </button>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
            {/* LOAN SECTION END */}
            {/* loan apply approve section start */}
            <div className='max-w-[2066px] w-full py-16 px-4 relative bg-[#e4efff]'>
                <p className='font-bold text-4xl text-center text-black'>Fast {typeEffect3}<Cursor /></p>
                <p className='text-center text-black py-6 '>SExperience the convenience of a swift and hassle-free application process, ensuring quick access <br /> to the services you need with minimal effort, <span className='text-[#fca300]'>saving you time and energy.</span></p>
                <div className="flex flex-wrap justify-center cursor-pointer gap-16">
                    <div className="relative bg-card text-card-foreground rounded-lg shadow-lg p-6 w-64 bg-gradient-to-b from-[#413ea0] to-[#4a8cf0] hover:scale-105 duration-500" data-aos="flip-left" data-aos-duration="2000">
                        <div className="absolute -top-8 left-1/2 transform -translate-x-1/2 bg-white p-2 rounded-full shadow-md">
                            <LooksOneIcon size={40} />
                        </div>
                        <h3 className="text-xl font-semibold mt-8 text-center text-white">Apply</h3>
                        <p className="text-muted-foreground text-center mt-2 text-white">Complete a simple online form or visit our nearest branch to start your application process.</p>
                        <div className="flex justify-center mt-4">
                            <button className="bg-primary text-primary-foreground p-2 rounded-full hover:scale-125 duration-500">
                                <img undefinedhidden="true" alt="arrow-right" src="https://openui.fly.dev/openui/white.svg?text=➡️" />
                            </button>
                        </div>
                    </div>

                    <div className="relative bg-card text-card-foreground rounded-lg shadow-lg p-6 w-64 bg-gradient-to-b from-[#413ea0] to-[#4a8cf0] hover:scale-105 duration-500" data-aos="flip-left" data-aos-duration="2000">
                        <div className="absolute -top-8 left-1/2 transform -translate-x-1/2 bg-white p-2 rounded-full shadow-md">
                            <LooksTwoIcon size={40} />
                        </div>
                        <h3 className="text-xl font-semibold mt-8 text-center text-white">Review</h3>
                        <p className="text-muted-foreground text-center mt-2 text-white">Our team will swiftly assess your application and guide you through any necessary documentation.</p>
                        <div className="flex justify-center mt-4">
                            <button className="bg-primary text-primary-foreground p-2 rounded-full">
                                <img undefinedhidden="true" alt="arrow-right" src="https://openui.fly.dev/openui/white.svg?text=➡️" />
                            </button>
                        </div>
                    </div>

                    <div className="relative bg-card text-card-foreground rounded-lg shadow-lg p-6 w-64 bg-gradient-to-b from-[#413ea0] to-[#4a8cf0] hover:scale-105 duration-500" data-aos="flip-left" data-aos-duration="2000">
                        <div className="absolute -top-8 left-1/2 transform -translate-x-1/2 bg-white p-2 rounded-full shadow-md">
                            <Looks3Icon size={40} />
                        </div>
                        <h3 className="text-xl font-semibold mt-8 text-center text-white">Approval</h3>
                        <p className="text-muted-foreground text-center mt-2 text-white">Upon approval, enjoy quick access to funds or services, making your experience seamless and efficient.</p>
                        <div className="flex justify-center mt-4">
                            <button className="bg-primary text-primary-foreground p-2 rounded-full">
                                <img undefinedhidden="true" alt="arrow-right" src="https://openui.fly.dev/openui/white.svg?text=➡️" />
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            {/* loan apply approve section end */}
            {/* COURSE SECTION */}
            <div className='max-w-[2066px] w-full py-16 px-4 relative '>
                <h1 className='text-2xl font-bold text-center '>Our {typeEffect2}<Cursor /></h1>
                <p className='py-6 text-xl text-center'>Education is about learning skills and knowledge.  It also means <br /> helping people to learn how to do things and support them <br /> to think <span className='font-semibold text-gray-600 '>about what they learn</span>.</p>

                {/*rating section start  */}
                <div className='max-w-[2066px] w-full py-16 px-4 relative bg-zinc-900 text-white'>
                    <h1 className='text-4xl font-bold text-center animate-bounce'>Testimonial & Reviews
                    </h1>
                    <p className='text-xl text-center text-[gray] py-6 '>Our customers rave about our exceptional service and hassle-free loan experience, praising our <br />   dedicated support team and transparent processes. With glowing testimonials and positive <br /> reviews, we've built a reputation for reliability and
                        <span className='font-bold text-[#fca300]'> customer satisfaction.</span></p>
                    <div className='grid w-full grid-cols-1 gap-6 pt-4 lg:grid-cols-3 sm:grid-cols-1' data-aos="fade-right"
                        data-aos-offset="300"
                        data-aos-easing="ease-in-sine">
                        {rating.map((e) => (
                            <div key={e.id} className=' bg-opacity-100 border rounded-lg shadow-lg  hover:text-[#fff]  mt-4  '>
                                {/* <img className='rounded-t-lg ' src={e.img} alt="/" /> */}
                                <div className='px-4'>
                                    <p className='mr-2 text-2xl font-bold text-[#fca300] mt-10'>{e.title}</p>
                                    <p className='mb-2 text-xl text-gray-300 '>{e.about}</p>
                                    <Rating name="read-only" value={e.rating} readOnly className='mt-10' />
                                    <div className='items-center mb-8'>
                                        <button class="bg-blue-500 hover:bg-[#fff] font-bold py-2 px-4 rounded text-black border-none">
                                            Explore
                                        </button>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
                {/* rating section end */}
                {/* choose section start */}
                <div className='max-w-[2066px] w-full py-16 px-4 relative bg-[#e4efff]'>
                    <p className='font-bold text-4xl text-center  animate-bounce text-black'>Why People Choose Us</p>
                    <p className='text-center py-6 text-black'>People choose us for our commitment to excellence, reliability, and personalized service, ensuring <br /> peace of mind and satisfaction<span className='text-[#fca300]'> with every interaction.</span></p>
                    <div className="flex flex-wrap justify-center cursor-pointer gap-16">
                        <div className="relative bg-card text-card-foreground rounded-lg shadow-lg p-6 w-64 bg-gradient-to-b from-[#413ea0] to-[#4a8cf0]" data-aos="flip-left" data-aos-duration="2000">
                            <div className="absolute -top-8 left-1/2 transform -translate-x-1/2 bg-white p-2 rounded-full shadow-md">
                                <BsPeopleFill size={35} />
                            </div>
                            <h3 className="text-xl font-semibold mt-8 text-center text-white">Competitive Offerings</h3>
                            <p className="text-muted-foreground text-center mt-2 text-white">Competitive interest rates and transparent terms.</p>
                            <div className="flex justify-center mt-4">
                                <button className="bg-primary text-primary-foreground p-2 rounded-full">
                                    <img undefinedhidden="true" alt="arrow-right" src="https://openui.fly.dev/openui/white.svg?text=➡️" />
                                </button>
                            </div>
                        </div>

                        <div className="relative bg-card text-card-foreground rounded-lg shadow-lg p-6 w-64 bg-gradient-to-b from-[#413ea0] to-[#4a8cf0]" data-aos="flip-left" data-aos-duration="2000">
                            <div className="absolute -top-8 left-1/2 transform -translate-x-1/2 bg-white p-2 rounded-full shadow-md">
                                <TfiCup size={35} />
                            </div>
                            <h3 className="text-xl font-semibold mt-8 text-center text-white">Responsive Customer Support</h3>
                            <p className="text-muted-foreground text-center mt-2 text-white">Prompt assistance and guidance from our dedicated support team.</p>
                            <div className="flex justify-center mt-4">
                                <button className="bg-primary text-primary-foreground p-2 rounded-full">
                                    <img undefinedhidden="true" alt="arrow-right" src="https://openui.fly.dev/openui/white.svg?text=➡️" />
                                </button>
                            </div>
                        </div>

                        <div className="relative bg-card text-card-foreground rounded-lg shadow-lg p-6 w-64 bg-gradient-to-b from-[#413ea0] to-[#4a8cf0] " data-aos="flip-left" data-aos-duration="2000">
                            <div className="absolute -top-8 left-1/2 transform -translate-x-1/2 bg-white p-2 rounded-full shadow-md">
                                <FaMobileRetro size={35} />
                            </div>
                            <h3 className="text-xl font-semibold mt-8 text-center text-white">Flexible Repayment Options</h3>
                            <p className="text-muted-foreground text-center mt-2 text-white">Tailored repayment plans to suit diverse financial needs.</p>
                            <div className="flex justify-center mt-4">
                                <button className="bg-primary text-primary-foreground p-2 rounded-full">
                                    <img undefinedhidden="true" alt="arrow-right" src="https://openui.fly.dev/openui/white.svg?text=➡️" />
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* choose section end */}
                {/* available start */}
                <div className='p-5' data-aos="zoom-in">
                    <div className=" text-2xl font-semibold  text-center text-white bg-gradient-to-r from-[#c252a9] to-[#dc989b] p-2 rounded-lg  items-center  cursor-pointer">
                        <div className={style.scrolling_text_container}>
                            <p className='font-bold text-xl text-white italic hover:not-italic' >Welcome to S.G. GROUP  , your ultimate destination for earning extra cash and discovering lucrative opportunities. Whether you are looking to supplement your income, explore innovative side hustles, or unlock financial freedom, S.G. GROUP OF MEDIA is here to help..</p>
                        </div>

                    </div>
                </div>
                {/* available start */}
            </div>
        </div>
    )
}

export default Home