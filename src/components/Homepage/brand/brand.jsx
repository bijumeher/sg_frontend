
import React from 'react';
import sg5 from '../../../assets/images/money (2).png'
import sg6 from '../../../assets/images/DREAM.png'
import sg7 from '../../../assets/images/TNJIT.png'

const LOGOS = [
    { src: sg5, alt: 'Sg Group' },
    { src: sg6, alt: 'Sg Group' },
    { src: sg7, alt: 'Sg Group' },

];

const logoContainerClasses = "flex space-x-4 overflow-x-auto p-2 gap-4";
const titleClasses = "text-xl font-bold mr-10";

const Brand = () => {
    return (
        <div className="flex items-center justify-center py-4  ">
            <div className={titleClasses}>CO-PARTNERS</div>
            <div className={`${logoContainerClasses} `}>
                {LOGOS.map((logo, index) => (
                    <img key={index} src={logo.src} alt={logo.alt} className='h-20 w-30 hover:scale-105 duration-500  cursor-pointer' />
                ))}
            </div>
        </div>
    );
};

export default Brand;

