import * as React from 'react';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import MobileStepper from '@mui/material/MobileStepper';
// import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import image1 from '../../../assets/images/loan.jpg'
import image2 from '../../../assets/images/education.jpg'

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const images = [
  {

    imgPath:
      image1,
    label: "first image",
    text: "Apply here"

  },
  {

    imgPath:
      image2,
    label: "second image",
    text: "Click For details"

  }

];

function Sliderimage() {
  const theme = useTheme();
  const [activeStep, setActiveStep] = React.useState(0);
  const maxSteps = images.length;

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  return (
    <Box sx={{ maxWidth: "100vw", flexGrow: 1 }}>
      {/* <Paper
        square
        elevation={0}
        sx={{
          display: 'flex',
          alignItems: 'center',
          height: 50,
          pl: 2,
          bgcolor: 'background.default',
        }}
      >
      </Paper> */}

      <AutoPlaySwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={activeStep}
        onChangeIndex={handleStepChange}
        enableMouseEvents
      >

        {images.map((step, index) => (
          <div  key={step.label}>

            {Math.abs(activeStep - index) <= 2 ? (

              //   <Box
              //   component="img"
              //   sx={{
              //     height: "90vh",
              //     display: 'block',
              //     maxWidth: "100vw",
              //     overflow: 'hidden',
              //     width: '100%',
              //   }}

              //   src={step.imgPath}

              //   alt={step.label}

              // />
              <Box>
                <img style={{ height: "85vh", width: "100vw" }} src={step.imgPath} alt="first pic" />
                <button style={{
                  position: "relative",
                  backgroundcolor: "black",
                  bottom: "115px",
                  left: "100px",
                  height: "30px",
                  width: "120px",
                  fontsize: "20px"
                }}
                >{step.text}</button>

              </Box>


            ) : null}
          </div>
        ))}
      </AutoPlaySwipeableViews>
      <MobileStepper
        steps={maxSteps}
        position="static"
        activeStep={activeStep}
        nextButton={
          <Button
            size="small"
            onClick={handleNext}
            disabled={activeStep === maxSteps - 1}
          >
            Next
            {theme.direction === 'rtl' ? (
              <KeyboardArrowLeft />
            ) : (
              <KeyboardArrowRight />
            )}
          </Button>
        }
        backButton={
          <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
            {theme.direction === 'rtl' ? (
              <KeyboardArrowRight />
            ) : (
              <KeyboardArrowLeft />
            )}
            Back
          </Button>
        }
      />
    </Box >
  );
}

export default Sliderimage;
