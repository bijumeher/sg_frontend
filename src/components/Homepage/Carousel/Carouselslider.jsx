import React, { useState } from 'react'
import Carousel from 'react-bootstrap/Carousel';
import 'bootstrap/dist/css/bootstrap.min.css';
import image1 from '../../../assets/images/loan.jpg';
import image2 from '../../../assets/images/education.jpg';
import './car.css'
import { Grid } from '@mui/material';
import { createFranchise } from '../../../actions/franchise/franchiseAction';
import { useDispatch } from 'react-redux';

// form 
const inputClasses = 'w-1/2 p-2 border border-zinc-300 rounded'
const textareaClasses = 'w-full p-2 border border-zinc-300 rounded'
const buttonClasses = 'w-full bg-blue-500 text-white p-2 rounded hover:bg-blue-600'

function Carouselslider() {
  const dispatch = useDispatch();

  const [franchiseData, setfranchiseData] = useState({
    name: '',
    email: '',
    mobile: '',
    state:'',
    city:'',
    area:''
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setfranchiseData({ ...franchiseData, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await dispatch(createFranchise(franchiseData));
      setfranchiseData({
        name: '',
        email: '',
        mobile: '',
        state: '',
        city: '',
        area: ''
      });
    } catch (error) {
      console.error('Error submitting franchise application:', error);
    }
  };
 
  return (
    <>
      <Grid container spacing={2} display={'flex'} className='mt-5' >
        <Grid item xs={12} sm={8} md={8}>
          <Carousel pause={false}>
            <Carousel.Item interval={5000}>
              <img
                className="d-block w-100 max-height-img"
                src={image1}
                alt="First slide"
              />
              <Carousel.Caption
              // style={{ bottom: "10.25rem", right: "0", textAlign: "unset" }}
              >
                <h5
                  style={{ fontWeight: "600", textShadow: "1px 1px 0px black" }}
                >FULLFILL YOUR DREAMS<br /> <strong style={{ color: "#f69013" }}>Fast Approval</strong></h5>

                <button className='buttonclick'>Apply Loan</button>

              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item interval={5000}   >
              <img
                className="d-block w-100 max-height-img"
                src={image2}
                alt="Second slide"
              />
              <Carousel.Caption >
                <h5 style={{ fontWeight: "600", textShadow: "1px 1px 0px black" }} >GET YOUR DREAM JOB</h5>
                <p style={{ color: "#f69013", textShadow: "1px 1px 0px black" }} >computer skills courses available</p>
                <button className='buttonclick' >Check Here</button>

              </Carousel.Caption>
            </Carousel.Item>
          </Carousel>
        </Grid>
        <Grid item xs={12} sm={4} md={4}>
          <div className="bg-background p-6 rounded-lg shadow-md max-w-md mx-auto">
            <h2 className="text-2xl font-bold text-primary mb-4 text-center">FRANCHISEE ENQUIRY FORM</h2>
            <form  onSubmit={handleSubmit}>
              <div className="mb-2">
                <label className="block text-muted-foreground mb-1" htmlFor="name">
                  Name
                </label>
                <input className="border border-border rounded-lg w-full p-2" type="text" id="name" placeholder="Enter your name" name="name" required value={franchiseData.name} onChange={handleChange}/>
              </div>
              <div className="mb-2">
                <label className="block text-muted-foreground mb-1" htmlFor="mobile">
                  Mobile
                </label>
                <input className="border border-border rounded-lg w-full p-2" type="tel" id="mobile" placeholder="Enter your mobile number" name="mobile" required value={franchiseData.mobile} onChange={handleChange}/>
              </div>
              <div className="mb-2">
                <label className="block text-muted-foreground mb-1" htmlFor="email">
                  Email
                </label>
                <input className="border border-border rounded-lg w-full p-2" type="email" id="email" placeholder="Enter your email" name="email" required value={franchiseData.email} onChange={handleChange}/>
              </div>
              <div className=' flex gap-2'>
                <div className="mb-2">
                  <label className="block text-muted-foreground mb-1" htmlFor="state">
                    State
                  </label>
                  <select className="border border-border rounded-lg w-full p-2" id="state" name='state' required value={franchiseData.state} onChange={handleChange}>
                    <option value="Andhra Pradesh">Andhra Pradesh</option>
                    <option value="Odisha">Odisha</option>
                    <option value="Jharkhand">Jharkhand</option>
                    <option value="Karnataka">Karnataka</option>
                    <option value="Uttarakhand">Uttarakhand</option>

                  </select>
                </div>
                <div className="mb-2">
                  <label className="block text-muted-foreground mb-1" htmlFor="city">
                    City
                  </label>
                  <input className="border border-border rounded-lg w-full p-2" type="text" id="city" placeholder="Enter your city" name="city" required value={franchiseData.city} onChange={handleChange}/>
                </div>
                <div className="mb-2">
                  <label className="block text-muted-foreground mb-1" htmlFor="area">
                    Area
                  </label>
                  <input className="border border-border rounded-lg w-full p-2" type="text" id="area" placeholder="Enter your area" name="area" required value={franchiseData.area} onChange={handleChange}/>
                </div>
              </div>
              <div className="mb-2">
                <label className="block text-muted-foreground mb-1" htmlFor="message">
                  Message
                </label>
                <textarea className="border border-border rounded-lg w-full p-2" id="message" rows="4" placeholder="Enter your message" required></textarea>
              </div>
              <button className="bg-primary rounded-lg w-full p-2 hover:scale-105 duration-500 font-bold" type='submit'>Enquiry Now</button>
            </form>
          </div>
        </Grid>
      </Grid>
    </>
  )
}

export default Carouselslider
