import { Box } from '@mui/material'
import React from 'react'
// import InstagramIcon from '@mui/icons-material/Instagram';
import FacebookIcon from '@mui/icons-material/Facebook';
// import LinkedInIcon from '@mui/icons-material/LinkedIn';
import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import PhoneIcon from '@mui/icons-material/Phone';
import EmailIcon from '@mui/icons-material/Email';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import img from '../../assets/images/logo.png'
import { MdContactPhone } from "react-icons/md";
import landline from '../../assets/images/icons8-rotary-dial-telephone-50.png'



const Footer = () => {

    return (
        <Box className='max-w-[2066px] w-full mx-auto py-16 px-4 grid lg:grid-cols-3 gap-8 text-gray-300 bg-black'>
            <div>
                <img className='w-[100px] h-[100px]' src={img} alt="/" />
                <p className=''>Welcome to S.G. GROUP OF MEDIA , your ultimate destination for earning extra cash and discovering lucrative opportunities. Whether you are looking to supplement your income, explore innovative side hustles, or unlock financial freedom, S.G. GROUP OF MEDIA is here to help..</p>
                <div className='flex justify-between my-6 flex-cols md:w-[75% py-4'>
                    {/* <a href="https://www.instagram.com/_can_u_call_me_sg_?utm_source=ig_web_button_share_sheet&igsh=ZDNlZDc0MzIxNw==" target="_blank">
                        < InstagramIcon />
                    </a> */}
                    <a href="https://www.facebook.com/profile.php?id=100007901175515&mibextid=ZbWKwL" target="_blank">
                    < FacebookIcon  />
                    </a>
                    {/* < LinkedInIcon /> */}
                    < WhatsAppIcon />
                </div>
            </div>
            <div>
                <h3><LocationOnIcon />Location</h3>
                <iframe src="https://www.google.com/maps/embed?pb=!1m17!1m12!1m3!1d3723.2089047428244!2d86.48695327525758!3d21.064316980593947!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m2!1m1!2zMjHCsDAzJzUxLjUiTiA4NsKwMjknMjIuMyJF!5e0!3m2!1sen!2sin!4v1721802882394!5m2!1sen!2sin" className='rounded-md' allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                <p className='mt-10'>About SG group
                    Privacy Policy
                    Terms & Condition
                    Blog</p>
            </div>
            <div>
                <h3 className='flex items-center gap-2'><MdContactPhone size={22} />Contact</h3>

                <p className='mt-4'><EmailIcon />info@sgroup.com </p>
                <p className='mt-4'> < PhoneIcon />+91 9556909848</p>
                <p className='mt-4 d-flex gap-1'> <img src={landline} alt="" className='h-6 ' />+91 6784356962</p>

                <p>© Copyright 2024 | S.G. GROUP OF MEDIA</p>

                <div>

                </div>
            </div>
        </Box>
    )
}
//  style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"
export default Footer

